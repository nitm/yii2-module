<?php
/**
 * @link http://www.nitm.com/
 *
 * @copyright Copyright (c) 2014 NITM Inc
 */

namespace nitm\assets;

use yii\web\AssetBundle;

/**
 * @author Malcolm Paul admin@nitm.com
 */
class SortableAsset extends AssetBundle
{
    public $sourcePath = '@nitm/assets/';
    public $js = [
      'js/base-sortable.js',
      'js/base-list.js',
    ];
    public $jsOptions = ['position' => \yii\web\View::POS_END];
    public $depends = [
        'nitm\assets\AppAsset',
    ];
}
