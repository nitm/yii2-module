<?php

namespace nitm\controllers;

use yii\helpers\Inflector;
use nitm\models\log\Entry;
use nitm\models\log\DbEntry;
use yii\filters\VerbFilter;

/**
 * LogController implements the CRUD actions for Entry model.
 */
class LogController extends DefaultController
{
    public $modelClass = 'nitm\models\log\DbEntry';

    /**
     * Lists all Log Entry models.
     *
     * @return mixed
     */
    public function actionIndex($type = null, $options = [])
    {
        $modelClass = $this->modelClass;
        if (!is_null($type)) {
            $modelClass::$collectionName = $modelClass::$collectionName = $type;
        } else {
            $modelClass::$collectionName = $modelClass::$collectionName = $this->module->logCollections[0];
        }
        return parent::actionIndex(DbEntry::class, [
            'construct' => [
                'defaults' => [
                    'sort' => ['created_at' => SORT_DESC],
                ],
            ],
        ]);
    }
}
