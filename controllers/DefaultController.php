<?php

namespace nitm\controllers;

use nitm\helpers\Html;
use nitm\helpers\ArrayHelper;
use nitm\models\Category;
use nitm\helpers\Response;

class DefaultController extends BaseController
{
    public $boolResult;
    /**
     * Redirect requests to the index page to the search function by default.
     */
    public $indexToSearch = true;
    public static $currentUser;

    public function init()
    {
        parent::init();
        static::$currentUser = \Yii::$app->user->identity;
        if (!$this->model && $this->modelClass) {
            $modelClass = $this->modelClass;
            $this->model = new $modelClass();
        }
        $this->determineResponseFormat();
    }

    public function behaviors()
    {
        $behaviors = [
            'access' => [
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => [
                            'index', 'add', 'list', 'list-search', 'view', 'create',
                            'update', 'delete', 'form', 'filter', 'disable',
                            'close', 'resolve', 'complete', 'error',
                        ],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'actions' => [
                    'index' => ['get', 'post'],
                    'list' => ['get', 'post'],
                    'list-search' => ['get', 'post'],
                    'add' => ['get'],
                    'view' => ['get'],
                    'delete' => ['post'],
                    'create' => ['post', 'get'],
                    'update' => ['post', 'get'],
                    'filter' => ['get', 'post'],
                ],
            ],
        ];

        return array_replace_recursive(parent::behaviors(), $behaviors);
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        $actions = [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'remove-parent' => [
                'class' => \nitm\actions\ParentAction::className(),
            ],
            'add-parent' => [
                'class' => \nitm\actions\ParentAction::className(),
            ],
            'list' => [
                'class' => \nitm\actions\ListAction::className(),
            ],
            'list-search' => [
                'class' => \nitm\actions\ListSearchAction::className(),
            ],
        ];

        if (class_exists('nitm\search\Module')) {
            $actions['filter'] = [
               'class' => \nitm\search\actions\FilterAction::className(),
           ];
        }

        return array_merge(parent::actions(), $actions);
    }

    public function beforeAction($action)
    {
        switch ($action->id) {
            case 'delete':
            case 'disable':
            case 'resolve':
            case 'complete':
            case 'close':
            case 'view':
            $this->enableCsrfValidation = false;
            break;
        }

        if (!isset($this->model)) {
            $class = $this->modelClass;
            if (class_exists($class)) {
                $this->model = new $class();
            }
        }

        return parent::beforeAction($action);
    }

    /**
     * Default index controller.
     *
     * @return string HTML index
     */
    public function actionIndex($className = null, $options = [])
    {
        if (!class_exists($className)) {
            if (isset($this->modelClass)) {
                $className = $this->modelClass;
            } elseif (isset($this->model)) {
                $className = $this->model->className();
            }
        }

        $options['with'] = $this->extractWith($options);

        try {
            $options = array_replace_recursive([
               'params' => \Yii::$app->request->get(),
               'with' => [],
               'viewOptions' => [],
               'construct' => [
                   'inclusiveSearch' => false,
                   'exclusiveSearch' => true,
                   'forceExclusiveBooleanSearch' => false,
                   'booleanSearch' => true,
                   'queryOptions' => [],
               ],
           ], $options);
            $this->model->beginSearch(ArrayHelper::getValue($options, 'construct'));
            $queryOptions = ArrayHelper::remove($options, 'queryOptions', []);
            $this->model->searchModel->queryOptions = array_merge($this->model->searchModel->queryOptions, $queryOptions);
            $this->model->searchModel->queryOptions['with'] = $options['with'];

            $dataProvider = $this->model->search($options['params']);
        } catch (\Exception $e) {
            throw $e;
            $searchModel = $this->model ?: new $className();
            $query = $className::find($this->model, $options);
            $dataProvider = new \yii\data\ActiveDataProvider([
              'query' => $query,
              'pagination' => [
                 'route' => '',
              ],
            ]);
        }

        $options['viewOptions'] = array_merge($this->getViewOptions($options), (array) @$options['viewOptions']);

        unset($options['createOptions'], $options['filterOptions']);

        $this->filterWith($dataProvider->query, $options['with']);

        if (!isset($options['pagination']['route'])) {
            $parts = [$this->id];
            if ($this->module) {
                array_unshift($parts, $this->module->id);
            }
            $dataProvider->pagination->route = implode('/', $parts);
        } else {
            $dataProvider->pagination->route = $options['pagination']['route'];
        }

        Response::viewOptions(null, [
            'view' => ArrayHelper::getValue($options, 'view', 'index'),
            'args' => array_merge([
                'dataProvider' => $dataProvider,
                'searchModel' => $this->model->searchModel,
                'model' => $this->model,
            ], $options['viewOptions']),
        ]);

        if (!$this->isResponseFormatSpecified) {
            $this->setResponseFormat('html');
        }

        return $this->renderResponse($this->responseFormat == 'json' ? $dataProvider->getModels() : null, Response::viewOptions(), false);
    }

    /*
     * Get the forms associated with this controller
     * @param string $param What are we getting this form for?
     * @param int $unique The id to load data for
     * @param array $options
     * @return string | json
     */
    public function actionForm($type, $id = null, $options = [], $returnData = false)
    {
        $parsedOptions = $this->getVariables($type, $id, $options);

        $this->determineResponseFormat('html');

        if (\Yii::$app->request->isAjax) {
            $containerId = Response::viewOptions('args.formOptions.container.id');
            $js = ArrayHelper::getValue($options, 'js', '');
            $js = is_array($js) ? $js : [$js];
            Response::viewOptions('js', implode(';', $js), true);
        }

        if ($returnData) {
            return Response::viewOptions();
        }

        return $this->renderResponse($this->responseFormat == 'json' ? $parsedOptions : null, Response::viewOptions(), false);
    }

    /**
     * Displays a single model.
     *
     * @param int $id
     *
     * @return mixed
     */
    public function actionView($id, $modelClass = null, $options = [])
    {
        $modelClass = !$modelClass ? $this->model->className() : $modelClass;
        $this->model = isset($options['model']) ? $options['model'] : $this->findModel($id, $modelClass, array_merge($this->getWith(), ArrayHelper::getValue($options, 'with', [])));
        $view = isset($options['view']) ? $options['view'] : '/'.$this->id.'/view';
        $result = isset($options['args']) ? $options['args'] : [];

        Response::viewOptions(null, $options);
        /*
         * Some default values we would like.
         */
        Response::viewOptions('view', ArrayHelper::getValue($options, 'view', 'view'));
        Response::viewOptions('args', array_merge(
           array_merge(['model' => $this->model], $result),
           ArrayHelper::getValue($options, 'args', []))
        );

        if (Response::viewOptions('assets')) {
            $this->initAssets(Response::viewOptions('assets'), true);
        }

        $this->prepareJsFor(true);

        Response::viewOptions('title', Response::viewOptions('title') ?
\nitm\helpers\Form::getTitle($this->model, ArrayHelper::getValue(Response::viewOptions(), 'title', [])) : '');

        Response::$forceAjax = false;

        $this->log($this->model->properName()."[$id] was viewed from ".\Yii::$app->request->userIp, 3);

        return $this->renderResponse(null, Response::viewOptions(), (\Yii::$app->request->get('__contentOnly') ? true : \Yii::$app->request->isAjax));
    }

    /**
     * Creates a new Category model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @return mixed
     */
    public function actionCreate($modelClass = null, $viewOptions = [])
    {
        if (!$this->isAjaxRequest || $this->isPjaxRequest) {
            return $this->actionForm('create', null, $modelClass, $viewOptions);
        }
        $this->action->id = 'create';
        $ret_val = false;
        $result = [
            'level' => 3,
            '/form/create',
        ];
        $level = 1;
        $this->getModel('create', null, $modelClass);

        if ($this->isValidationRequest()) {
            return $this->performValidationRequest();
        }

        $this->determineResponseFormat('html');

        list($ret_val, $result) = $this->saveInternal(null, 'create');

        if (!\Yii::$app->request->isAjax || !\Yii::$app->getModule('nitm')->enableAjaxForms) {
            $result['redirect'] = str_replace('create', 'update', \yii\helpers\Url::toRoute(['/'.\Yii::$app->requestedRoute, 'id' => $this->model->id]));
        }

        Response::viewOptions('args', array_merge((array) $viewOptions, ['model' => $this->model]), true);

        return [$ret_val, $result, $viewOptions];
    }

    /**
     * Updates an existing Category model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param int $id
     *
     * @return mixed
     */
    public function actionUpdate($id, $modelClass = null, $with = [], $viewOptions = [])
    {
        if (!$this->isAjaxRequest || $this->isPjaxRequest) {
            return $this->actionForm('update', $id, $modelClass, $viewOptions);
        }
        $ret_val = false;
        $result = ['level' => 3];
        $this->getModel('update', $id, $modelClass, $with);

        if ($this->isValidationRequest()) {
            return $this->performValidationRequest();
        }

        $this->determineResponseFormat('html');

        list($ret_val, $result) = $this->saveInternal(null, 'update');

        if (!\Yii::$app->request->isAjax) {
            $result['redirect'] = str_replace('update', 'update', \yii\helpers\Url::toRoute(['/'.\Yii::$app->requestedRoute, 'id' => $id]));
        }

        Response::viewOptions('args', array_merge($viewOptions, ['model' => $this->model]), true);

        return [$ret_val, $result, $viewOptions];
    }

    /**
     * Deletes an existing Category model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param int $id
     *
     * @return mixed
     */
    public function actionDelete($id, $modelClass = null)
    {
        $deleted = false;
        $this->getModel('delete', $id, $modelClass);
        $modelClass = !class_exists($modelClass) ? $this->model->className() : $modelClass;
        if (is_object($this->model)) {
            switch (1) {
                case \Yii::$app->user->identity->isAdmin():
                case $this->model->hasAttribute('author_id') && ($this->model->author_id == \Yii::$app->user->getId()):
                case $this->model->hasAttribute('user_id') && ($this->model->user_id == \Yii::$app->user->getId()):
                $attributes = $this->model->getAttributes();
                if ($this->model->delete()) {
                    $deleted = true;
                    $this->model = new $modelClass($attributes);
                }
                $level = 1;
                break;

                default:
                $level = 6;
                break;
            }
        }

        $this->setResponseFormat('json');

        return [$deleted, \Yii::$app->request->isAjax ? [] : ['redirect' => \Yii::$app->request->getReferrer(), 'logLevel' => $level]];
    }

    /**
     * Put here primarily to handle action after create/update.
     */
    public function afterAction($action, $result)
    {
        if ($this->isValidationRequest()) {
            return $result;
        }
        if (is_callable($result)) {
            return $this->renderResponse(null, $result);
        }
        if ($result instanceof \yii\web\Response) {
            return $result;
        }
        if (isset($result[1]['redirect'])) {
            return $this->redirect($result[1]['redirect'] ? $result[1]['redirect'] : ['index']);
        }

        if (!is_array($result)) {
            return $result;
        }
        if (ArrayHelper::isIndexed($result)) {
            @list($success, $result, $options) = $result;
            $ret_val = is_array($result) ? $result : [
                'success' => $success,
            ];
            $ret_val['success'] = $success;
        } else {
            $ret_val = $result;
            $success = ArrayHelper::getValue($result, 'success', false);
        }
        $options = isset($options) ? $options : [];
        if ($success) {

            /*
             * Perform logging if logging is enabled in the module and the controller enables it
             * NOTICE: This is now handled at the Model level.
             */
             try {
                 if ($this->module->enableLogger && $this->shouldLog) {
                     call_user_func_array([$this->module, 'log'], $this->getLogParams($success, $result, $this->model));
                     foreach (['logLevel', 'collection_name'] as $remove) {
                         unset($ret_val[$remove]);
                     }
                 }
             } catch (\Exception $e) {
                 \Yii::warning("[".$this->module->id."] Logging is enabled but experienced error: ".$e);
             }

            if (\Yii::$app->request->isAjax) {
                $format = $this->isResponseFormatSpecified ? $this->responseFormat : 'json';
                $this->setResponseFormat($format);
                if ($this->action->id == 'create') {
                    $ret_val['message'] = ArrayHelper::getValue($ret_val, 'message', '').' '.Html::tag('a', 'Click Here to Update', [
                         'href' => '/'.($this->module->id != 'app' ? $this->module->id.'/' : '').$this->id.'/form/update/'.$this->model->id,
                     ]);
                }
                $viewFile = ArrayHelper::getValue($options, 'view', $this->model->isWhat().'/view');
                $ret_val['success'] = true;
                $ret_val['action'] = ArrayHelper::getValue($options, 'action', ArrayHelper::getValue($result, 'action', $this->action->id));
                if ($this->responseFormat == 'json') {
                    if ($viewFile = $this->resolveViewPath($viewFile)) {
                        $ret_val['data'] = $this->renderAjax($viewFile, ['model' => $this->model]);
                    }
                } else {
                    if ($viewFile = $this->resolveViewPath($viewFile)) {
                        Response::viewOptions('content', $this->renderAjax($viewFile, ['model' => $this->model]));
                    } else {
                        Response::viewOptions('content', true);
                    }
                }
            } else {
                if (!isset($ret_val['class'])) {
                    $ret_val['class'] = $success ? 'success' : 'error';
                }
                \Yii::$app->getSession()->setFlash($ret_val['class'], ArrayHelper::getValue($ret_val, 'message', 'Success!'));

                return $this->redirect(isset($result['redirect']) ? $result['redirect'] : ['index']);
            }
        }
        if (!$success) {
            if ($this->model && $this->model->getErrors()) {
                $ret_val['message'] = array_map('implode', $this->model->getErrors(), ['. ']);
            } else {
                $ret_val['message'] = ArrayHelper::getValue($ret_val, 'message', implode(' ', array_filter([
                   "Couldn't",
                   ArrayHelper::getValue($result, 'action', $action->id),
                   $this->model->properName(),
                   $this->model->title(),
                ])));
            }
        } else {
            $ret_val['message'] = ArrayHelper::getValue($ret_val, 'message', implode(' ', array_filter([
               'Successfully',
               ArrayHelper::getValue($result, 'action', $action->id).'d',
               $this->model->properName(),
               $this->model->title(),
           ])));
        }
        $ret_val['id'] = $this->model->getId();

        return $this->renderResponse($ret_val, Response::viewOptions(), \Yii::$app->request->isAjax);
    }

    protected function resolveViewPath($path)
    {
        $path = \Yii::getAlias($path);
        if (file_exists($path)) {
            return $path;
        } else {
            $path = $this->getViewPath().DIRECTORY_SEPARATOR.ltrim($path, '/');
            if (file_exists($path)) {
                return $path;
            }
        }

        return false;
    }
}
