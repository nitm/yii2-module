<?php

namespace nitm\controllers;

use Yii;
use yii\rest\Controller;
use nitm\helpers\Response;
use nitm\helpers\ArrayHelper;

class DefaultApiController extends Controller
{
    use \nitm\traits\Configer, \nitm\traits\Controller, \nitm\traits\ControllerActions;
    public $boolResult;

    const ELEM_TYPE_PARAM = '__elemType';
    const EVENT_AFTER_FIND = 'afterFind';
    const EVENT_AFTER_CREATE = 'afterCreate';
    const EVENT_AFTER_UPDATE = 'afterUpdate';
    const EVENT_AFTER_DELETE = 'afterDelete';

    public function behaviors()
    {
        $behaviors = [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'only' => ['index', 'add', 'list', 'view', 'creat', 'update', 'delete'],
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['index', 'add', 'list', 'view', 'create', 'update', 'delete'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => \yii\filters\VerbFilter::className(),
                'actions' => [
                    'index' => ['get'],
                    'list' => ['get', 'post'],
                    'add' => ['get'],
                    'view' => ['get'],
                    'delete' => ['post'],
                    'create' => ['post'],
                    'update' => ['post', 'get'],
                   'filter' => ['get', 'post'],
                ],
            ],
        ];

        return array_replace_recursive(parent::behaviors(), $behaviors);
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'remove-parent' => [
                'class' => \nitm\actions\ParentAction::className(),
            ],
            'add-parent' => [
                'class' => \nitm\actions\ParentAction::className(),
            ],
            'list' => [
                'class' => \nitm\actions\ListAction::className(),
            ],
            'list-json' => [
                'class' => \nitm\actions\ListJsonAction::className(),
            ],
            'list-search' => [
                'class' => \nitm\actions\ListSearchAction::className(),
            ],
            'boolean-action' => [
                'class' => \nitm\actions\BooleanAction::className(),
            ],
        ];
    }

    public function init()
    {
        //$this->initConfig(@Yii::$app->controller->id);
        parent::init();
        if (!$this->isResponseFormatSpecified) {
            $this->setResponseFormat('json');
        }

        if (!isset($model) && (isset($this->modelClass))) {
            $this->model = new $this->modelClass();
        }
    }

    /**
     * Creates a new Category model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @return mixed
     */
    public function actionCreate($modelClass = null, $options=[])
    {
        $modelClass = is_null($modelClass) ? $this->model->className() : $modelClass;
        $saved = false;
        $this->getModel('create', null, $modelClass);

        if ($this->isValidationRequest()) {
            return $this->performValidationRequest();
        }

        return $this->saveInternal(null, 'create', null, $options);
    }

    /**
     * Updates an existing Category model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param int $id
     *
     * @return mixed
     */
    public function actionUpdate($id, $modelClass = null, $options = [])
    {
        $modelClass = is_null($modelClass) ? $this->model->className() : $modelClass;
        $saved = false;
        $this->getModel('update', $id, $modelClass);

        if ($this->isValidationRequest()) {
            return $this->performValidationRequest();
        }

        return $this->saveInternal(null, 'update', null, $options);
    }

    /**
     * Updates an existing Category model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param int $id
     *
     * @return mixed
     */
    public function actionView($id, $modelClass = null)
    {
        $modelClass = is_null($modelClass) ? $this->model->className() : $modelClass;
        $this->model = $this->findModel($id, $modelClass);

        return $this->model->toArray();
    }

    public function actionDelete($id, $modelClass = null, $setFlagOnly = false)
    {
        $modelClass = is_null($modelClass) ? $this->model->className() : $modelClass;
        $this->setResponseFormat('json');
        $this->getModel('delete', $id, $modelClass);
        $deleted = false;
        if ($this->model) {
            if ($setFlagOnly) {
                $this->model->deleted = 1;
                $deleted = $this->model->save();
            } else {
                $deleted = $this->model->delete();
            }
        } else {
            $this->model = new $modelClass;
        }
        if (is_numeric($id)) {
            $this->model->id = $id;
        }

        return [$deleted, \Yii::$app->request->isAjax ? [] : ['redirect' => \Yii::$app->request->getReferrer(), 'logLevel' => $level]];
    }

    /**
     * Put here primarily to handle action after create/update.
     */
    public function afterAction($action, $result)
    {
        if (is_callable($result)) {
            return $this->renderResponse(null, $result);
        }

        if (!is_array($result)) {
            return $result;
        }
        if (ArrayHelper::isIndexed($result)) {
            @list($success, $result, $options) = $result;
            $ret_val = is_array($result) ? $result : [
                'success' => $success,
            ];
            $ret_val['success'] = $success;
        } else {
            $ret_val = $result;
            $success = ArrayHelper::getValue($result, 'success', false);
        }
        $ret_val = is_array($result) ? $result : [
            'success' => $success,
        ];
        $options = isset($options) ? $options : [];
        if ($success) {
            $format = Response::formatSpecified() ? $this->getResponseFormat() : 'json';
            $this->setResponseFormat($format);
            $view = ArrayHelper::getValue($options, 'view', 'view');
            switch ($this->getResponseFormat()) {
               case 'json':
               try {
                $ret_val = [
                    'data' => $this->renderAjax($view, ['model' => $this->model]),
                    'success' => true,
                ];
               } catch (\Exception $e) {
                   $ret_val = [
                       'data' => $result
                   ];
               }
               break;

               default:
               Response::viewOptions('content', $this->renderAjax($view, ['model' => $this->model]));
               break;
          }
        } else {
            $ret_val['errors'] = $this->model->getErrors();
            $ret_val['message'] = implode('. ', array_map(function ($errors) {
                return implode('. ', $errors);
            }, $this->model->getErrors()));
        }
        $ret_val['success'] = $success;
        $ret_val['action'] = $this->action->id;
        $ret_val['id'] = $this->model->getId();

        return $this->renderResponse($ret_val, Response::viewOptions(), \Yii::$app->request->isAjax);
    }
}
