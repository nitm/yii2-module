<?php

namespace nitm\controllers;

use nitm\models\search\User as UserSearch;
use yii\filters\VerbFilter;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends DefaultController
{
    public $modelClass = 'nitm\models\User';

    public $legend = [
        'success' => 'Active User',
        'default' => 'Inactive User',
        'danger' => 'Banned User',
        'info' => 'Admin User',
        'warning' => 'Api User',
    ];

    /**
     * Search for a user.
     */
    public function actionAutocomplete()
    {
        $ret_val = [];
        $searchModel = new UserSearch();
        $searchModel->setScenario('apiSearch');
        $dataProvider = $searchModel->search($_GET);
        $serializer = new \yii\data\ModelSerializer([
            'fields' => ['id', 'f_name', 'l_name'],
        ]);
        foreach ($dataProvider->getModels() as $user) {
            $ret_val[] = [
                'value' => $user->id,
                'label' => $user->f_name.' '.$user->l_name.' ('.$user->getStatus().', '.$user->getRole().')',
            ];
        }
        $this->displayAjaxResult($ret_val);
    }

    /**
     * Get the class indicator value for the users status.
     *
     * @param User $user
     *
     * @return string $css class
     */
    public function getStatusIndicator($user = null)
    {
        $user = is_null($user) ? $this : $user;
        $ret_val = 'default';
        switch ($user instanceof User) {
            case true:
            switch (User::getStatus($user)) {
                case 'Active':
                $ret_val = 'success';
                break;

                case 'Banned':
                $ret_val = 'error';
                break;
            }
            switch (User::getRole($user)) {
                case 'Admin':
                $ret_val = 'info';
                break;

                case 'Api User':
                $ret_val = 'warning';
                break;
            }
            break;
        }
        $indicator = $this->statusIndicators[$ret_val];

        return $indicator;
    }
}
