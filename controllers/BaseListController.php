<?php

namespace nitm\controllers;

use nitm\helpers\ArrayHelper;

abstract class BaseListController extends DefaultApiController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = [
            'access' => [
                'rules' => [
                    [
                        'actions' => [
                            'toggle', 'add-to-list', 'remove-from-list', 'update-list-item',
                        ],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'actions' => [
                    'toggle' => ['post'],
                    'add-to-list' => ['post'],
                    'remove-from-list' => ['post'],
                    'update-list-item' => ['post'],
                ],
            ],
        ];

        return array_replace_recursive(parent::behaviors(), $behaviors);
    }

    /**
     * Add an item to the list
     *
     * @param string $modelClass
     * @param array $options
     * @return void
     */
    public function actionAddToList($modelClass = null, $options = [])
    {
        $modelClass = $this->getModelClass(\Yii::$app->request->get('type')) ?: $this->modelClass;

        return parent::actionCreate($modelClass, array_merge($options, [
            'view' => '_form'
        ]));
    }

    /**
     * Update a list item
     *
     * @param integer|string $id
     * @param string $modelClass
     * @param array $options
     * @return void
     */
    public function actionUpdateListItem($id, $modelClass = null, $options = [])
    {
        $modelClass = $this->getModelClass(\Yii::$app->request->get('type')) ?: $this->modelClass;

        return parent::actionUpdate($id, $modelClass, $options);
    }

    /**
     * Remove an item from the list
     *
     * @param integer|string $id
     * @param string $modelClass
     * @return void
     */
    public function actionRemoveFromList($id, $modelClass = null)
    {
        $modelClass = $this->getModelClass(\Yii::$app->request->get('type')) ?: $this->modelClass;

        return parent::actionDelete($id, $modelClass);
    }
    
    /**
     * Toggle a value
     *
     * @param integer|string $id
     * @return void
     */
    public function actionToggle($id = null)
    {
        $ret_val = [
         'success' => true,
         'class' => \nitm\helpers\Statuses::getListIndicator('default'),
      ];
        $class = $this->getModelClass(\Yii::$app->request->get('type'));
        $this->model = new $class();
        $id = ArrayHelper::getValue(\Yii::$app->request->post(), $this->model->formName().'.id', $id ?? \Yii::$app->request->post('id'));
        if ($id) {
            $this->model = $class::findOne($id);
            if ($this->model) {
                $this->model->delete();
                $ret_val['message'] = 'Removed '.$this->model->title().' from list';
                $this->model = new $class();
            } else {
                $this->model = new $class();
            }
            $this->action->id = 'toggle-delete';
        } else {
            $this->model = new $class();
            $this->model->load($_POST);
            if ($this->model->save()) {
                $ret_val['class'] = \nitm\helpers\Statuses::getListIndicator($this->model->getStatus());
                $ret_val['message'] = 'Added '.$this->model->title().' to list';
            } else {
                $ret_val['success'] = false;
            }
            $this->action->id = 'toggle-create';
        }
        $this->responseFormat = 'json';

        return [$ret_val['success'], $ret_val];
    }
}
