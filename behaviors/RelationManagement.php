<?php

namespace nitm\behaviors;

use nitm\helpers\ArrayHelper;
use nitm\db\ActiveRecord;

/**
 * Automatically manage and sync relations after saving a model
 * Related data should be provdied as attributes to the model. i.e.:
 * Attributes = [id, name, items]
 * Relation = types
 * $_POST should be:
 * [
 *    id => {id},
 *    name => {name},
 *    items => [
 *       {items},
 *       ...
 *    ]
 * ].
 *
 * All items will then be saved as relations to the owner model
 *
 * @property \yii\db\ActiveRecord $owner Description
 *
 * @author Malcolm Paul <malcolm@ninjasitm.com>
 */
class RelationManagement extends \yii\base\Behavior
{
    public $relations;
    public $force;

    public function events()
    {
        return [
               ActiveRecord::EVENT_AFTER_INSERT => 'sync',
               ActiveRecord::EVENT_AFTER_UPDATE => 'sync',
               ActiveRecord::EVENT_BEFORE_INSERT => 'sync',
               ActiveRecord::EVENT_BEFORE_UPDATE => 'sync',
           ];
    }
    /**
     * @param array $data
     * @param array $relations
     *
     * @return bool Description
     */
    public function sync($event, $data = null, $relations = null)
    {
        if (\Yii::$app instanceof \yii\console\Application) {
            return;
        }
        $data = $data ?: \Yii::$app->request->post($event->sender->formName());
        if (empty($data)) {
            return false;
        }
        $this->relations = (array) ($relations ?: $this->relations);
        if (is_array($this->relations) && !count($this->relations) || $this->force) {
            $this->relations = array_unique(array_filter(array_map(function ($name) use ($event) {
                // For columns sych as type_id, category_id...etc remove the _id portion
                $name = str_replace(['_', '_'], '', preg_replace('/_id$/', '', $name, 1));
                try {
                    $relation = $event->sender->getRelation($name, false);
                } catch (\Exception $e) {
                    $relation = null;
                }
                if ($relation) {
                    return $name;
                }
            }, $event->sender->attributes())));
        }
        $result = [];
        foreach ($this->relations as $relationName) {
            $result = array_merge((array) $result, (array) $this->saveRelation($event, $relationName, $data));
        }

        return $result;
    }

    protected function saveRelation($event, $relationName, $data)
    {
        $relation = $event->sender->getRelation($relationName);
        if (!$relation) {
            return false;
        }
        /* @var $class \yii\db\ActiveRecord */
        $class = $relation->modelClass;
        $relatedModel = new $class();
        $multiple = $relation->multiple;

        // link of relation
        $links = [];
        foreach ($relation->link as $from => $to) {
            if ($event->sender->hasAttribute($to)) {
                $links[$from] = $event->sender[$to];
            }
        }
        //Don't save if the links have empty values
        if (count(array_filter($links)) == 0) {
            return false;
        }
        $indexBy = key($links);
        $relation->indexBy($indexBy);

        $formName = $relatedModel->formName();
        $postDetails = ArrayHelper::getValue($data, $formName, []);

        /* @var $detail \yii\db\ActiveRecord */
        $result = false;
        if ($multiple) {
            $keep = [];
            $children = $relation->all();
            foreach ($postDetails as $index => $dataDetail) {
                if (ArrayHelper::isSubset(array_merge($links, $dataDetail), $children)) {
                    continue;
                }
                $detail = new $class($dataDetail);
                $keep[$dataDetail[$indexBy]] = $detail;
            }
            if (count($keep)) {
                try {
                    $result = $event->sender->link($relationName, $keep);
                } catch (\Exception $e) {
                    $result = $event->sender->getErrors();
                }
            }

            $delete = ArrayHelper::filter($children, ['!'.$indexBy]);
            if (count($delete)) {
                try {
                    array_walk($delete, function ($item) use ($event, $relationName) {
                        $event->sender->unlink($relationName, $item, true);
                    });
                } catch (\Exception $e) {
                    $result = $event->sender->getErrors();
                }
            }
        } else {
            $children = $relation->one();
            if (is_array($children)) {
                $keep = new $class;
                $keep->load($children, '');
            } else {
                $keep = $children === null ? new $class() : $children;
            }
            if ($keep && $keep->isNewRecord) {
                try {
                    $result = $event->sender->link($relationName, $keep);
                } catch (\Exception $e) {
                    $result = $event->sender->getErrors();
                }
            }
        }

        $result = $event->sender->populateRelation($relationName, $keep);

        return $result;
    }
}
