<?php
/**
 * @author Brett O'Donnell <cornernote@gmail.com>
 * @copyright 2015 Mr PHP
 *
 * @link https://github.com/cornernote/yii2-softdelete
 *
 * @license BSD-3-Clause https://raw.github.com/cornernote/yii2-softdelete/master/LICENSE.md
 */

namespace nitm\behaviors;

use yii\base\Behavior;
use yii\db\BaseActiveRecord;
use nitm\helpers\ArrayHelper;
use nitm\helpers\Model as ModelHelper;
use nitm\helpers\Cache as CacheHelper;

/**
 * SoftDeleteBehavior.
 *
 * @usage:
 * ```
 * public function behaviors() {
 *     return [
 *         [
 *             'class' => 'nitm\behaviors\RecordList',
 *             'idAttribute' => 'id',
 *             'labelAttribute' => 'title'
 *             'where' => []
 *         ],
 *     ];
 * }
 * ```
 *
 * @property BaseActiveRecord $owner
 */
class RecordList extends Behavior
{
    /**
     * @var string List id/value attribute
     */
    public $idAttribute = 'id';

     /**
      * @var string List label/display attribute
      */
     public $labelAttribute = 'title';

    public $where = [
        'id' => 'id',
     ];

    private $_assertedOwner;

    public function getChildList($id = null, $label = null)
    {
        // $children = $this->resolveOwner()->children();
        // if (is_array($children)) {
        //     \Yii::warning("Children is array: ".\yii\helpers\VarDumper::dump($this->resolveOwner()));
        // }

        return $this->resolveOwner()->getChildren()->asList($id, $label);
    }

    public function getJsonChildList($id = null, $label = null)
    {
        return $this->resolveOwner()->getChildren()->asJsonList($id, $label);
    }

    protected function getListItems($id = null, $label = null, $options = [])
    {
        $separator = ArrayHelper::remove($options, 'separator');
        $class = $this->owner->className();
        $separator = is_null($separator) ? ' ' : $separator;
        $realLabel = is_callable($label) ? ModelHelper::getTitleAttribute($this->owner) : $label;
        $options['select'] = ArrayHelper::getValue($options, 'select', [$id, $realLabel]);
        if (!isset($options['orderBy'])) {
            $options['orderBy'] = [(is_array($realLabel) ? end($realLabel) : $realLabel) => SORT_ASC];
        }
      //   $options['groupBy'] = ArrayHelper::getValue($options, 'groupBy', [$id]);
        $options['indexBy'] = ArrayHelper::getValue($options, 'indexBy', $id);

        return [ModelHelper::locateItems($this->owner, $options), $id, $label, $realLabel, $separator];
    }

   /**
    * Get a one dimensional associative array.
    *
    * @param mixed $label
    * @param mixed $queryOptions
    *
    * @return array
    */
   public function getList($id = null, $label = null, $options = [])
   {
       $id = $id ?: $this->idAttribute;
       $label = $label ?: $this->labelAttribute;

       return CacheHelper::remember([$this->owner->isWhat(), $options, 'list', true], function () use ($options, $label, $id) {
           list($items, $id, $label, $realLabel, $separator) = $this->getListItems($id, $label, $options);
           if (count($items) >= 1) {
               $ret_val = self::populateList($items, function ($item, $label, $separator) {
                   return ModelHelper::getLabel($item, $label, $separator);
               }, [
                    'label' => $label,
                    'separator' => $separator,
                    'children' => ArrayHelper::getValue($options, 'children', false),
               ]);
           }

           return $ret_val;
       });
   }

    /**
     * Get a multi dimensional associative array suitable for Json return values.
     *
     * @param mixed $label
     * @param mixed $options
     *
     * @return array
     */
    public function getJsonList($id = null, $label = null, $options = [])
    {
        $id = $id ?: $this->idAttribute;
        $label = $label ?: $this->labelAttribute;

        return CacheHelper::remember([$this->owner->isWhat(), $options, 'json-list', true], function () use ($label, $options, $id) {
            list($items, $id, $label, $realLabel, $separator) = $this->getListItems($id, $label, $options);
            $ret_val = [];
            if (count($items)) {
                $ret_val = self::populateList($items, function ($item, $label, $separator) use ($options) {
                    $_ = [
                            'id' => $item->getId(),
                            'value' => $item->getId(),
                            'text' => $item->$label,
                            'name' => $item->$label,
                            'label' => ModelHelper::getLabel($item, $label, $separator),
                        ];
                    if (isset($options['with'])) {
                        foreach ($options['with'] as $attribute) {
                            if ($attribute == 'htmlView') {
                                $view = \Yii::$app->getViewPath();
                                if (!isset($options['view']['file'])) {
                                    if (file_exists(\Yii::getAlias($path.$item->isWhat(true).'/view.php'))) {
                                        $view = '/'.$item->isWhat(true).'/view';
                                    } elseif (file_exists(\Yii::getAlias($path.$item->isWhat().'/view.php'))) {
                                        $view = '/'.$item->isWhat().'/view';
                                    }
                                }
                                $viewOptions = isset($options['view']['options']) ? $options['view']['options'] : ['model' => $item];
                                $_['html'] = \Yii::$app->getView()->renderAjax($view, $viewOptions);
                            }
                        }
                    }

                    return $_;
                }, [
                     'label' => $label,
                     'separator' => $separator,
                     'children' => ArrayHelper::getValue($options, 'children', false),
                ]);
            }

            return $ret_val;
        });
    }

    /**
     * Assert the proper owner model based on the $where condition.
     *
     * @return object [description]
     */
    protected function resolveOwner()
    {
        if (!isset($this->_assertedOwner)) {
            if ($this->owner->isNewRecord) {
                if (is_array($this->where)) {
                    $query = $this->owner->find();
                    foreach ($this->where as $key => $attribute) {
                        $query->andWhere([$key => $this->owner->{$attribute}]);
                    }
                    $this->_assertedOwner = $query->one();
                } elseif (is_callable($this->where)) {
                    $this->_assertedOwner = call_user_func_array($this->where, [$query]);
                } else {
                    $this->_assertedOwner = $this->owner;
                }
            } else {
                $this->_assertedOwner = $this->owner;
            }
        }

        return $this->_assertedOwner ?: $this->owner;
    }

    protected function populateList($items, $callable, $options, $canStop = false)
    {
        extract($options);
        $ret_val = [];
        foreach ($items as $idx => $item) {
            $itemLabel = call_user_func_array($callable, [
             $item, $label, $separator,
          ]);
            if (isset($children) && $children === true) {
                // $item->resetBehavior('tree');
                $item->owner->setFlag('shouldBindToParent', false);
                if ($item->isRelationPopulated('children')) {
                    $ret_val[$itemLabel] = static::populateList($item->children, $callable, $options);
                } elseif ($item->hasMethod('children')) {
                    $itemChildren = $item->children()->all();
                    if (count($itemChildren)) {
                        $ret_val[$itemLabel] = static::populateList($itemChildren, $callable, $options, true);
                    } else {
                        $ret_val[$item[$this->idAttribute]] = $itemLabel;
                    }
                }
            } else {
                $ret_val[$item[$this->idAttribute]] = $itemLabel;
            }
        }

        return $ret_val;
    }
}
