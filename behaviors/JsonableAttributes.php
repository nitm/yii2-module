<?php

namespace nitm\behaviors;

use yii\db\ActiveRecord;
use nitm\helpers\ArrayHelper;
use nitm\helpers\DateFormatter;
use nitm\helpers\Json;

class JsonableAttributes extends \yii\base\Behavior
{
    public $attributes = [];

    public function events()
    {
        return [
            ActiveRecord::EVENT_BEFORE_INSERT => 'attributeToJson',
            ActiveRecord::EVENT_BEFORE_UPDATE => 'attributeToJson',
            ActiveRecord::EVENT_INIT => 'attributesToArray',
        ];
    }

    public function attributeToJson($event)
    {
        $attributes = array_filter((array)$this->attributes);
        foreach ($attributes as $attribute) {
            $event->sender->{$attribute} = Json::encode($event->sender->{$attribute});
        }
    }

    public function attributesToArray($event)
    {
        $attributes = array_filter((array)$this->attributes);
        foreach ($attributes as $attribute) {
            if ($event->sender->hasAttribute($attribute)) {
                $event->sender->setAttribute($attribute, $this->attributeToArray($attribute, $event->sender));
            }
        }
        return true;
    }

    public function attributeToArray($attribute, $sender=null)
    {
        $sender = $sender ?: $this->owner;
        $value = $sender->{$attribute};
        return Json::decode($value, true);
    }
}
