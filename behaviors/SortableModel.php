<?php

namespace nitm\behaviors;

use yii\db\ActiveRecord;

/**
 * Sortable behavior. Enables model to be sorted manually by admin.
 */
class SortableModel extends \yii\base\Behavior
{
    public function events()
    {
        return [
            ActiveRecord::EVENT_BEFORE_INSERT => 'findPriority',
        ];
    }

    public function findPriority()
    {
        if (!$this->owner->priority) {
            $priority = (int) (new \yii\db\Query())
                ->select('MAX(priority)')
                ->from($this->owner->tableName())
                ->scalar();
            $this->owner->priority = ++$priority;
        }
    }
}
