<?php

namespace nitm\behaviors;

use Yii;
use yii\db\ActiveRecord;

/**
 * CacheFlush behavior
 * {@inheritdoc}
 */
class Cache extends \yii\base\Behavior
{
    /** @var string */
    protected $_key;

    public function attach($owner)
    {
        parent::attach($owner);
    }

    public function events()
    {
        return [
            ActiveRecord::EVENT_AFTER_INSERT => 'flush',
            ActiveRecord::EVENT_AFTER_UPDATE => 'flush',
            ActiveRecord::EVENT_AFTER_DELETE => 'flush',
        ];
    }

    public function setKey($key)
    {
        $this->_key = $key;
    }

    public function getKey()
    {
        if (!$this->_key) {
            $this->_key = \nitm\helpers\Cache::getKey($this->owner);
        }
    }

    /**
     * Flush cache.
     */
    public function flush()
    {
        if ($this->key) {
            if (is_array($this->key)) {
                foreach ($this->key as $key) {
                    Yii::$app->cache->delete($key);
                }
            } else {
                Yii::$app->cache->delete($this->key);
            }
        } else {
            Yii::$app->cache->flush();
        }
    }
}
