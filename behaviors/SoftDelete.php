<?php
/**
 * @author Brett O'Donnell <cornernote@gmail.com>
 * @copyright 2015 Mr PHP
 *
 * @link https://github.com/cornernote/yii2-softdelete
 *
 * @license BSD-3-Clause https://raw.github.com/cornernote/yii2-softdelete/master/LICENSE.md
 */

namespace nitm\behaviors;

use yii\base\Behavior;
use yii\base\Event;
use yii\db\BaseActiveRecord;
use yii\db\Expression;
use nitm\helpers\ArrayHelper;

/**
 * SoftDeleteBehavior.
 *
 * @usage:
 * ```
 * public function behaviors() {
 *     return [
 *         [
 *             'class' => 'cornernote\behaviors\SoftDeleteBehavior',
 *             'attribute' => 'delete_time',
 *             'value' => new Expression('NOW()'),
 *         ],
 *     ];
 * }
 * ```
 *
 * @property BaseActiveRecord $owner
 */
class SoftDelete extends Behavior
{
    /**
     * @var string SoftDelete attribute
     */
    public $attribute = 'deleted_at';

    protected $getDeleted = false;

    /**
     * @var callable|Expression The expression that will be used for generating the timestamp.
     *                          This can be either an anonymous function that returns the timestamp value,
     *                          or an [[Expression]] object representing a DB expression (e.g. `new Expression('NOW()')`).
     *                          If not set, it will use the value of `time()` to set the attributes
     */
    public $value;

    /**
     * {@inheritdoc}
     */
    public function events()
    {
        $class = $this->owner->className();
        return [
           $class::EVENT_BEFORE_DELETE => 'softDeleteEvent',
           $class::EVENT_BEFORE_INSERT => 'softRestoreEvent'
        ];
    }

    public function withDeleted()
    {
        $this->withDeleted = true;
    }

    public function withoutDeleted()
    {
        $this->withDeleted = false;
    }

    /**
     * Restore a model before save if it exists
     *
     * @param Event $event
     */
    public function softRestoreEvent($event)
    {
        $query = $event->sender->find();
        $rules = $event->sender->rules();
        $scenario = $event->sender->scenario;
        if (ArrayHelper::getValue($scenario, 'returnExisting', false)) {
            $existing = $this->owner->find()->where(array_intersect_key($event->sender->attributes, array_flip($attributes)))->one();
            if ($existing) {
                $existing->scenario = 'update';
                if (is_null($providedModel)) {
                    $this->owner = $existing;
                }
                $existing->load($event->sender->dirtyAttributes, '');
                $this->owner = $existing;
                try {
                    //Try restorin the model if it is deleted
                    $this->owner->restore();
                    $event->isValid = false;
                } catch (\Exception $e) {
                }
            }
        }
    }

    /**
     * Set the attribute with the current timestamp to mark as deleted.
     *
     * @param Event $event
     */
    public function softDeleteEvent($event)
    {
        // remove and mark as invalid to prevent real deletion
        return $this->softDelete();
        $event->isValid = false;
    }

    /**
     * Soft delete record.
     */
    public function softDelete()
    {
        // set attribute with evaluated timestamp
        $attribute = $this->attribute;
        if ($this->owner->hasAttribute($attribute)) {
            $this->owner->$attribute = $this->getValue(null);
            // save record
            return $this->owner->save(false, [$attribute]);
        }
    }

    /**
     * Restore record.
     */
    public function restore($save = true)
    {
        // set attribute as null
        $attribute = $this->attribute;
        $this->owner->$attribute = null;
        if ($save) {
            // save record
            return $this->owner->save(false, [$attribute]);
        }
    }

    /**
     * Delete record from database regardless of the $safeMode attribute.
     */
    public function forceDelete()
    {
        // store model so that we can detach the behavior
        $model = $this->owner;
        $this->detach();
        // delete as normal
        return $model->delete();
    }

    public function getIsDeleted()
    {
        $attribute = $this->attribute;
        return $this->owner->$attribute !== null;
    }


    /**
     * Evaluate the timestamp to be saved.
     *
     * @param Event|null $event the event that triggers the current attribute updating
     *
     * @return mixed the attribute value
     */
    protected function getValue($event)
    {
        if ($this->value instanceof Expression) {
            return $this->value;
        } else {
            return $this->value !== null ? call_user_func($this->value, $event) : new \yii\db\Expression('NOW()');
        }
    }
}
