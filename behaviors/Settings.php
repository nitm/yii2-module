<?php

namespace nitm\behaviors;

use yii\db\ActiveRecord;

class Settings extends \yii\base\Behavior
{

    //public members
    public $initLocalConfig = false;
    public $initLocalConfigOnEmpty = false;

    public static $initClassConfig = false;
    public static $initClassConfigOnEmpty = false;

    public function events()
    {
        return [
            ActiveRecord::EVENT_INIT => 'initConfig',
        ];
    }

    public function initConfig()
    {
        $nitm = \Yii::$app->getModule('nitm');
        if ($nitm && ((bool) $this->initLocalConfig || (bool) static::$initClassConfig) && $nitm->hasComponent('config') && !$nitm->config->exists($this->owner->isWhat(true), $this->initLocalConfigOnEmpty || static::$initClassConfigOnEmpty)) {
            $container = $this->owner->isWhat(true);
            $container = is_null($container) ? $nitm->config->container : $container;
            if (!$nitm->config->containerExists($container)) {
                $nitm->config->setType($container);
                $config = $nitm->config->getConfig($container, true);
                $nitm->config->set($container, $config);
            }
        }
    }
    /*
   * Initialize configuration
   * @param string $container
   */
   public function init()
   {
       $module = \Yii::$app->getModule('nitm');
       if ($module && $module->hasComponent('config')) {
           $container = $this->owner->is;
           if (!$module->config->containerExists($container)) {
               $module->config->setType($container);
               $config = $module->config->getConfig($container, true);
               $module->config->set($container, $config);
           }
       }
   }

   /**
    * Get a setting value.
    *
    * @param string $setting the locator for the setting
    */
   public function setting($setting = '@')
   {
       $module = \Yii::$app->getModule('nitm');
       if ($module && $module->hasComponent('config')) {
           $hierarchy = explode('.', $setting);
           $isWhat = isset($this->owner) ? $this->owner->isWhat(true) : $module->config->container;
           switch ($hierarchy[0]) {
              case '@':
              array_pop($hierarchy);
              break;

              case $isWhat:
              case null:
              $hierarchy = sizeof($hierarchy) == 1 ? $isWhat : $hierarchy;
              break;

              default:
              array_unshift($hierarchy, $isWhat);
              break;
          }

           return $module->config->get(implode('.', array_filter($hierarchy)));
       }

       return null;
   }
}
