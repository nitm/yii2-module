<?php

namespace nitm\behaviors;

use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\db\Query;
use nitm\helpers\ArrayHelper;
use nitm\helpers\DateFormatter;

class DateAttributes extends \yii\base\Behavior
{
    public $attributes = [];
    public $format;
    public $as;

    public function events()
    {
        return [
            ActiveRecord::EVENT_BEFORE_INSERT => 'timeToString',
            ActiveRecord::EVENT_BEFORE_UPDATE => 'timeToString',
            ActiveRecord::EVENT_AFTER_INSERT => 'setTimeObjects',
            ActiveRecord::EVENT_AFTER_UPDATE => 'setTimeObjects',
            ActiveRecord::EVENT_INIT => 'setTimeObjects',
            ActiveRecord::EVENT_AFTER_FIND => 'setTimeObjects',
        ];
    }

    public function timeToString($event)
    {
        $attributes = array_filter((array)$this->attributes);
        foreach ($attributes as $attribute=>$format) {
            $attribute = is_numeric($attribute) ? $format : $attribute;
            if ($event->sender->{$attribute} instanceof Expression || $event->sender->{$attribute} instanceof Query) {
                continue;
            } elseif ($event->sender->{$attribute} instanceof DateFormatter) {
                $event->sender->setAttribute($attribute, $event->sender->{$attribute}->getValue());
            }
        }
    }

    public function setTimeObjects($event)
    {
        $attributes = array_filter((array)$this->attributes);
        foreach ($attributes as $k=>$v) {
            $options = $v;
            $as = $format = null;
            if (is_array($options)) {
                $as = ArrayHelper::getValue($options, 'as', $this->as);
                $format = ArrayHelper::getValue($options, 'format', $this->format);
                $attribute = $k;
            } else {
                $format = is_numeric($k) ? ($this->format ? $this->format : 'default') : $v;
                $as = is_numeric($k) ? ($this->as ? $this->as : 'asDbTimestamp') : null;
                $attribute = is_numeric($k) ? $v : $k;
            }
            if ($event->sender->hasAttribute($attribute) && !($event->sender->{$attribute} instanceof DateFormatter)) {
                $value = $event->sender->{$attribute};
                $object = new DateFormatter([
                    'value' => $value == 'NOW()' ? strtotime('now') : (string)$value,
                    'format' => $format,
                    'as' => $as
                ]);
                $event->sender->setAttribute($attribute, $object);
                $event->sender->addMethod('set'.$attribute, function ($value) use ($attribute, $event) {
                    if (in_array($event->name, ['init', 'beforeInsert', 'beforeUpdate', 'beforeDelete'])) {
                        if ($value instanceof Expression || $value instanceof Query) {
                            $event->sender->setAttribute($attribute, $value);
                        } else {
                            $event->sender->{$attribute}->setValue((string)$value);
                            $event->sender->{$attribute}->event = $event->name;
                        }
                    } else {
                        $event->sender->{$attribute}->setValue((string)$value);
                        $event->sender->{$attribute}->event = $event->name;
                    }
                });
            }
        }
        return true;
    }
}
