<?php
/**
 * @author Brett O'Donnell <cornernote@gmail.com>
 * @copyright 2015 Mr PHP
 *
 * @link https://github.com/cornernote/yii2-softdelete
 * @description Extended from cornernot softdelete
 *
 * @license BSD-3-Clause https://raw.github.com/cornernote/yii2-softdelete/master/LICENSE.md
 */

namespace nitm\behaviors;

use yii\base\Behavior;
use yii\db\ActiveQuery;

/**
 * SoftDeleteQueryBehavior.
 *
 * @usage:
 * ```
 * public function behaviors() {
 *     return [
 *         [
 *             'class' => 'nitm\behaviors\SoftDeleteQuery',
 *             'attribute' => 'delete_time',
 *         ],
 *     ];
 * }
 * ```
 *
 * @property ActiveQuery $owner
 */
class SoftDeleteQuery extends Behavior
{
    /**
     * @var string SoftDelete attribute
     */
    public $attribute = 'deleted_at';

    /**
     * {@inheritdoc}
     */
    public function events()
    {
        $class = $this->owner->modelClass;
        if (!$class) {
            return [];
        }

        return [
           $class::EVENT_INIT => 'determineScope',
        ];
    }

    public function determineScope()
    {
        $model = $this->owner->primaryModel;
        if ($model) {
            try {
                if ($model->getDeleted) {
                    $this->owner->deleted();
                } else {
                    $this->owner->notDeleted();
                }
            } catch (\Exception $e) {
                throw $e;
            }
        } else {
            $class = $this->owner->modelClass;
            if ($class::getTableSchema()->getColumn($this->attribute)) {
                $this->owner->notDeleted();
            }
        }
    }

    /**
     * @return static
     */
    public function deleted()
    {
        return $this->owner->andWhere(['not', [$this->attribute => null]]);
    }

    /**
     * @return static
     */
    public function notDeleted()
    {
        return $this->owner->andWhere([$this->attribute => null]);
    }
}
