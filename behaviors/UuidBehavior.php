<?php

namespace nitm\behaviors;

use Yii;
use yii\behaviors\AttributeBehavior;
use yii\db\BaseActiveRecord;

class UuidBehavior extends AttributeBehavior
{
    /** @var  string */
    public $attribute = 'uuid';
    public $fromAttribute = 'title';
    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        if (empty($this->attributes)) {
            $this->attributes = [
                BaseActiveRecord::EVENT_BEFORE_INSERT => $this->attribute,
            ];
        }
    }

    /**
     * @param \yii\base\Event $event
     * @return mixed
     */
    protected function getValue($event)
    {
        if ($this->value === null) {
            return $this->generator->uuid5(\Ramsey\Uuid\Uuid::NAMESPACE_OID, $this->owner->{$this->fromAttribute});
        }
        return parent::getValue($event);
    }

    protected function getGenerator()
    {
        return Yii::$app->getModule('nitm')->getUuid();
    }
}
