<?php
/**
 * @author Brett O'Donnell <cornernote@gmail.com>
 * @copyright 2015 Mr PHP
 *
 * @link https://github.com/cornernote/yii2-softdelete
 *
 * @license BSD-3-Clause https://raw.github.com/cornernote/yii2-softdelete/master/LICENSE.md
 */

namespace nitm\behaviors;

use yii\base\Behavior;
use yii\db\ActiveQuery;

/**
 * ListQueryBehavior.
 *
 * @usage:
 * ```
 * public function behaviors() {
 *     return [
 *         [
 *             'class' => 'nitm\behaviors\ListQuery',
 *             'idAttribute' => 'id',
 *             'labelAttribute' => 'title'
 *         ],
 *     ];
 * }
 * ```
 *
 * @property ActiveQuery $owner
 */
class RecordListQuery extends Behavior
{
    /**
     * @var string List id/value attribute
     */
    public $idAttribute = 'id';

     /**
      * @var string List label/display attribute
      */
     public $labelAttribute = 'title';

    public static function findFromRoot()
    {
        $query = static::find();

        $query->andWhere([
             'parent_id' => static::find()
             ->select('id')
             ->where([
                 'slug' => (new static())->isWhat(),
             ]),
         ]);

        return $query;
    }

    /**
     * @return static
     */
    public function asList($id = null, $label = null)
    {
        $id = $id ?: $this->idAttribute;
        $label = $label ?: $this->labelAttribute;

        $result = $this->owner->select([$id, $label])->asArray()->indexBy($id)->all();
        foreach ($result as $key => $item) {
            $result[$key] = $item[$label];
        }

        return $result;
    }

  /**
   * @return static
   */
  public function asJsonList($id = null, $label = null)
  {
      $id = $id ?: $this->idAttribute;
      $label = $label ?: $this->labelAttribute;

      $result = $this->owner->select([$id, $label])->asArray()->indexBy($id)->all();
      foreach ($result as $key => $item) {
          $result[$key] = [
             'id' => $key,
             'value' => $key,
             'text' => $item[$label],
             'name' => $item[$label],
             'label' => \nitm\helpers\Model::getLabel($item, $label),
          ];
      }

      return $result;
  }
}
