<?php

namespace nitm\behaviors;

use nitm\helpers\ArrayHelper;
use nitm\db\ActiveRecord;
use nitm\models\Activity as ActivityModel;

/**
 * Automatically record activity based on model events
 *
 * All items will then be saved as relations to the owner model
 *
 * @property \yii\db\ActiveRecord $owner Description
 *
 * @author Malcolm Paul <malcolm@ninjasitm.com>
 */
class Activity extends \yii\base\Behavior
{
    public $eventMap = [];
    public $actionMap = [];
    public $transformMap = [];
    public $title = '';
    public $activityClass;

    protected $_user;
    protected $_object;
    protected $_actor;
    protected $_action;
    protected $_target;
    protected $_isAdminAction;

    public function events()
    {
        return array_merge([
            ActiveRecord::EVENT_AFTER_INSERT => 'record',
            ActiveRecord::EVENT_AFTER_UPDATE => 'record',
            ActiveRecord::EVENT_AFTER_DELETE => 'record',
         ], $this->eventMap);
    }

    protected function determineAction($event, $action = null)
    {
        if ($action) {
            $scenario = $action;
        } else {
            $scenario = $action = $event->sender->scenario;
        }
        if ($scenario == 'default') {
            if ($event->name === 'afterInsert') {
                $action = 'create';
            } elseif ($event->name === 'afterUpdate') {
                $action = 'update';
            } elseif ($event->name === 'afterDelete') {
                $action = 'delete';
            }
        }

        return $action;
    }

    public function getActivity()
    {
        return $this->owner->hasMany(ActivityModel::class, ['user_id' => 'id']);
    }

    /**
     * [record description]
     * @param  [type] $action [description]
     * @param  [type] $actor  [description]
     * @param  [type] $target [description]
     * @return [type]         [description]
     */
    public function record($event, $action = null, $actor = null, $target = null)
    {
        $action = $this->determineAction($event, $action);

        $activityClass = class_exists($this->activityClass) && is_subclass_of($this->activityClass, static::class) ? $this->activityClass : static::class;
        $activity = $activityClass == static::class ? $this : new $activityClass;
        if ($model = $activity->prepare($action, $this->owner)) {
            if ($model->validate()) {
                $model->save();
                $this->finish();
            } else {
                \Yii::warning(new \Exception($model->formatErrors()));
            }
        }
    }

    /**
     * [prepare description]
     * @param  [type] $action [description]
     * @param  [type] $object [description]
     * @param  [type] $actor  [description]
     * @param  [type] $target [description]
     * @return [type]         [description]
     */
    public function prepare($action, $object = null, $actor = null, $target = null)
    {
        if (!$this->getUser()) {
            return false;
        }
        $this->action = $action;
        $object = $object ?: $this->owner;
        $this->actor = $this->formatActor($actor ?: $this->getUser());
        $this->object = $this->formatObject($object);
        if ($target) {
            $this->target = $this->formatTarget($target);
        }

        $this->title = $this->getTitleString();

        $model = new ActivityModel([
             'verb' => $this->action,
             'actor' => $this->actor,
             'object' => $this->object,
             'title' => $this->title,
             'target' => $this->target,
             'user_id' => $this->getUser()->id,
             'is_admin_action' => $this->isAdminAction,
             'object_type' => $this->transform('remoteType', $this->owner),
             'object_class' => $this->object['_model']['class'],
             'object_id' => $this->object['_model']['id'],
             'target_type' => ArrayHelper::getValue($this->target, 'type'),
             'target_class' => ArrayHelper::getValue($this->target, '_model.class'),
             'target_id' => ArrayHelper::getValue($this->target, '_model.id'),
        ]);

        return $model;
    }

    /**
     * [finish description]
     * @return [type] [description]
     */
    public function finish()
    {
        $this->action = $this->target = $this->object = null;
    }

    /**
     * [getModel description]
     * @return [type] [description]
     */
    public function getModel()
    {
        return $this->owner;
    }

    /**
     * [getActionName description]
     * @return [type] [description]
     */
    public function getActionName()
    {
        /**
         * If there is a map for the specified action the user it otherwise use the current action
         * @var [type]
         */
        $action = ArrayHelper::getValue($this->actionMap, $this->action, $this->action);
        $endsWith = substr($action, strlen($action) - 1);
        if (in_array($endsWith, ['e'])) {
            return $action.'d';
        } elseif (in_array($endsWith, ['y'])) {
            return substr($action, 0, strlen($action) - 1).'ied';
        } elseif (in_array($endsWith, ['d'])) {
            return $action;
        } else {
            return $action.'ed';
        }
    }

    /**
     * [getActionString description]
     * @return [type] [description]
     */
    protected function getActionString()
    {
        switch ($this->getActionName()) {
            case 'created':
                return $this->getActionName().' new';
            break;

            default:
                return $this->getActionName();
            break;
        }
    }

    /**
     * [getTitleString description]
     * @return [type] [description]
     */
    public function getTitleString()
    {
        return $this->formatTitle();
    }

    /**
     * [formatTitle description]
     * @return [type] [description]
     */
    public function formatTitle()
    {
        $objectType = ArrayHelper::getValue($this->object, 'type');

        if ($objectType == 'user') {
            $objectName = '@'.ArrayHelper::getValue($this->object, 'name');
        } else {
            $objectName = ArrayHelper::getValue($this->object, 'fullName');
            if (!$objectName) {
                $objectName = ArrayHelper::getValue($this->object, 'name');
            }
            $objectName = '#'.$objectName;
        }

        $actorName = ArrayHelper::getValue($this->actor, 'name', ArrayHelper::getValue($this->actor, 'id'));
        if ($this->actor['type'] == 'user') {
            $actorName = '@'.$actorName;
        } else {
            $actorName = '#'.$actorName;
        }

        $parts = [
            $actorName,
            $this->getActionString(),
        ];

        if ($objectType != ArrayHelper::getValue($this->actor, 'type')) {
            array_push($parts, $objectType, $objectName);
        } else {
            array_push($parts, $objectName);
        }

        return implode(' ', $parts);
    }
    /**
     * [getTitle description]
     * @return [type] [description]
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * [setAction description]
     * @param [type] $actor [description]
     */
    protected function setAction($action)
    {
        $this->_action = $action;
    }

    /**
     * [getAction description]
     * @return [type] [description]
     */
    public function getAction()
    {
        return $this->_action;
    }

    /**
     * [setActor description]
     * @param [type] $actor [description]
     */
    protected function setActor($actor)
    {
        $this->_actor = $actor;
    }

    /**
     * [getActor description]
     * @return [type] [description]
     */
    public function getActor()
    {
        return $this->_actor;
    }

    /**
     * [setObject description]
     * @param [type] $object [description]
     */
    protected function setObject($object)
    {
        $this->_object = $object;
    }

    /**
     * [getObject description]
     * @return [type] [description]
     */
    public function getObject()
    {
        return $this->_object;
    }

    /**
     * [setTarget description]
     * @param [type] $target [description]
     */
    protected function setTarget($target)
    {
        $this->_target = $target;
    }

    /**
     * [getTarget description]
     * @return [type] [description]
     */
    public function getTarget()
    {
        return $this->_target;
    }

    /**
     * Format the activity object.
     *
     * @param BaseContent $model The target
     * @param bool        $force Should we force formatting?
     *
     * @return array the formatted activity object
     */
    public function formatObject($model, $force = false)
    {
        if (!isset($this->_object) || $force) {
            $this->object = $this->formatObjectOrTarget($model);
        }

        return $this->object;
    }

    /**
     * Format the activity target.
     *
     * @param BaseContent $model The target
     * @param bool        $force Should we force formatting?
     *
     * @return array the formatted activity target
     */
    protected function formatTarget($model, $force = false)
    {
        if (!isset($this->_target) || $force) {
            $this->target = $this->formatObjectOrTarget($model);
        }

        return $this->target;
    }

    /**
     * Format the activity object.
     *
     * @param BaseContent $model The object
     *
     * @return array the formatted activity object
     */
    protected function formatObjectOrTarget($model)
    {
        if ($model instanceof \nitm\models\User) {
            return $this->formatUser($model);
        } else {
            if (is_array($model)) {
                return $model;
            } elseif (is_object($model)) {
                return [
                   'id' => $model->publicId,
                   'type' => $this->transform('type', $model, $model->is),
                   'url' => $model->publicId,
                   'name' => $model->title(),
                   'image' => [
                      'type' => 'link',
                      'url' => $this->getImage($model),
                   ],
                   'data' => $model->toArray(),
                   '_model' => [
                      'id' => $model->id,
                      'class' => get_class($model),
                   ],
                ];
            }
        }
    }

    protected function getImage($model)
    {
        try {
            return $model->image && !$model->image->isNewRecord ? \nitm\filemanager\helpers\ImageHelper::createThumbnails($model->image, 256, 256) : '';
        } catch (\Exception $e) {
            return '';
        }
    }

    public function transform($key, $model, $default = null)
    {
        if (in_array($key, array_keys($this->transformMap))) {
            return $this->transformMap[$key]($model);
        } else {
            return $default ?: $model->is;
        }
    }

    /**
     * Format the actor User object.
     *
     * @param User $model The actor
     * @param bool $force Should we force formatting?
     *
     * @return array the formatted actor object
     */
    protected function formatActor($model, $force = false)
    {
        $this->setIsAdminAction($model);
        if (!isset($this->_actor) || $force) {
            $this->actor = $this->formatUser($model);
        }

        return $this->actor;
    }

    /**
     * Format the user User object.
     *
     * @param User $model The actor
     * @param bool $force Should we force formatting?
     *
     * @return array the formatted actor object
     */
    protected function formatUser($model)
    {
        if (is_array($model)) {
            return $model;
        } elseif ($model instanceof \nitm\models\User) {
            if ($model instanceof \nitm\cms\models\Admin) {
                $model = $model->publicUser;
            }
            $id = $model->username ?: $model->id;

            return [
                'id' => 'user/'.$id,
                'type' => 'user',
                'url' => '/user/'.$id,
                'image' => [
                    'type' => 'link',
                    'url' => $model->avatar(),
                ],
                'name' => $id,
                'fullName' => $model->fullName,
                'data' => $model->toArray(['id', 'avatar', 'fullName', 'profile', 'isConfirmed', 'roleName']),
                '_model' => [
                   'id' => $model->id,
                   'class' => get_class($model),
                ],
             ];
        }
    }

    /**
     * [getIsAdminAction description]
     * @return [type] [description]
     */
    public function getIsAdminAction()
    {
        return $this->_isAdminAction;
    }

    /**
     * [setIsAdminAction description]
     */
    protected function setIsAdminAction()
    {
        $this->_isAdminAction = $this->getUser()->isAdmin;
    }

    /**
     * [setUser description]
     * @param \nitm\models\User $user The user model
     */
    protected function setUser($user)
    {
        $this->_user = $user;
    }

    /**
     * [getUser description]
     * @return \nitm\models\User User Model
     */
    protected function getUser()
    {
        if (!isset($this->_user)) {
            $this->_user = \Yii::$app->user->identity;
        }
        return $this->_user;
    }
}
