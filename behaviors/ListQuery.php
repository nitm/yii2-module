<?php
/**
 * @author Brett O'Donnell <cornernote@gmail.com>
 * @copyright 2015 Mr PHP
 *
 * @link https://github.com/cornernote/yii2-softdelete
 *
 * @license BSD-3-Clause https://raw.github.com/cornernote/yii2-softdelete/master/LICENSE.md
 */

namespace nitm\behaviors;

use yii\base\Behavior;
use yii\db\ActiveQuery;

/**
 * ListQueryBehavior.
 *
 * @usage:
 * ```
 * public function behaviors() {
 *     return [
 *         [
 *             'class' => 'nitm\behaviors\ListQuery',
 *             'idAttribute' => 'id',
 *             'labelAttribute' => 'title'
 *         ],
 *     ];
 * }
 * ```
 *
 * @property ActiveQuery $owner
 */
class ListQuery extends Behavior
{
    /**
     * @var string List id/value attribute
     */
    public $idAttribute = 'id';

     /**
      * @var string List label/display attribute
      */
     public $labelAttribute = 'title';

    /**
     * @return static
     */
    public function getList($id = null, $label = null)
    {
        $id = $id ?: $this->idAttribute;
        $label = $label ?: $this->labelAttribute;

        $result = $this->owner->select([$id, $label])->asArray()->all();

        return $this->owner->andWhere($this->attribute.' IS NOT NULL');
    }
}
