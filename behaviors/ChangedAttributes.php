<?php

namespace nitm\behaviors;

use yii\db\ActiveRecord;
use nitm\helpers\ArrayHelper;

class ChangedAttributes extends \yii\base\Behavior
{
    protected $_changedAttributes;
    public function events()
    {
        return [
            ActiveRecord::EVENT_AFTER_INSERT => 'updateChangedAttributes',
            ActiveRecord::EVENT_AFTER_UPDATE => 'updateChangedAttributes',
            ActiveRecord::EVENT_BEFORE_INSERT => 'resetChangedAttributes',
            ActiveRecord::EVENT_BEFORE_UPDATE => 'resetChangedAttributes',
        ];
    }

    public function updateChangedAttributes($event)
    {
        $event->sender->changedAttributes = $event->changedAttributes;
    }

    public function resetChangedAttributes($event)
    {
        $event->sender->changedAttributes = [];
        return true;
    }

    public function getChangedAttributes($names = null)
    {
        return ArrayHelper::getValue($this->_changedAttributes, $names, $this->_changedAttributes);
    }

    public function setChangedAttributes($attributes = null)
    {
      if($attributes) {
        $this->_changedAttributes = $attributes;
      }
    }

    public function isChangedAttribute($param)
    {
        return isset($this->_changedAttributes[$param]);
    }

    /**
     * Check to see if the specified attributes have been changed.
     *
     * @param array|string $params [description]
     * @param [type]       $filter Exclude these attributes from the check
     *
     * @return bool Result
     */
    public function isOnlyChangedAttribute($params, $filter = [])
    {
        if (empty($this->_changedAttributes)) {
            return true;
        }

        $params = (array) $params;
        if (ArrayHelper::isIndexed($params)) {
            $params = array_flip($params);
        }
        if (ArrayHelper::isIndexed($filter)) {
            $filter = array_flip($filter);
        }

        $changedAttributes = array_diff_key($this->_changedAttributes, $filter);

        //If any other parameter was specified then return false
        if (count(array_diff_key($changedAttributes, $params)) >= 1) {
            return false;
        }

        return true;
    }

    public function getHasNoChangedAttributes()
    {
        return count(array_filter($this->_changedAttributes)) == 0;
    }
}
