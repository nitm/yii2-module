<?php

namespace nitm\behaviors;

use nitm\controllers\BaseController as Controller;
use nitm\helpers\Model as ModelHelper;

class Breadcrumbs extends \yii\base\Behavior
{
    public $events;
    public $baseUrl;

    public function events()
    {
        return [
            Controller::EVENT_BEFORE_ACTION => 'beforeAction',
        ];
    }

    public function beforeAction()
    {
        if (!\Yii::$app->request->isAjax) {
            if ($this->owner->hasProperty('model') && $this->owner->model) {
                foreach ($this->allEvents as $event) {
                    \yii\base\Event::on($this->owner->className(), $event, function ($event) {
                        $this->owner->view->params['breadcrumbs'] = $this->getLinks();
                        $event->handled = true;
                    });
                }
            }
        }
    }

    protected function getAllEvents()
    {
        if (!$this->events) {
            return [
                Controller::EVENT_AFTER_CREATE,
                Controller::EVENT_AFTER_UPDATE,
                Controller::EVENT_AFTER_FIND,
            ];
        }

        return $this->events;
    }

    protected function getLinks()
    {
        $links = [];
        if ($this->owner->hasProperty('model') && $this->owner->model) {
            $links = [
               [
                 'label' => $this->owner->model->properName(),
                 'url' => '/'.$this->baseUrl.'/'.$this->owner->id,
               ],
            ];
            if (\Yii::$app->request->referrer && \Yii::$app->request->referrer != $links[0]['url']) {
                array_unshift($links, [
                  'label' => 'Previous',
                  'url' => \Yii::$app->request->referrer,
               ]);
            }
            if (!$this->owner->model->isNewRecord) {
                $links[] = [
                    'label' => ucfirst($this->owner->model->scenario).' '.ModelHelper::getTitle($this->owner->model),
                    'url' => ModelHelper::resolveUrl($this->owner->model, [
                        'baseUrl' => $this->baseUrl,
                        'mask' => '/form',
                    ]),
                ];
            }
        }

        return $links;
    }
}
