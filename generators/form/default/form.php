<?php
/**
 * This is the template for generating an action view file.
 */

/* @var $this yii\web\View */
/* @var $generator nitm\generators\form\Generator */

echo "<?php\n";
?>

use nitm\helpers\Html;

/* @var $this yii\web\View */
/* @var $model <?= $generator->modelClass ?> */
/* @var $form ActiveForm */

$defaultFormOptions = [
    'type' => 'vertical'
];
$formOptions = isset($formOptions) ? array_merge($defaultFormOptions, $formOptions) : $defaultFormOptions;
$formOptions['options']['role'] = 'ajaxForm';

$formClass = \Yii::$container->get('kartik\widgets\ActiveForm');

<?= "?>" ?>

<div class="<?= str_replace('/', '-', trim($generator->viewName, '_')) ?>">

    <?= "<?php " ?>$form = include(\Yii::getAlias("@nitm/views/layouts/form/header.php")); ?>

    <?php foreach ($generator->getModelAttributes() as $attribute): ?>
    <?= "<?= " ?>$form->field($model, '<?= $attribute ?>') ?>
    <?php endforeach; ?>

        <div class="form-group">
            <?= "<?= " ?>Html::submitButton() ?>
        </div>
    <?= "<?php " ?>$formClass::end(); ?>

</div><!-- <?= str_replace('/', '-', trim($generator->viewName, '-')) ?> -->
