<?php

namespace nitm\grid;

use Yii;
use nitm\helpers\Html;
use yii\helpers\Inflector;
use nitm\helpers\ArrayHelper;
use nitm\helpers\WidgetHElper;
use nitm\helpers\Icon;

/**
 * This action column supports setting the icon framework to use.
 *
 * @why Karttik TabularForm needs ActionColumn to be derived from \kartik\grid\ActionColumn
 *
 * @author Malcolm Paul <malcoLM@ninjasitm.com>
 *
 * @since 2.0
 */
class KartikActionColumn extends \kartik\grid\ActionColumn
{
    use \nitm\traits\GridActionColumnTrait;
}
