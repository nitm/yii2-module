<?php

namespace nitm\grid;

use Yii;
use nitm\helpers\Html;
use yii\helpers\Inflector;
use nitm\helpers\ArrayHelper;
use nitm\helpers\WidgetHElper;
use nitm\helpers\Icon;

/**
 *  This action column supposrts setting the icon framework to use.
 *
 * @author Malcolm Paul <malcoLM@ninjasitm.com>
 *
 * @since 2.0
 */
class ActionColumn extends \yii\grid\ActionColumn
{
    use \nitm\traits\GridActionColumnTrait;
}
