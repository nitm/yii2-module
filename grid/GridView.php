<?php

/**
 * @package   yii2-grid
 * @author    Kartik Visweswaran <kartikv2@gmail.com>
 * @copyright Copyright &copy; Kartik Visweswaran, Krajee.com, 2014 - 2016
 * @version   3.1.4
 */

namespace nitm\grid;

use kartik\base\Config;
use kartik\dialog\Dialog;
use kartik\grid\GridView as BaseGridView;
use nitm\helpers\Icon;
use Yii;
use yii\base\InvalidConfigException;
use yii\bootstrap\ButtonDropdown;
use yii\bootstrap\Button;
use yii\grid\Column;
use yii\helpers\ArrayHelper;
use nitm\helpers\WidgetHelper;
use nitm\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\web\View;
use yii\widgets\Pjax;

/**
 * Extended gridview widget for NITM to support some usefulfunctionality
 *
 * @author Malcolm pauul <malcolm@ninjasitm.com>
 * @since  1.0
 */
class GridView extends BaseGridView
{
    public $export = false;

    /**
     * Support batch functionlaity
     * @var boolean
     */
    public $enableBatchOperations = false;

    /**
     * The batch operations supported by this grid. In the following format
     * {{operation}} can be either a string or an array. If it is a string the built in operation that matches will be used
     * [
     *      {{operation}} => [
     *          'label' => string,
     *          'script' => The javascript that is used for this action
     *      ]
     * ]
     * @var array
     */
    public $operations = [];

    public $pjaxSettings = [
        'options' => [
            'linkSelector' => 'a[data-pjax=1]',
            'formSelector' => 'form[data-pjax=1]'
        ],
    ];

    public function init()
    {
        /**
         * Add default NITM actions if action column is present
         */
        $nitmButtons = \nitm\helpers\Grid::getDefaultActions()['buttons'];
        foreach ($this->columns as $idx=>$column) {
            $class = ArrayHelper::getValue((array)$column, 'class');
            if ($class && is_a($class, \yii\grid\ActionColumn::class, true)) {
                $buttons = ArrayHelper::getValue($column, 'buttons', []);
                $column['buttons'] = $buttons;
                $column['class'] = $class == \yii\grid\ActionColumn::class ? ActionColumn::class : $class;
                foreach ($nitmButtons as $url=>$button) {
                    $directUrl = (($formPos = strpos($url, 'form/')) !== false) ? substr($url, strlen('form/')) : $url;
                    if ($formPos !==false) {
                        $url = $directUrl;
                    }
                    $column['buttons'][$url] = $button;
                }
                $this->columns[$idx] = $column;
            }
        }
        parent::init();
    }

    /**
     * @inheritdoc
     * @throws InvalidConfigException
     */
    public function run()
    {
        if ($this->enableBatchOperations) {
            $this->setupBatchOperations();
        }
        if (empty($this->rowOptions)) {
            $this->rowOptions = function ($model, $key, $index, $grid) {
                return [
                    'id' => $model->isWhat().$model->getId(),
                ];
            };
        }
        return parent::run();
    }

    protected function setupBatchOperations()
    {
        $items = [];
        $this->operations = empty($this->operations) ? $this->getSupportedOperations() : $this->operations;
        foreach ((array)$this->operations as $operation=>$options) {
            if (is_string($options)) {
                $options = ArrayHelper::getValue($supportedOperations, $options, null);
            }
            if (is_array($options)) {
                $script = ArrayHelper::remove($options, 'script', null);
                if ($script) {
                    $this->getView()->registerJs($script instanceof JsExpression ? $script : new JsExpression($script));
                }
                $items[] = is_array($options) ? array_merge($options, [
                    'url' => '#',
                    'encodeLabel' => false
                ]) : [
                    'label' => $options,
                    'url' => '#',
                    'encodeLabel' => false
                ];
            }
        }

        $batchActionsId = 'batch-options'.uniqid();
        $operations = [
            [
                'content' => WidgetHelper::widget(ButtonDropdown::class, [
                    'encodeLabel' => false,
                    'label' => 'With Selected&nbsp;&nbsp;',
                    'dropdown' => [
                        'encodeLabels' => false,
                        'items' => $items
                    ],
                    'options' => [
                        'id' => $batchActionsId,
                        'class' => 'disabled btn btn-disabled',
                        'role' => 'batchOperations'.$batchActionsId
                    ]
                ])
            ],
            '{toggleData}'
        ];
        $this->toolbar = $operations;

        $this->getView()->registerJs(new JsExpression('function updateBatchButton() {
            let count = $(\'#'.$this->options['id'].'\').yiiGridView("getSelectedRows").length,
                $button = $(\'[role="batchOperations'.$batchActionsId.'"]\');
            if(!count) {
                $button.removeClass("btn-primary").addClass("disabled");
            } else {
                $button.removeClass("disabled").addClass("btn-primary");
            }
        }
        updateBatchButton();
        $(\'.kv-row-select input[name="selection[]"]\').on("change", function (event) {
            updateBatchButton();
        });
        $(\'#'.$this->options['id'].' tr\').on("click", function(e) {
            $(this).find(\'input[name="selection[]"]\').click();
        });
        '));
    }

    protected function getSupportedOperations()
    {
        $context = \Yii::$app->controller;
        $action = '/'.$context->module->id.'/'.$context->id.'/batch?type=delete';
        return [
            'delete' => [
                'encodeLabel' => false,
                'label' => Html::a(Icon::forAction('delete').' Delete', '#', [
                    'class'=> 'nitm-batch-delete'
                ]),
                'options' => [
                    'onClick' => new JsExpression('(function(e) {
                        e.preventDefault();
                        let $form = $(\''.$this->options['data-form'].'\'),
                            $grid = $form.find(\'#'.$this->options['id'].'\');
                        let keys = $grid.yiiGridView("getSelectedRows").length;
                        if(keys) {
                            if(window.confirm("Are you sure you want to delete these "+keys+" items?")) {
                                $form.attr(\'action\', \''.$action.'\');
                                $form.submit();
                            }
                        } else {
                            alert("You need to select at least one item for batch deleting");
                        }
                    })(event)')
                ]
            ]
        ];
    }
}
