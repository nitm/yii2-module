<?php

use yii\db\Migration;

class m161114_010628_create_categories extends Migration
{
    public function safeUp()
    {
        $tableSchema = \Yii::$app->db->getTableSchema('nitm_categories');
        if ($tableSchema) {
            return true;
        }
        $this->createTable('nitm_categories', [
              'id' => $this->primaryKey(),
              'author_id' => $this->integer(),
              'remote_type' => $this->string(64),
              'remote_for' => $this->string(64),
              'remote_id' => $this->integer(),
              'user_id' => $this->integer(),
              'action' => $this->string(32),
              'methods' => $this->string(128),
              'priority' => $this->string(16),
              'global' => $this->boolean()->defaultValue(false),
              'disabled' => $this->boolean()->defaultValue(false),
              'created_at' => $this->timestamp()->defaultExpression('NOW()'),
          ]);
    }

    public function safeDown()
    {
        return $this->dropTable('nitm_categories');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
