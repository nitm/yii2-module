<?php

use yii\db\Schema;

class m140312_212247_logs extends \yii\db\Migration
{
    public function safeUp()
    {
        $tableSchema = \Yii::$app->db->getTableSchema('nitm_logs');
        if ($tableSchema) {
            return true;
        }

        $this->createTable('nitm_logs', [
            'id' => $this->primaryKey(),
            'message' => $this->text(),
            'level' => $this->integer(1),
            'prefix' => $this->string(64),
            'internal_category' => $this->string(32),
            'category' => $this->string(32),
            'created_at' => $this->timestamp()->defaultExpression('NOW()'),
            'log_time' => $this->integer(),
            'traces' => $this->text(),
            'memory_usage' => $this->integer(),
            'response_status_code' => $this->integer(),
            'response_status_text' => $this->string(64),
            'response_headers' => $this->text(),
            'response_data' => $this->text(),
            'request_headers' => $this->text(),
            'request_data' => $this->text(),
            'request_method' => $this->string(10),
            'action' => $this->string(32),
            'db_name' => $this->string(32),
            'table_name' => $this->string(32),
            'user' => $this->string(12),
            'user_id' => $this->integer(),
            'ip_addr' => $this->string(32),
            'host' => $this->string(128),
            'user_agent' => $this->string(128),
            'cookie_id' => $this->text(),
            'fingerprint' => $this->text(),
            'error_level' => $this->string(16),
            'ua_family' => $this->string(64),
            'ua_version' => $this->string(64),
            'os_family' => $this->string(64),
            'os_version' => $this->string(64),
            'device_family' => $this->string(64),
          ]);
    }

    public function safeDown()
    {
        echo "m140312_212247_logs cannot be reverted.\n";
        return true;
    }
}
