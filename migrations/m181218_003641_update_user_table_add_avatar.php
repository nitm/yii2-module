<?php

use yii\db\Migration;

/**
 * Class m181218_003641_update_user_table_add_avatar
 */
class m181218_003641_update_user_table_add_avatar extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%user}}', 'avatar', $this->text()->null());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%user}}', 'avatar');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181218_003641_update_user_table_add_avatar cannot be reverted.\n";

        return false;
    }
    */
}
