<?php

use yii\db\Schema;
use yii\helpers\ArrayHelper;

class m140312_212206_config extends \yii\db\Migration
{
    public $tableOptions = '';

    private $_foreignOptions = [
        'onUpdate' => 'CASCADE',
        'onDelete' => 'RESTRICT',
    ];

    public function safeUp()
    {
        /*
         * Create containers
         */
        $this->createTable('nitm_config_containers', [
            'id' => $this->primaryKey(),
            'name' => $this->string(128),
            'author' => $this->integer(),
            'editor' => $this->integer()->null(),
            'created_at' => $this->timestamp()->defaultExpression("NOW()"),
            'updated_at' => $this->timestamp()->null(),
            'deleted' => $this->boolean()->defaultValue(false),
        ]);

        $this->createIndex('unique_container_name', 'nitm_config_containers', [
            'name'
        ], true);

        //These Dbs don't support foreign keys
        if(!in_array(get_class($this->db->schema), [
          \yii\db\sqlite\Schema::class
        ])) {
          $this->addForeignKey('foreignContainerAuthor', '{{%nitm_config_containers}}', 'author', '{{%user}}', 'id', 'CASCADE', 'RESTRICT');
          $this->addForeignKey('foreignContainerEditor', '{{%nitm_config_containers}}', 'editor', '{{%user}}', 'id', 'CASCADE', 'RESTRICT');
        }

        /**
         * Create sections
         * @var [type]
         */
        $this->createTable('nitm_config_sections', [
            'id' => $this->primaryKey(),
            'containerid' => $this->integer(),
            'name' => $this->string(128),
            'author' => $this->integer(),
            'editor' => $this->integer()->null(),
            'created_at' => $this->timestamp()->defaultExpression("NOW()"),
            'updated_at' => $this->timestamp()->null(),
            'deleted' => $this->boolean()->defaultValue(false),
        ]);

        $this->createIndex('unique_section_name', 'nitm_config_sections', [
            'name', 'containerid'
        ], true);

        //These Dbs don't support foreign keys
        if(!in_array(get_class($this->db->schema), [
          \yii\db\sqlite\Schema::class
        ])) {
          $this->addForeignKey('foreignSectionAuthor', '{{%nitm_config_sections}}', 'author', '{{%user}}', 'id', 'CASCADE', 'RESTRICT');
          $this->addForeignKey('foreignSectionEditor', '{{%nitm_config_sections}}', 'editor', '{{%user}}', 'id', 'CASCADE', 'RESTRICT');
          $this->addForeignKey('foreignSectionContainer', '{{%nitm_config_sections}}', 'containerid', '{{%nitm_config_containers}}', 'id', 'CASCADE', 'RESTRICT');
        }

        /**
         * Create values
         * @var [type]
         */
        $this->createTable('nitm_config_values', [
            'id' => $this->primaryKey(),
            'sectionid' => $this->integer(),
            'containerid' => $this->integer(),
            'name' => $this->string(128),
            'value' => $this->text(),
            'comment' => $this->text()->null(),
            'author' => $this->integer(),
            'editor' => $this->integer()->null(),
            'created_at' => $this->timestamp()->defaultExpression("NOW()"),
            'updated_at' => $this->timestamp()->null(),
            'deleted' => $this->boolean()->defaultValue(false),
        ]);

        $this->createIndex('unique_value_name', 'nitm_config_values', [
            'name', 'sectionid', 'containerid'
        ], true);

        //These Dbs don't support foreign keys
        if(!in_array(get_class($this->db->schema), [
          \yii\db\sqlite\Schema::class
        ])) {
          $this->addForeignKey('foreignValueAuthor', '{{%nitm_config_values}}', 'author', '{{%user}}', 'id', 'CASCADE', 'RESTRICT');
          $this->addForeignKey('foreignValueEditor', '{{%nitm_config_values}}', 'editor', '{{%user}}', 'id', 'CASCADE', 'RESTRICT');
          $this->addForeignKey('foreignValueSection', '{{%nitm_config_values}}', 'sectionid', '{{%nitm_config_sections}}', 'id', 'CASCADE', 'RESTRICT');
          $this->addForeignKey('foreignValueContainer', '{{%nitm_config_values}}', 'containerid', '{{%nitm_config_containers}}', 'id', 'CASCADE', 'RESTRICT');
        }
    }

    public function safeDown()
    {
        /*
         * Only delete if the tables are empty
         */
        foreach (array_keys($this->_tables) as $table) {
            switch (!$this->getTableSchema($table)->count()) {
                case true:
                $this->dropTable($table);
                break;

                default:
                echo "Not dropping '$table' becuase there is data. Empty this table first";
                break;
            }
        }

        return true;
    }
}
