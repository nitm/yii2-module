<?php

use yii\db\Migration;

class m171205_013806_create_activity_table extends Migration
{
    public function safeUp()
    {
        $tableSchema = \Yii::$app->db->getTableSchema('nitm_activity');
        if ($tableSchema) {
            return true;
        }
        $this->createTable('nitm_activity', [
            'id' => $this->bigPrimaryKey(),
            'title' => $this->string(),
            'verb' => $this->string()->notNull(),
            'created_at' => $this->timestamp(),
            'actor' => $this->text()->null(),
            'object' => $this->text()->null(),
            'target' => $this->text()->null(),
            'user_id' => $this->integer(),
            'is_admin_action' => $this->boolean()->defaultValue(false),
            'updated_at' => $this->timestamp()->null(),
            'object_type' => $this->string(),
            'object_class' => $this->text(),
            'object_id' => $this->integer(),
            'target_type' => $this->string()->null(),
            'target_class' => $this->string()->null(),
            'target_id' => $this->integer()->null()
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('nitm_activity');
        // echo "m171205_013806_create_activity_table cannot be reverted.\n";

        return true;
        return false;
    }
}
