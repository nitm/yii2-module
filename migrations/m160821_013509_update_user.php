<?php

use yii\db\Migration;
use yii\db\Schema;

class m160821_013509_update_user extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%user}}', 'last_active_at', Schema::TYPE_INTEGER.' NOT NULL DEFAULT 0');
        $this->addColumn('{{%user}}', 'logged_in_at', Schema::TYPE_INTEGER.' NOT NULL DEFAULT 0');
    }

    public function safeDown()
    {
        echo "m160821_013509_update_user cannot be reverted.\n";

        return false;
    }
}
