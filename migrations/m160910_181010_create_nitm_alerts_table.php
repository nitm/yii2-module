<?php

use yii\db\Migration;

/**
 * Handles the creation of table `nitm_alerts`.
 */
class m160910_181010_create_nitm_alerts_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $tableSchema = \Yii::$app->db->getTableSchema('nitm_alerts');
        if ($tableSchema) {
            return true;
        }
        $this->createTable('nitm_alerts', [
            'id' => $this->primaryKey(),
            'title' => $this->string(255),
            'slug' => $this->string(255),
            'type_id' => $this->integer(),
            'parent_id' => $this->integer(),
            'html_icon' => $this->string(32),
            'created_at' => $this->timestamp()->defaultExpression('NOW()'),
            'updated_at' => $this->timestamp()->defaultExpression('NOW()'),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropTable('nitm_alerts');
    }
}
