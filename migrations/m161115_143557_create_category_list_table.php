<?php

use yii\db\Schema;
use yii\db\Migration;

class m161115_143557_create_category_list_table extends Migration
{
    public function safeUp()
    {
        $tableSchema = \Yii::$app->db->getTableSchema('nitm_category_list');
        if ($tableSchema) {
            return true;
        }
        $this->createTable('nitm_category_list', [
            'id' => 'pk',
            'author_id' => Schema::TYPE_INTEGER.' NOT NULL',
            'created_at' => Schema::TYPE_TIMESTAMP.' NOT NULL DEFAULT NOW()',
            'editor_id' => Schema::TYPE_INTEGER.' NULL',
            'updated_at' => Schema::TYPE_TIMESTAMP.' NULL',
            'remote_id' => Schema::TYPE_INTEGER.' NOT NULL',
            'remote_type' => Schema::TYPE_STRING.'(32) NOT NULL',
            'remote_class' => Schema::TYPE_TEXT.' NOT NULL',
            'category_id' => Schema::TYPE_INTEGER.' NOT NULL',
            'priority' => Schema::TYPE_INTEGER.' NULL',
            'deleted' => Schema::TYPE_BOOLEAN.' NULL DEFAULT false',
            'deleted_at' => Schema::TYPE_TIMESTAMP.' NULL',
            'deleted_by' => Schema::TYPE_INTEGER.' NULL',
            'disabled' => Schema::TYPE_BOOLEAN.' NULL DEFAULT false',
            'disabled_at' => Schema::TYPE_TIMESTAMP.' NULL',
            'disabled_by' => Schema::TYPE_INTEGER.' NULL',
        ]);

        $this->createIndex('nitm_category_list_category_id', '{{%nitm_category_list}}', ['category_id']);
        $this->createIndex('nitm_category_list_remote_id', '{{%nitm_category_list}}', ['remote_id']);
        $this->createIndex('nitm_category_list_unique', '{{%nitm_category_list}}', [
            'remote_id', 'remote_type', 'category_id',
        ], true);

        //These Dbs don't support foreign keys
        if(!in_array(get_class($this->db->schema), [
          \yii\db\sqlite\Schema::class
        ])) {
          $this->addForeignKey('fk_nitm_category_list_author', '{{%nitm_category_list}}', 'author_id', '{{%user}}', 'id', 'CASCADE', 'RESTRICT');
          $this->addForeignKey('fk_nitm_category_list_editor', '{{%nitm_category_list}}', 'editor_id', '{{%user}}', 'id', 'CASCADE', 'RESTRICT');
          $this->addForeignKey('fk_nitm_category_list_category', '{{%nitm_category_list}}', 'category_id', '{{%nitm_categories}}', 'id', 'CASCADE', 'RESTRICT');
        }

        /**
         * Create the metadata table.
         */
        $this->createTable('nitm_category_list_metadata', [
            'id' => 'pk',
            'content_id' => Schema::TYPE_INTEGER.' NOT NULL',
            'key' => Schema::TYPE_STRING.'(32) NOT NULL',
            'value' => Schema::TYPE_TEXT.' NOT NULL',
            'created_at' => Schema::TYPE_TIMESTAMP.' NOT NULL DEFAULT NOW()',
            'updated_at' => Schema::TYPE_TIMESTAMP.' NULL',
        ]);

        //These Dbs don't support foreign keys
        if(!in_array(get_class($this->db->schema), [
          \yii\db\sqlite\Schema::class
        ])) {
          $this->addForeignKey('fk_nitm_category_list_metadata', '{{%nitm_category_list_metadata}}', 'content_id', '{{%nitm_category_list}}', 'id', 'CASCADE', 'RESTRICT');
        }
    }

    public function safeDown()
    {
        echo "m150314_143557_create_nitm_category_list_table cannot be reverted.\n";

        return true;
    }
}
