<?php

namespace nitm\actions;

use yii\helpers\ArrayHelper;

/**
  * Controller actions.
  */
 class ListAction extends \yii\base\Action
 {
     public $modelClass;
     public $listFormat;
     public $formatField = '_list-format';
     public $foreignKey = 'parent_id';

     /**
      * This shoudl be configured by each controller to determine what constraints are supported.
      *
      * @var [type]
      */
     protected $constraints = [];
     protected $_queryOptions = [];
     protected $_isDepDrop;

     protected function beforeRun()
     {
         $this->controller->setResponseFormat('json');
         $this->setDepDropConstraints();
         $modelClass = $this->modelClass = $this->modelClass ?: get_class($this->model);
         if (!$this->model instanceof $this->modelClass) {
             $this->model = new $modelClass();
         }

         return true;
     }

     public function run()
     {
         if ($this->listFormat == 'object') {
             $result = $this->model->getJsonList($this->model->titleAttribute, $this->_queryOptions);
         } else {
             $result = $this->model->getList(null, $this->model->titleAttribute, $this->_queryOptions);
         }
         
         return [
             true,
             ['results' => $this->format($result)]
         ];
     }

     protected function format($result)
     {
         if ($this->_isDepDrop) {
             return [
               'output' => $result,
            ];
         } else {
             return $result;
         }
     }

     protected function setDepDropConstraints()
     {
         $constraints = $_REQUEST;
         if (count($constraints)) {
             foreach ($constraints as $key => $value) {
                 if ($key == 'depdrop_parents') {
                     $this->_isDepDrop = true;
                     $this->_queryOptions['andWhere'][$this->foreignKey] = $value;
                     continue;
                 }

                 if (is_array($value)) {
                     //Check to see if the provided keys are described for custom constraints
                     foreach ($value as $attribute => $userValue) {
                         $this->addQueryOption($attribute, $userValue);
                     }
                 } else {
                     $this->addQueryOption($key, $value);
                 }
             }
         }
     }

     /**
      * Add query options if they're supported.
      *
      * @param string $attribute [description]
      * @param mixed $userValue The user supplied value
      */
     protected function addQueryOption($attribute, $userValue)
     {
         $constraint = ArrayHelper::getValue($this->constraints, $attribute, null);
         if (is_array($constraint)) {
             $method = ArrayHelper::remove($constraint, 'constraint', 'andWhere');
             $operator = ArrayHelper::remove($constraint, 'operator', '=');
             $field = ArrayHelper::remove($constraint, 'field', $attribute);
             $this->_queryOptions[$method][] = [
                 $field, $operator, $userValue,
              ];
         }
     }

     public function getModel() {
         return $this->controller->model;
     }
 }
