<?php

namespace nitm\actions;

use yii\helpers\ArrayHelper;

/**
  * Controller actions.
  */
 class PriorityAction extends \yii\base\Action
 {
     protected function beforeRun()
     {
         $this->controller->setResponseFormat('json');

         return true;
     }

     public function run()
     {
         $result = null;
         if ($this->controller->action->id == 'set-priority') {
             $result = $this->setPriority();
         }
         if ($this->controller->action->id == 'disable') {
             $result = $this->disable();
         }

         return $this->controller->renderResponse($result);
     }

     /**
      * Set the priority of an instruction.
      *
      * @param int $id
      * @param int $priority
      */
     public function setPriority()
     {
         extract($this->controller->actionParams);
         $where = $this->getId([$type, $id, $item]);
         $modelClass = $this->controller->model->className();
         $this->controller->model = $this->findModel($modelClass, $where);
         $this->controller->model->setScenario('setPriority');
         /**
          * Find the model that already has this priority.
          */
         $existing = $modelClass::find()
         ->where(array_merge($where, [
             'priority' => $priority,
             'deleted' => false,
         ]))->one();
         //Swap priorities
         if ($existing instanceof $modelClass) {
             $swappingPriority = $this->controller->model->priority;
             $existing->setScenario('setPriority');
             $existing->priority = $swappingPriority;
             $this->controller->model->priority = $priority;
             $existing->save();
         } else {
             $this->controller->model->priority = $priority;
         }

         return $this->controller->model->save();
     }

     public function disable()
     {
         extract($this->controller->actionParams);
         $modelClass = $this->controller->model->className();
         $this->controller->model = $this->findModel($modelClass, $where);
         $this->controller->model->setScenario('disable');
         $this->model->disabled = !$this->model->disabled;
         $ret_val = $this->model->save();
         //Once instruction is disabled update all the priorities that are greater than this one
         if ($ret_val) {
             if (!$this->controller->model->disabled) {
                 $ret_val = [
                   'id' => $this->model->id,
                ];
                //we increase all subsequent instructions if we're enabling an instruction
                 $counter = ['priority' => 1];
                 //If there is already an instruction with this priority then we need to place this one after it
                 $existing = $modelClass::find()->where($this->disableFindWhere)->one();
                 if ($existing instanceof $modelClass) {
                     $this->controller->model->priority = $existing->priority + 1;
                     $this->controller->model->save();
                 }
             } else {
                 //do the opposite otherwise
                 $counter = ['priority' => -1];
             }
             $modelClass::updateAllCounters($counter, $this->disableUpdateWhere);
             $ret_val['priority'] = $this->controller->model->priority;
         }

         return $ret_val;
     }

     protected function getIndexFindWhere($type, $id)
     {
         return [
             'remote_id' => $id,
             'remote_type' => $type,
         ];
     }

     protected function getDisableFindWhere()
     {
         return [
             'remote_id' => $this->controller->model->remote_id,
             'remote_type' => $this->controller->model->remote_type,
             'priority' => $this->controller->model->priority,
             'deleted' => false,
         ];
     }

     protected function getDisableUpdateWhere()
     {
         return [
             'and', 'priority>'.$this->controller->model->priority,
             'disabled=false',
             'remote_id='.$this->controller->model->remote_id,
             'remote_type=\''.$this->controller->model->remote_type.'\'',
             'priority!='.$this->controller->model->priority,
         ];
     }

     protected function idParts($values)
     {
         if (ArrayHelper::isIndexed($values)) {
             return $values;
         }
         $post = array_intersect_key(ArrayHelper::getValue(\Yii::$app->request->post(), $this->controller->model->formName(), []), array_flip($this->controller->model->primaryKeys()));
         if (!empty($post)) {
             $values = $post;
         }

         return $values;
     }

     protected function getId($values)
     {
         $values = $this->idParts($values);

         return array_combine($this->controller->model->primaryKey(), array_filter($values));
     }
 }
