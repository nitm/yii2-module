<?php

namespace nitm\actions;

/**
  * Controller actions.
  */
 class BooleanAction extends \yii\base\Action
 {
     /**
     * Extra acctions supported by this action.
     *
     * @var array
     *
     * Format actions as:
     * [
     *   action => [
     *      scenario => Scenario
     *      attributes => [
     *         attribute => *Requried* The name of the attribute affected by this action,
     *         blamable => The attribute that receves the current user Id,
     *         date => The data attribute
     *      ],
     *      title => [
     *         Title When False,
     *         Title When True
     *      ]
     *   ]
     * ]
     */
    public $extraActions = [];

     protected $boolResult;

     public function beforeAction()
     {
         \Yii::$app->request->setQueryParams([]);
     }

     public function run()
     {
         $saved = false;
         $id = $this->controller->actionParams['id'];
         $this->controller->model = $this->findModel($id, $this->controller->model->className());
         if (array_key_exists($action, static::booleanActions())) {
             extract(static::booleanActions()[$action]);
             $this->controller->model->setScenario($scenario);
             $this->boolResult = !$this->controller->model->getAttribute($attributes['attribute']) ? 1 : 0;
             foreach ($attributes as $key => $value) {
                 switch ($this->controller->model->hasAttribute($value)) {
                     case true:
                     switch ($key) {
                         case 'blamable':
                         $this->controller->model->setAttribute($value, (!$this->boolResult ? null : \Yii::$app->user->getId()));
                         break;

                         case 'date':
                         $this->controller->model->setAttribute($value, ($this->boolResult ? new \yii\db\Expression('NOW()') : null));
                         break;
                     }
                     break;
                 }
             }
             $this->controller->model->setAttribute($attributes['attribute'], $this->boolResult);
             if (!$this->isResponseFormatSpecified) {
                 $this->setResponseFormat('json');
             }

             if (isset($afterAction) && is_callable($afterAction)) {
                 $afterAction($this->controller->model);
             }

             $saved = $this->controller->model->save();
         }

         $this->shouldLog = true;
         $actionTitle = strtolower($title[(int) $this->boolResult]);
         $actionTitle .= (in_array(substr($actionTitle, strlen($actionTitle) - 1, 1), ['e']) ? 'd' : 'ed');

         return $this->result();
     }

     public function result()
     {
         $ret_val = [];
         extract(ArrayHelper::getValue(static::booleanActions(), $this->id, []));
         $title = $title ?? [$this->id, 'Not '.$this->id];
         $this->model->setScenario($scenario ?? 'default');
         $this->boolResult = !$this->model->getAttribute($attributes['attribute']) ? 1 : 0;
         foreach ($attributes as $key => $value) {
             switch ($this->model->hasAttribute($value)) {
                 case true:
                 switch ($key) {
                     case 'blamable':
                     $this->model->setAttribute($value, (!$this->boolResult ? null : \Yii::$app->user->getId()));
                     break;

                     case 'date':
                     $this->model->setAttribute($value, (!$this->boolResult ? null : new \yii\db\Expression('NOW()')));
                     break;
                 }
                 break;
             }
         }
         $this->model->setAttribute($attribute, $this->boolResult);
         $ret_val = [
            'actionHtml' => \nitm\helpers\Icon::forAction($this->id, $attribute, $this->controller->model),
            'data' => $this->boolResult,
            'title' => ArrayHelper::getValue($title, $this->boolResult, ucfirst($this->id)),
            'class' => 'wrapper '.\nitm\helpers\Statuses::getIndicator($this->controller->model->getStatus()),
            'action' => $this->id,
            'id' => $this->conroller->model->getId(),
         ];
         $this->setResponseFormat('json');

         return $this->controller->renderResponse($ret_val, \yii\web\Response::viewOptions(), \Yii::$app->request->isAjax);
     }

     public static function booleanActions()
     {
         return array_replace_recursive([
             'close' => [
                 'scenario' => 'close',
                 'attributes' => [
                     'attribute' => 'closed',
                     'blamable' => 'closed_by',
                     'date' => 'closed_at',
                 ],
                 'title' => [
                     'Re-Open',
                     'Close',
                 ],
             ],
             'complete' => [
                 'scenario' => 'complete',
                 'attributes' => [
                     'attribute' => 'completed',
                     'blamable' => 'completed_by',
                     'date' => 'completed_at',
                 ],
                 'title' => [
                     'In-Complete',
                     'Complete',
                 ],
             ],
             'resolve' => [
                 'scenario' => 'resolve',
                 'attributes' => [
                     'attribute' => 'resolved',
                     'blamable' => 'resolved_by',
                     'date' => 'resolved_at',
                 ],
                 'title' => [
                     'Un-Resolve',
                     'Resolve',
                 ],
             ],
             'disable' => [
                 'scenario' => 'disable',
                 'attributes' => [
                     'attribute' => 'disabled',
                     'blamable' => 'disabled_by',
                     'date' => 'disabled_at',
                 ],
                 'title' => [
                     'Enable',
                     'Disable',
                 ],
             ],
             'delete' => [
                 'scenario' => 'delete',
                 'attributes' => [
                     'attribute' => 'deleted',
                     'blamable' => 'deleted_by',
                     'date' => 'deleted_at',
                 ],
                 'title' => [
                     'Restore',
                     'Delete',
                 ],
             ],
         ], $this->extraActions);
     }
 }
