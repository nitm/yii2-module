<?php

namespace nitm\actions;

use nitm\models\ParentMap;

/**
  * Controller actions.
  */
 class ParentAction extends \yii\base\Action
 {
     public function run($type, $id)
     {
         $ret_val = false;
         if ($this->id == 'remove-parent') {
             $ret_val = $this->removeParent($type, $id);
         } else {
             $ret_val = $this->addParent($type);
         }
         $this->controller->setResponseFormat('json');

         return $this->controller->renderResponse($ret_val);
     }

     protected function addParent($type)
     {
         $ret_val = false;
         $model = $this->controller->model->findOne($type);
         if (is_a($model, $this->controller->model->className())) {
             if ($model->hasAttribute('parent_ids')) {
                 $model->parent_ids = [$id];
             }
             $result = $model->addParentMap([$id]);
             if (array_key_exists($id, (array) $result)) {
                 $parent = $result[$id];
                 $model = $parent['parent_class']::findOne($id);
                 if ($model->hasAttribute('name')) {
                     $name = $model->name;
                 } elseif ($model->hasAttribute('title')) {
                     $name = $model->title;
                 } else {
                     $name = $parent['remote_type'].'-parent-'.$id;
                 }

                 if ($this->controller->determineResponseFormat() == 'json') {
                     $ret_val = [
                       'id' => $id,
                       'removeUrl' => '/'.$this->controller->model->isWhat().'/remove-parent/'.$type.'/'.$id,
                       'name' => $name,
                       'type' => $type,
                   ];
                 } else {
                     $ret_val = Html::tag('li', $name.
                       Html::tag('span',
                           Html::a('Remove '.Icon::show('remove'),
                               '/'.$this->controller->model->isWhat().'/remove-parent/'.$type.'/'.$id, [
                               'role' => 'parentListItem',
                               'style' => 'color:white',
                           ]), [
                           'class' => 'badge',
                       ]), [
                       'class' => 'list-group-item',
                   ]);
                 }
             }
         }

         return $ret_val;
     }

     protected function removeParent($type, $id)
     {
         $where = ['remote_id' => $type, 'parent_id' => $id];
         $model = ParentMap::find()->where($where)->one();
         if (is_object($model)) {
             return $model->find()->createCommand()->delete($model->tableName(), $where)->execute();
         }

         return false;
     }
 }
