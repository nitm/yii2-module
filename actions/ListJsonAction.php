<?php

namespace nitm\actions;

/**
  * Controller actions.
  */
 class ListJsonAction extends ListAction
 {
     public function run()
     {
         $result = $this->controller->model->getJsonList($this->controller->model->idAttribute, $this->controller->model->titleAttribute, $this->_queryOptions);

         return $this->format($result);
     }
 }
