<?php

namespace nitm\actions;

use nitm\helpers\Icon;
use nitm\helpers\Html;
use yii\helpers\ArrayHelper;

/**
  * Controller actions.
  */
 class ListSearchAction extends ListAction
 {
     public $constraints = [];

     public function run()
     {
         $this->controller->setResponseFormat('json');
         $modelClass = $this->modelClass;

         $result = $this->getList($modelClass, []);
         return [
             true,
             ['results' => $this->format($result)]
         ];
     }

     public function getList($searchClass, $options = [], $level = 'summary')
     {
         $ret_val = [];
         $this->model->beginSearch([
             'booleanSearch' => true,
             'inclusiveSearch' => true,
             'mergeInclusive' => true,
             'queryOptions' => ArrayHelper::merge((array) $this->model->queryOptions, ArrayHelper::getValue($options, 'queryOptions', []))
         ]);

         $labelField = $this->model->titleAttribute;
         $params = \Yii::$app->request->get();
         $params['text'] = isset($params['text']) ? $params['text'] : \Yii::$app->request->get('term');
         $dataProvider = $this->model->search($params);

         if (!count($params)) {
             //Specialized sorting algorithm goes here
            $dataProvider->query->orderBy(['id' => SORT_DESC]);
         }

         foreach ($dataProvider->getModels() as $item) {
             $level = isset($options['getter']) && is_callable($options['getter']) ? 'getter' : $level;
             switch ($level) {
                case 'getter':
                $_ = $options['getter']($item);
                break;

                default:
                $_ = [
                    'id' => $item->id,
                    'title' => $item->$labelField,
                    'label' => $item->$labelField,
                    'text' => $item->$labelField,
                ];
                break;
            }
            $ret_val[] = $_;
         }

         return (sizeof(array_filter($ret_val)) >= 1) ? $ret_val : [['id' => 0, 'text' => 'Nothing Found']];
     }
 }
