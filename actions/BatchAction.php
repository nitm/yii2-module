<?php

namespace nitm\actions;

use nitm\helpers\ArrayHelper;
use nitm\helpers\Response;

/**
  * Controller actions.
  */
 class BatchAction extends \yii\base\Action
 {
     /**
     * Extra actions supported by this action.
     *
     * @var array
     *
     * Format actions as:
     * [
     *   action => [
     *      title => [
     *         Title When False,
     *         Title When True
     *      ],
     *      messages => [
     *         Message When False,
     *         Message When True
     *      ],
     *      operation => funciton ($model) {
     *          return ...;
     *      }
     *   ]
     * ]
     */
    public $extraActions = [];

     protected $_result;
     protected $_currentAction = [];

     public function beforeAction()
     {
         \Yii::$app->request->setQueryParams([]);
     }

     public function run()
     {
         $saved = false;
         $ids = \Yii::$app->request->post('selection');
         if (count($ids)) {
             $action = \Yii::$app->request->get('type');
             if (array_key_exists($action, $this->batchActions())) {
                 $this->_currentAction = $this->batchActions()[$action];
                 foreach ($ids as $id) {
                     try {
                         $model = $this->controller->findModel($id, $this->controller->model->className());
                         $success = call_user_func($this->_currentAction['operation'], $model);
                         $result = ['model' => $model, 'id' => $id];
                         if ($success) {
                             $result['success'] = true;
                             if (isset($afterAction) && is_callable($afterAction)) {
                                 $afterAction($model);
                             }
                         } else {
                             $result['success'] = false;
                         }
                         $actionTitle = ArrayHelper::getValue($this->_currentAction, 'title.'.(int)$success, strtolower($action));
                         $actionTitle .= (in_array(substr($actionTitle, strlen($actionTitle) - 1, 1), ['e']) ? 'd' : 'ed');
                         $result['action'] = strtolower($action);
                         $result['class'] = \nitm\helpers\Statuses::getListIndicator($success ? 'success' : 'warning');
                         $result['message'] = str_replace([
                             '{{title}}', '{{type}}', '{{errors}}', '{{action}}', '{{resultAction}}'
                         ], [
                             $model->title(), $model->is, \nitm\helpers\Model::formatErrors($model), $action, strtolower($actionTitle)
                         ], ArrayHelper::getValue($this->_currentAction, 'messages.'.(int)$success, $success ? "Successfully {{action}} {{title}}" : "Failed to {{action}} {{title}}. Reason: {{errors}}"));
                     } catch (\yii\web\NotFoundHttpException $e) {
                         $success = false;
                         $class = $this->controller->model->className();
                         $result = [
                             'model' => new $class,
                             'id' => $id,
                             'message' => $e->getMessage(),
                            'class' => 'list-group-item list-group-item-warning',
                         ];
                     }
                     $this->_result[] = $result;
                 }
             }
         }

         return $this->result();
     }

     public function result()
     {
         $ret_val = [];
         if (!$this->controller->isResponseFormatSpecified && \Yii::$app->request->isAjax) {
             $this->controller->setResponseFormat('json');
         }
         if ($this->controller->responseFormat == 'html' || $this->controller->responseFormat == 'modal') {
             Response::viewOptions(null, [
                 'view' => '@nitm/views/batch/index',
                 'args' => [
                     'dataProvider' => new \yii\data\ArrayDataProvider([
                         'models' => $this->_result
                     ])
                 ]
             ]);
         } else {
             $ret_val = $this->_result;
         }

         return $this->controller->renderResponse($ret_val, Response::viewOptions(), \Yii::$app->request->isAjax);
     }

     public function batchActions()
     {
         return array_replace_recursive([
             'delete' => [
                 'title' => [
                     'Undelete',
                     'Delete',
                 ],
                 "messages" => [
                    "Unable to delete {{title}}. Reason: {{errors}}",
                    "Successfully deleted {{title}}"
                 ],
                 'operation' => function ($model) {
                     return $model->delete();
                 }
             ],
         ], $this->extraActions);
     }
 }
