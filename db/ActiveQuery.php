<?php

namespace nitm\db;

use nitm\helpers\ArrayHelper;
use nitm\helpers\QueryFilter;

/**
 * Traits defined for expanding query scopes until yii2 resolves traits issue.
 */
class ActiveQuery extends \yii\db\ActiveQuery
{
    protected $success;

    public function behaviors()
    {
        return [
            \nitm\behaviors\SoftDeleteQuery::className(),
            \nitm\behaviors\RecordListQuery::className(),
        ];
    }

    /**
     * Sets the successfull parameter for query.
     */
    public function successful()
    {
        return $this->success === true;
    }

    /**
     * Overriding default find function.
     */
    public function find($model = null, $options = null)
    {
        $modelClass = $this->modelClass;
        $this->from = [$model instanceof ActiveRecord ? $model->tableName() : $modelClass::tableName()];
        if ($model instanceof ActiveRecord) {
            $this->aliasColumns();
            foreach ($model->queryOptions as $filter => $value) {
                switch (strtolower($filter)) {
                    case 'select':
                    case 'indexby':
                    case 'orderby':
                    if (is_string($value) && ($value == 'primaryKey')) {
                        unset($model->queryOptions[$filter]);
                        $this->$filter($modelClass::primaryKey()[0]);
                    }
                    break;
                }
            }
            $this->applyFilters(ArrayHelper::getValue($model, 'queryOptions', []));
        } else {
            $this->aliasColumns();
        }

        return $this;
    }

    public function addWith($with)
    {
        $with = is_array($with) ? $with : [$with];
        $this->with = array_merge($this->withs, $with);
    }

    /*
     * Apply the filters specified by the end user
     * @param ActiveQuery $this
     * @param mixed $filters
     */
    public function applyFilters($filters = null)
    {
        return QueryFilter::applyFilters($this, $filters);
    }

    /*
     * Some common filters
     * @param $name The name of the filter
     * @param $default Should a default value be appended?
     * @return mixed $filter
     */
    public function getFilter($name = null, $default = true)
    {
        return QueryFilter::getFilter($this, $name, $default);
    }

    /*
     * Does this object support this filter?
     * @param string|int #name
     * @return boolean
     */
    public static function hasFilter($name)
    {
        return QueryFilter::hasFilter($name, static::filters());
    }

    /*
     * Set the aliased fields according to the class columns() function
     * @param ActiveQuery $this
     */
    public function aliasColumns()
    {
        return QueryFilter::aliasColumns($this);
    }

    protected function applyQueryOptions($options, $target = null)
    {
        return QueryFilter::applyQueryOptions($this, $options, $target);
    }
}
