<?php

/**
 * Base Data getter/operation class.
 */

namespace nitm\db;

use Yii;
use nitm\helpers\ArrayHelper;

class ActiveRecord extends \yii\db\ActiveRecord
{
    use \nitm\traits\Data, \nitm\traits\Relations;

    public static $shouldBindToParent = true;
    public $queryOptions = [];
    protected $queryClass;
    protected $skipInit;
    protected $_scenario;

    public function init()
    {
        if (!$this->skipInit) {
            parent::init();
        }
    }

    public function behaviors()
    {
        $behaviors = [
            'list' => [
                'class' => \nitm\behaviors\RecordList::className(),
            ],
            'relationManager' => [
                'class' => \nitm\behaviors\RelationManagement::className(),
            ],
            'changedAttributes' => [
                'class' => \nitm\behaviors\ChangedAttributes::className(),
            ],
            'cache' => [
                'class' => \nitm\behaviors\Cache::className(),
            ],
        ];
        if (class_exists('\nitm\search\Module')) {
            $behaviors['search'] = [
                'class' => \nitm\search\behaviors\SearchModel::class,
            ];
        }

        return array_replace_recursive(parent::behaviors(), $behaviors);
    }

    public function setScenario($scenario)
    {
        if (isset($this->scenarios()[$scenario])) {
            $this->_scenario = $scenario;
        } else {
            $this->_scenario = 'default';
        }
    }

    /**
     * Formats all model errors into a single string.
     *
     * @return string
     */
    public function formatErrors()
    {
        $result = '';
        foreach ($this->getErrors() as $attribute => $errors) {
            $result .= implode(' ', $errors).' ';
        }

        return $result;
    }

    /**
     * Overriding default find function.
     *
     * @param static $model   The model to get default options from
     * @param array  $options the Array fo options for the query
     */
    public static function find($model = null, $options = null)
    {
        $queryClass = ArrayHelper::remove($options, 'queryClass', ActiveQuery::className());
        $query = \Yii::createObject($queryClass, [get_called_class()]);
        $query->find($model);
        static::applyQueryOptions($query, $options);
        if (isset($model) && ($model instanceof self) && !isset($model->shouldBindToParent)) {
            $model->shouldBindToParent = true;
        }

        return $query;
    }

    public function getErrorMessage()
    {
        return array_map('implode', $this->getErrors(), ['. ']);
    }

    /**
     * Get the unique ID of this object.
     *
     * @return string|int
     */
    public function getId($splitter = '', $key = null)
    {
        $key = $key ?: $this->primaryKey();
        $splitter = $splitter ?: '';
        $id = implode($splitter, array_filter(array_map(function ($attribute) {
            return $this->getAttribute($attribute);
        }, $key)));

        if (is_numeric($id)) {
            return (int) $id;
        }

        return $id;
    }

    public function getPublicId()
    {
        return $this->id;
    }

    public function getNamespace()
    {
        if (isset($this)) {
            $class = $this->className();
        } else {
            $class = static::className();
        }

        return \nitm\helpers\ClassHelper::getNameSpace($class);
    }

    public function flagId($flag)
    {
        if (isset($this) && $this instanceof \nitm\models\Data) {
            return self::getNamespace().'\\'.$this->getId();
        } else {
            return self::getNamespace().'\\';
        }
    }

    /**
     * Some support for setting custom flags at runtime.
     */
    public function setFlag($flag, $value)
    {
        self::$_flags[self::flagId($flag)] = $value;
    }

    public function getFlag($flag)
    {
        return ArrayHelper::getValue(self::$_flags, self::flagId($flag), null);
    }

    public static function unsetFlag($flag)
    {
        return ArrayHelper::remove(self::$_flags, self::flagId($flag), null);
    }

    /*
     * Return a string imploded with ucfirst characters
     * @param string $name
     * @return string
     */
    public static function properName($value = null, $plural = false)
    {
        if (isset($this)) {
            $value = is_null($value) ? $this->isWhat($plural) : $value;
        } else {
            $value = is_null($value) ? static::isWhat($plural) : $value;
        }

        return \nitm\helpers\ClassHelper::properName($value);
    }

    /*
     * Return a string imploded with ucfirst characters
     * @param string $name
     * @return string
     */
    public function properFormName($value = null)
    {
        if (isset($this)) {
            $value = is_null($value) ? $this->isWhat() : $value;
        } else {
            $value = is_null($value) ? static::isWhat() : $value;
        }

        return \nitm\helpers\ClassHelper::properFormName($value);
    }

    /*
     * Return a string imploded with ucfirst characters
     * @param string $name
     * @return string
     */
    public function properClassName($value = null)
    {
        if (isset($this)) {
            $value = is_null($value) ? $this->className() : $value;
        } else {
            $value = is_null($value) ? static::className() : $value;
        }

        return \nitm\helpers\ClassHelper::properClassName($value, $this->namespace);
    }

    /**
     * Sets the successfull parameter for query.
     */
    public function successful()
    {
        return $this->success === true;
    }

    /**
     * Get the array of arrays.
     *
     * @return array
     */
    public static function getArrays()
    {
        $query = static::find(isset($this) ? $this : null);
        $ret_val = $query->asArray()->all();

        return $ret_val;
    }

    /**
     * Get array of objects.
     *
     * @return mixed
     */
    public static function getModels()
    {
        $query = static::find(isset($this) ? $this : null);
        $ret_val = $query->all();

        return $ret_val;
    }

    /**
     * Get a single record.
     */
    public static function getOne()
    {
        $query = static::find(isset($this) ? $this : null);
        $ret_val = $query->one();

        return $ret_val;
    }

    public function addWith($with)
    {
        $with = is_array($with) ? $with : [$with];
        $this->queryOptions['with'] = array_merge((array) ArrayHelper::getValue($this->queryOptions, 'with', []), $with);
    }

    public function setShouldBindToParent($value)
    {
        static::$shouldBindToParent = $value;
    }

    public function getShouldBindToParent()
    {
        return static::$shouldBindToParent;
    }

    protected function resetBehavior($behavior)
    {
        try {
            $this->getBehavior('tree')->detach();
            $this->attachBehavior('tree', $this->behaviors()['tree']);
            echo "Sucessfully reattached to tree behavior\n";
        } catch (\Exception $e) {
        }
    }

    /**
     * Yii DataProvider sort creator.
     *
     * @param array $to     Add the sort options to $to
     * @param array $labels THe labels for the sort
     *                      [
     *                      'name' => [
     *                      'attribute' => [
     *                      'asc' => [string (field|relation) => order],
     *                      'desc' => [string (field|relation) => order],
     *                      'defautl' => string|numeric Default sort order,
     *                      'label' => string The label for the sort field,
     *                      ...
     *                      ],
     *                      ...
     *                      ],
     *                      ]
     * @param array $params The default params for the sort
     */
    public function addSortParams(&$to, array $labels, array $params = [])
    {
        foreach ($labels as $attr => $options) {
            @list($relation, $label, $orderAttr) = (array) $options;
            $relation = is_null($relation) ? $attr : $relation;

            if ($orderAttr instanceof \yii\db\Expression) {
                $relation = serialize(new \yii\db\Expression($relation.'.'.$orderAttr));
            } else {
                $relation .= is_null($orderAttr) ? '' : '.'.$orderAttr;
            }

            $to[$attr] = array_merge([
                'asc' => [$relation => SORT_ASC],
                'desc' => [$relation => SORT_DESC],
                'default' => SORT_DESC,
                'label' => $label,
            ], $params);
        }
    }

    public function getSort()
    {
        return [];
    }

    /**
     * Apply an array of query options to the query.
     *
     * @param ActiveQuery $query        [description]
     * @param array       $queryOptions [description]
     * @param bool        $asArray      [description]
     *
     * @return $query [description]
     */
    protected static function applyQueryOptions(&$query, $queryOptions, $asArray = false)
    {
        foreach ((array) $queryOptions as $method => $params) {
            if ($query->hasMethod($method)) {
                $query->$method($params);
            }
        }

        if ($asArray === true) {
            $query->asArray();
        }

        return $query;
    }
}
