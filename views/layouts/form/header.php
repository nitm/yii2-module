<?php

use nitm\helpers\Html;
use yii\helpers\ArrayHelper;

$formClass = \kartik\widgets\ActiveForm::class;

echo Html::tag('div', '', ['id' => 'alert']);

$defaultFormOptions = [
    'type' => 'vertical',
    'options' => [
        'role' => 'ajaxForm'
    ]
];
$formOptions = isset($formOptions) ? array_merge($defaultFormOptions, $formOptions) : $defaultFormOptions;

$form = $formClass::begin(array_merge([
        'id' => ArrayHelper::getValue($formOptions, 'options.id', uniqid()),
        'type' => 'horizontal',
        'fieldConfig' => [
            'inputOptions' => ['class' => 'form-control'],
            'labelOptions' => ['class' => 'col-lg-2 col-md-3 control-label'],
        ],
        'enableAjaxValidation' => true,
    ], array_diff_key($formOptions, [
        'container' => null,
    ])
));

return $form;
