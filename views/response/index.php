<?php
    if (!isset($content)) {
        throw new \yii\web\NotFoundHttpException('No Data Found', 404);
    } else {
        echo $content;
    }
