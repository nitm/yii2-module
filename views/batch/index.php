<?php

use nitm\helpers\Html;
use yii\widgets\ListView;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var lab1\models\search\Country $searchModel
 */

$this->title = Yii::t('app', 'Batch result');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="batch-result-index">
    <h1><?= Html::encode($this->title) ?></h1>
	<?= Html::a('Go Back', \Yii::$app->request->referrer, ['class' => 'btn btn-primary']); ?><br><br>
	<?= ListView::widget([
        'summary' => false,
        'options' => [
            'tag' => 'ul',
            'class' => 'list-group'
        ],
        'dataProvider' => $dataProvider,
        'itemOptions' => [
            'tag' => false,
        ],
        'itemView' => function ($model) {
            return Html::tag('li', $model['message'], [
                'class' =>  $model['class']
            ]);
        }
    ]); ?>
	<?= Html::a('Go Back', \Yii::$app->request->referrer, ['class' => 'btn btn-primary']); ?>
</div>
