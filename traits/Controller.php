<?php

namespace nitm\traits;

use nitm\helpers\Response;
use nitm\helpers\Icon;
use nitm\helpers\ArrayHelper;
use nitm\helpers\Html;

/**
  * Traits defined for expanding active relation scopes until yii2 resolves traits issue.
  */
 trait Controller
 {
     public $modelClass;
     public $model;

     protected $_logLevel = 3;
     protected $logCollection;
     protected $shouldLog = true;
     protected $returnExistingOnCreate = false;

    /*
     * Check to see if somethign is supported
     * @param mixed $what
     */
    public function isSupported($what)
    {
        return @$this->settings['supported'][$what] == true;
    }

     public static function assets()
     {
         return [];
     }

    /**
     * Initialze the assets supported by this controller. Taken from static::has();.
     *
     * @param mixed $assts  Array of assets
     * @param bool  $force. Force registration of assets
     */
    public function initAssets($assets = [], $force = false)
    {
        $assets = array_merge($assets, (array) static::assets());
        foreach ($assets as $asset) {
            //This may be an absolute namespace to an asset
            switch (class_exists($asset)) {
                case true:
                $asset::register(\Yii::$app->getView());
                break;

                default:
                //It isn't then it may be an asset we have in nitm/assets or nitm/widgets
                $class = $asset.'\assets\Asset';
                switch (class_exists($class)) {
                    case true:
                    $class::register(\Yii::$app->getView());
                    break;

                    default:
                    //This is probably not a widget asset but a module asset
                    $class = '\nitm\assets\\'.static::properName($asset).'Asset';
                    switch (class_exists($class)) {
                        case true:
                        $class::register(\Yii::$app->getView());
                        break;
                    }
                    break;
                }
                break;
            }
        }
    }

     /**
      * [getFormVariables description].
      *
      * @method getFormVariables
      *
      * @param  [type]           $model        [description]
      * @param  [type]           $options      [description]
      * @param  [type]           $modalOptions [description]
      *
      * @return [type]                         [description]
      */
     public function getFormVariables($model, $options, $modalOptions = [])
     {
         return \nitm\helpers\Form::getVariables($model, $options, $modalOptions);
     }

    /**
     * Use nitm logger to log something.
     *
     * @param string|array $message
     * @param int          $level.           Only log if logging level is above this
     * @param string       $action
     * @param string       $category         The category to insert this with
     * @param string       $internalCategory
     * @param object       $model            The model object
     *
     * @return bool
     */
    protected function log($message, $level = 0, $action = null, $options = [], $model = null)
    {
        if (\Yii::$app->getModule('nitm')->enableLogger) {
            if (is_null($message)) {
                return false;
            }

            if (!$model) {
                $model = ($model instanceof \nitm\models\Data) ? $model : $this->model;
            }

            /*
             * Only log this information if the logging $level is less than or equal to the gloabl accepted level.
             */
            $options = array_merge([
                'category' => 'user-activity',
                'internal_category' => 'User Activity',
                'table_name' => $model->tableName(),
                'message' => $message,
                'action' => (is_null($action) ? $this->action->id : $action),
            ], $options);

            return \Yii::$app->getModule('nitm')->log($options, $level, null, $model);
        }

        return false;
    }

     protected function commitLog()
     {
         return \Yii::$app->getModule('nitm')->commitLog();
     }

     protected function getWith()
     {
         return [];
     }

    /**
     * Get te log parameters.
     *
     * @param bool                    $saved
     * @param array                   $result
     * @param \nitm\models\Data based $model
     *
     * @return array;
     */
    protected function getLogParams($saved, $result, $model = null)
    {
        $action = strtolower(ArrayHelper::remove($result, 'actionName', $this->action->id));
        $level = ArrayHelper::remove($result, 'logLevel', $this->_logLevel);
        $category = ArrayHelper::remove($result, 'logCategory', 'User Action');
        $internalCategory = ArrayHelper::remove($result, 'internalLogCategory', 'user-activity');

        $baseArgs = [
            'category' => $category,
            'internal_category' => $internalCategory,
        ];

        if (!isset($result['collection_name']) && (isset($this->logCollection) && !is_null($this->logCollection))) {
            $baseArgs['collection_name'] = $this->logCollection;
        }

        if (!$model) {
            $model = $this->hasProperty('model') && ($this->model instanceof \nitm\models\Data) ? $this->model : new \nitm\models\Data(['noDbInit' => true]);
        }

        $id = ArrayHelper::remove($result, 'id', $model->getId());
        $message = [\Yii::$app->user->identity->username];

        array_push($message, ($saved ? $action.(in_array(substr($action, strlen($action) - 1, 1), ['e']) ? 'd' : 'ed') : "failed to $action"), $this->model->isWhat());
        if ($id) {
            array_push($message, "with id $id");
        }
        if (!$saved) {
            array_push($message, "\n\nError was: \n\n".var_export($message));
        }

        return [
            implode(' ', $message), $level, $action, $baseArgs, $model,
        ];
    }

    /**
     * Prepare some standard Javascript functions.
     *
     * @param bool  $force   Force preparing the operation
     * @param array $options
     */
    protected function prepareJsFor($force = false, $options = [])
    {
        $options = $options == [] ? ['forms', 'actions'] : $options;
        if (!Response::viewOptions('js') || $force) {
            $js = '';
            foreach ((array) $options as $type) {
                switch ($type) {
                    case 'forms':
                    $js .= 'module.initForms(null, "'.$this->model->isWhat().'");';
                    break;

                    case 'actions':
                    $js .= 'module.initMetaActions(null, "'.$this->model->isWhat().'");';
                    break;
                }
            }
            Response::viewOptions('js', new \yii\web\JsExpression('$nitm.onModuleLoad("entity", function (module) {'.$js.'}, null, "anonymous")'));

            return true;
        }

        return false;
    }

    /**
     * Determine how to return the data.
     *
     * @param mixed $result Data to be displayed
     */
    public function renderResponse($result = null, $params = null, $partial = null)
    {
        Response::initContext(\Yii::$app->controller,  \Yii::$app->controller->getView());
        $params = is_null($params) ? Response::viewOptions() : $params;

        return Response::render($result, $params, $partial);
    }

    /**
     * Get the desired display format supported.
     *
     * @return string format
     */
    public function setResponseFormat($format = null)
    {
        return Response::setFormat($format);
    }

    /**
     * Get the response format.
     *
     * @method getResponseFormat
     *
     * @return string The response format
     */
    public function getResponseFormat()
    {
        return Response::getFormat();
    }

    /**
     * Is the response format specified?
     *
     * @method isResponseFormatSpecified
     *
     * @return bool
     */
    public function getIsResponseFormatSpecified()
    {
        return Response::formatSpecified();
    }

    /**
     * Determine what format to return for the response.
     *
     * @method determineResponseFormat
     *
     * @param string $format THe response format
     *
     * @return string The response format
     */
    protected function determineResponseFormat($format = null)
    {
        if (Response::formatSpecified()) {
            $this->setResponseFormat(Response::getFormat());

            return;
        }

        if (\Yii::$app->request->isAjax) {
            $this->setResponseFormat(\Yii::$app->request->get('_pjax') ? 'html' : 'json');
        } else {
            $this->setResponseFormat($format ?: 'html');
        }

        return $this->responseFormat;
    }

    /*
     * Return a string imploded with ucfirst characters
     * @param string $name
     * @return string
     */
    protected static function properName($value)
    {
        return \nitm\helpers\ClassHelper::properName($value);
    }

    /**
     * Finds the Category model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param string $className
     * @param int    $id
     * @param array  $with      Load with what
     *
     * @return the loaded model
     *
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function findModel($id = null, $className = null, $with = null, $queryOptions = [], $throwError = true)
    {
        $ret_val = null;
        if ($className == null) {
            $className = $this->model ? $this->model->className() : $this->modelClass;
        }
        if ($id != null || $queryOptions != []) {
            $query = $className::find();

            if ($id && is_numeric($id)) {
                $pk = $className::primaryKey();
                $query->where([array_shift($pk) => $id]);
            } elseif (is_array($id)) {
                $query->where($id);
            }

            $with = is_array($with) ? $with : (is_null($with) ? null : [$with]);
            if (is_array($with)) {
                $query->with($with);
            }

            if ($queryOptions != []) {
                foreach ($queryOptions as $type => $options) {
                    $query->$type($options);
                }
            }

            if (($ret_val = $query->one()) !== null) {
                $ret_val->afterFind();

                return $ret_val;
            } elseif ($throwError) {
                $id = implode('-', (array) $id);
                if (defined('YII_DEBUG') && (defined('YII_ENV') && YII_ENV == 'dev')) {
                    throw new \yii\web\NotFoundHttpException((new $className())->properName()." : $id doesn't exist!");
                }

                return $ret_val;
            }

            return null;
        } elseif ($throwError && defined('YII_DEBUG') && (defined('YII_ENV') && YII_ENV == 'dev')) {
            throw new \yii\web\NotFoundHttpException((new $className())->properName()." doesn't exist!");
        }

        return $ret_val;
    }

    /**
     * Either create a new model or update an existing model.
     *
     * @method getModel
     *
     * @param string      $scenario   The scenario we're working on
     * @param int|null    $id         The id of an existing model
     * @param string|null $modelClass The model classs
     *
     * @return [type] [description]
     */
    public function getModel($scenario, $id = null, $modelClass = null, $with = null, $queryOptions=[], $throwError = true)
    {
        if (!$modelClass && isset($this->model)) {
            $modelClass = $this->model->className();
        } elseif (!class_exists($modelClass) && $this->hasProperty('modelClass') && class_exists($this->modelClass)) {
            $modelClass = $this->modelClass;
        }
        $this->model = $this->loadModel($scenario, $id, $modelClass, $with, $queryOptions, $throwError);
        $this->trigger('afterFind', new \yii\base\Event([
           'sender' => $this,
        ]));

        return $this->model;
    }

    /**
     * Extract the relation parameters.
     *
     * @method extractWith
     *
     * @param array $options Array of specified options
     *
     * @return array The with parameters
     */
    public function extractWith($options)
    {
        $ret_val = [];
        $with = array_merge($this->getWith(), (array) ArrayHelper::getValue($options, 'with', ArrayHelper::getValue($options, 'queryOptions.with', [])));
        foreach ($with as $relation => $callable) {
            if (is_numeric($relation)) {
                $ret_val[] = $callable;
            } elseif (is_string($relation) && !is_callable($callable)) {
                $ret_val[] = $relation;
            } elseif (is_string($relation) && is_callable($callable)) {
                $ret_val[$relation] = $callable;
            }
        }

        return array_unique($ret_val);
    }

    /**
     * Go through the relations and determine whether they shoudl be kept or excluded.
     *
     * @method filterWith
     *
     * @param \yii\db\Query $query The query object
     * @param array with			    The relations
     *
     * @return bool Relations were dropped
     */
    public function filterWith($query, $with = [])
    {
        if (in_array($query->className(), [
            '\yii\elasticsearch\ActiveQuery',
            '\yii\mongodb\ActiveQuery',
        ])) {
            $query->with = null;
        }

        $query->with = array_unique(array_merge((array) $query->with, $with));

        return true;
    }

    /**
     * Get some related videw options.
     *
     * @method getViewOptions
     *
     * @param array $options User specified options
     *
     * @return array View options
     */
    public function getViewOptions($options = [])
    {
        $createOptions = isset($options['createOptions']) ? $options['createOptions'] : [];

        $filterOptions = isset($options['filterOptions']) ? $options['filterOptions'] : [];

        return [
            'createButton' => $this->getCreateButton($createOptions),
            'createMobileButton' => $this->getCreateButton(array_replace_recursive([
                'containerOptions' => [
                    'class' => 'btn btn-default navbar-toggle aligned',
                ],
            ], $createOptions), 'Create'),
            'filterButton' => $this->getFilterButton($filterOptions),
            'filterCloseButton' => $this->getFilterButton($filterOptions, 'Close'),
            'isWhat' => $this->model->isWhat(),
        ];
    }

    /**
     * Is the current request an ajax request?
     * @method getIsAjaxRequest
     * @return boolean
     */
    public function getIsAjaxRequest()
    {
        if (\Yii::$app->request->isAjax && $this->responseFormat !== 'json'
        || \Yii::$app->request->isAjax) {
            return true;
        }
        return false;
    }

    /**
     * Is the current request an pjax request?
     * @method getIsPjaxRequest
     * @return boolean
     */
    public function getIsPjaxRequest()
    {
        if (\Yii::$app->request->isPjax) {
            return true;
        }
        return false;
    }

    /**
     * Is this a form request for validation?
     *
     * @method isValidationRequest
     *
     * @return bool Whether this is a validation request
     */
    public function isValidationRequest()
    {
        return \Yii::$app->request->isAjax && (boolval(ArrayHelper::getValue($_REQUEST, 'do', false)) !== true) && !\Yii::$app->request->get('_pjax');
    }

    /**
     * Perform the validation request on the model.
     *
     * @method performValidationRequest
     *
     * @param $model ActiveRecord
     *
     * @return array The errors is there were any
     */
    public function performValidationRequest($model = null)
    {
        $this->setResponseFormat('json');
        $model = $model ?: $this->model;
        if (!$model) {
            throw new \yii\web\NotFoundHttpException("Couldn't find model to validate");
        }

        return \yii\widgets\ActiveForm::validate($model);
    }

    /**
     * Either create a new model or update an existing model.
     *
     * @method getModel
     *
     * @param string      $scenario   The scenario we're working on
     * @param int|null    $id         The id of an existing model
     * @param string|null $modelClass The model classs
     *
     * @return [type] [description]
     */
    public function loadModel($scenario, $id = null, $modelClass = null, $with = null, $throwError = true)
    {
        $modelClass = $modelClass ?: $this->modelClass;
        if (!class_exists($modelClass)) {
            throw new \yii\base\UnknownClassException("$modelClass doesn't exist");
        }

        $data = \Yii::$app->request->post();

        if (!is_null($id)) {
            $model = $this->findModel($id, $modelClass, is_null($with) ? $this->getWith() : $with, [], $throwError);
        } else {
            $model = new $modelClass();
        }
        if (!$model) {
            return null;
        }
        if (isset($model->scenarios()[$scenario])) {
            $model->setScenario($scenario);
        } else {
            $model->setScenario('default');
        }
        if (!empty($data)) {
            $model->load($data);
        }

        return $model;
    }

     protected function scenarios()
     {
         return[];
     }

    /**
     * If there were errors after the save then handle them.
     *
     * @method handleSaveErrors
     *
     * @param array  $data   The data to be saved
     * @param string $action The action being performed
     *
     * @return string The message after success or failure
     */
    protected function saveInternal($data, $action, $providedModel = null, $options=[])
    {
        $model = $providedModel ?: $this->model;
        $ret_val = false;
        $result = [
            'message' => "Unable to {$action} ".$this->model->isWhat(),
        ];
        if ($model->save()) {
            $ret_val = true;
            $result['changedAttributes'] = ArrayHelper::getValue($this->model, 'changedAttributes');
            $result['message'] = implode(' ', [
                "Succesfully {$action}d ",
                $model->isWhat(),
                ': '.\nitm\helpers\Helper::getTitle($model),
            ]);
            Response::viewOptions('view', '/'.$this->id.'/view');
            if ($model->scenario == 'create') {
                $this->trigger(static::EVENT_AFTER_CREATE, new \yii\base\Event([
                  'sender' => $this,
               ]));
            } else {
                $this->trigger(static::EVENT_AFTER_UPDATE, new \yii\base\Event([
                  'sender' => $this,
               ]));
            }
        } else {
            if ($model) {
                $result['message'] = $model->formatErrors();

                \Yii::$app->getSession()->setFlash('error', $ret_val);
            } else {
                $this->shouldLog = false;
            }

            /*
             * If the save failed, we're most likely going back to the form so get the form variables.
             */
            $variables = $this->getVariables($model->isWhat());
            if (is_array($variables)) {
                $variables = array_merge($variables, [
                   'view' => '/'.$this->id."/$action",
               ]);
            }
            Response::viewOptions(null, $variables, true);
        }

        return [$ret_val, $result, $options];
    }

    /**
     * Get the create button.
     *
     * @method getCreateButton
     *
     * @param array  $options The options for the create button
     * @param string $text    The text for the button
     *
     * @return string The HTML button
     */
    public function getCreateButton($options = [], $text = null, $isWhat = null)
    {
        $isWhat = $isWhat ?: $this->model->isWhat();
        $id = $isWhat ?: $this->id;
        $properName = $isWhat ? \nitm\helpers\ClassHelper::properName($isWhat) : $this->model->properName();
        $text = ucwords(\Yii::t('yii', ' new '.($text ?: $properName)));
        $options = array_replace_recursive([
            'toggleButton' => [
                'tag' => 'a',
                'label' => Icon::forAction('plus').' '.$text,
                'href' => \Yii::$app->urlManager->createUrl(['/'.$id.'/form/create', '_format' => 'modal']),
                'title' => \Yii::t('yii', 'Add a new '.$properName),
                'role' => 'dynamicAction createAction disabledOnClose',
                'class' => 'btn btn-success',
            ],
            //'dialogOptions' => [
            //	"class" => "modal-full"
            //],
            'containerOptions' => [
                'class' => 'navbar-collapse navbar-collapse-content',
            ],
        ], (array) $options);

        $containerOptions = $options['containerOptions'];
        unset($options['containerOptions']);

        return Html::tag('div', \nitm\widgets\modal\Modal::widget($options), $containerOptions);
    }

    /**
     * Get the filter button.
     *
     * @method getFilterButton
     *
     * @param array  $options The options for the filter button
     * @param string $text    The text for the button
     *
     * @return string The HTML button
     */
    public function getFilterButton($options = [], $text = 'filter', $isWhat = null)
    {
        $isWhat = $isWhat ?: $this->model->isWhat();
        $containerOptions = isset($options['containerOptions']) ? $options['containerOptions'] : ['class' => 'navbar-toggle aligned'];
        unset($options['containerOptions']);

        return Html::tag('div', Html::button(Icon::forAction('filter').' '.ucwords($text), array_replace([
            'class' => 'btn btn-default',
            'data-toggle' => 'collapse',
            'data-target' => '#'.$isWhat.'-filter',
        ], (array) $options)), $containerOptions);
    }

    /*
     * Get the variables for a model
     * @param string $param What are we getting this form for?
     * @param int $unique The id to load data for
     * @param array $options
     * @return string | json
     */
    protected function getVariables($type = null, $id = null, $options = [])
    {
        $force = false;
        $options['id'] = $id;
        $options['param'] = $type;

        if (isset($options['modelClass'])) {
            $this->model = new $options['modelClass'](ArrayHelper::getValue($options, 'construct', []));
        }
        $this->model->setAttributes(ArrayHelper::getValue($options, 'construct', []), false);
        $options = array_merge([
            'title' => ['title', 'Create '.static::properName($this->model->formName())],
            'scenario' => !$id ? 'create' : 'update',
            'provider' => null,
            'dataProvider' => null,
            'view' => isset($options['view']) ? $options['view'] : $type,
            'args' => [],
            'modelClass' => $this->model->className(),
            'force' => false,
        ], $options);

        $options['modalOptions'] = isset($options['modalOptions']) ? (array) $options['modalOptions'] : [];
        $modalOptions = array_merge([
            'body' => [
                'class' => 'modal-full',
            ],
            'dialog' => [
                'class' => 'modal-full',
            ],
            'content' => [
                'class' => 'modal-full',
            ],
            'contentOnly' => true,
        ], ArrayHelper::getValue($options, 'modalOptions', []));

        unset($options['modalOptions']);

        return $this->getFormVariables($this->model, $options, $modalOptions);
    }
 }
