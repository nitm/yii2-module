<?php

/**
 * @package mhdevent/yii2-module
 *
 * Traits for grid action view
 */
namespace nitm\traits;

use Yii;
use nitm\helpers\Html;
use yii\helpers\Inflector;
use nitm\helpers\ArrayHelper;
use nitm\helpers\WidgetHElper;
use nitm\helpers\Icon;

trait GridActionColumnTrait
{
    public $pjax = true;
    public $menuIcon = 'bars';
    public $alignMenu = 'pull-right';
       /**
        * Map icons that couldbe missing from the different frameworks.
        *
        * @var array
        * [
        *    $provider => [
        *       $iconFrom => $iconTo,
        *       ...
        *    ],
        *    ...
        * ]
        */
       public $iconMap = [];
        /**
         * The icon framework to use
         * One of [glyphicon, fontAwesome].
         *
         * @var string
         */
        public $iconFramework = 'fontAwesome';
        /**
         * Initializes the default button rendering callback for single button.
         *
         * @param string $name              Button name as it's written in template
         * @param string $iconName          The part of Bootstrap glyphicon class that makes it unique
         * @param array  $additionalOptions Array of additional options
         *
         * @since 2.0.11
         */
        protected function initDefaultButton($name, $iconName, $additionalOptions = [])
        {
            if (!isset($this->buttons[$name]) && strpos($this->template, '{'.$name.'}') !== false) {
                $this->buttons[$name] = function ($url, $model, $key) use ($name, $iconName, $additionalOptions) {
                    $title = Yii::t('yii', ucfirst($name));
                    $options = array_merge([
                        'title' => $title,
                        'aria-label' => $title,
                        'data-pjax' => '0',
                    ], $additionalOptions, $this->buttonOptions);
                    $icon = $this->getIcon($iconName);

                    return [
                       'label' => $icon,
                       'url' => $url,
                       'linkOptions' => $options,
                    ];
                };
            }
        }

        /**
         * Get the icon name.
         *
         * @param string $iconName [description]
         *
         * @return string The html icon element
         */
        protected function getIcon($iconName)
        {
            $iconName = $this->getIconMap($iconName);
            switch ($this->iconFramework) {
                 case 'fontAwesome':
                 return Html::tag('span', '', ['class' => "fa fa-$iconName"]);
                 break;

                 case 'glyphicon':
                 return Html::tag('span', '', ['class' => "glyphicon glyphicon-$iconName"]);
                 break;

                 default:
                 return \nitm\helpers\Icon::show($iconName);
                 break;
             }
        }

    protected function getIconMap($iconName)
    {
        $map = array_merge([
                'fontAwesome' => [
                   'eye-open' => 'eye',
                ],
             ], $this->iconMap);

        return ArrayHelper::getValue($map, $this->iconFramework.'.'.$iconName, $iconName);
    }

       /**
        * {@inheritdoc}
        */
       protected function renderDataCellContent($model, $key, $index)
       {
           $buttons = array_filter(preg_split('/[\{\}\s]/', $this->template), 'strlen');

           $this->buttons = array_intersect_key($this->buttons, array_flip($buttons));

           foreach ($buttons as $name) {
               if (!strlen($name)) {
                   continue;
               }
               if (isset($this->visibleButtons[$name])) {
                   $isVisible = $this->visibleButtons[$name] instanceof \Closure
                      ? call_user_func($this->visibleButtons[$name], $model, $key, $index)
                      : $this->visibleButtons[$name];
               } else {
                   $isVisible = true;
               }
               if ($isVisible && isset($this->buttons[$name])) {
                   $url = $this->createUrl($name, $model, $key, $index);
                   $label = explode('/', $name);
                   $label = Inflector::humanize(array_pop($label));

                   if (is_callable($this->buttons[$name])) {
                       $button = call_user_func($this->buttons[$name], $url, $model, $key);
                       if (is_string($button)) {
                           list($attributes, $label) = $this->extractTag($button);
                           $options = $this->extractAttributes($attributes);
                           $options = array_combine($options[0], $options[1]);
                           $options['data-pjax'] = ArrayHelper::getValue($options, 'data-pjax', $this->pjax);
                       }
                       $this->buttons[$name] = [
                          'label' => trim($label),
                          'url' => ArrayHelper::remove($options, 'href', $url),
                          'linkOptions' => $options,
                       ];
                   } elseif (is_array($this->buttons[$name]) && !empty($this->buttons[$name])) {
                       $this->buttons[$name]['linkOptions'] = array_merge([
                           'data-pjax' => $this->pjax
                       ], ArrayHelper::getValue($this->buttons, $name.'.linkOptions', []));
                       $this->buttons[$name] = array_replace([
                          'url' => ArrayHelper::getValue($this->buttons[$name], 'url', $url),
                          'label' => ArrayHelper::getValue($this->buttons[$name], 'label', $label),
                       ], $this->buttons[$name]);
                   }
               } else {
                   $this->buttons[$name]['visible'] = false;
               }
           }
           foreach ($this->buttons as $url => $button) {
               if (is_callable($button)) {
                   $this->buttons[$url] = call_user_func($button, $url, $model, $key);
               }
           }

          //  print_r($this->buttons);
          //  exit;

           return WidgetHelper::widget(\yii\bootstrap\ButtonDropdown::class, [
              'encodeLabel' => false,
              'label' => Icon::show($this->menuIcon).'&nbsp;',
              'options' => ['class' => 'btn'],
              'dropdown' => [
                'encodeLabels' => false,
                'options' => [
                   'class' => $this->alignMenu,
                ],
                'items' => $this->buttons,
             ],
          ]);
       }

    protected function extractTag($tag)
    {
        preg_match('/<[\w]+?[\s]+([^>]+)>(.*)<\/[\w]+?>/', $tag, $matches);
        array_shift($matches);

        return $matches;
    }

    protected function extractAttributes($attributes)
    {
        preg_match_all('/([\w\-]+)\=[\'\"]?([^\'\"]+)[\'\"]?/', $attributes, $options);
        array_shift($options);

        return $options;
    }
}
