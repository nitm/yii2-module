<?php

namespace nitm\traits;

use nitm\helpers\Relations as RelationsHelper;
use nitm\models\ParentMap;
use nitm\models\Category;
use nitm\helpers\ArrayHelper;
use nitm\helpers\QueryFilter;
use nitm\helpers\ClassHelper;

/**
 * Traits defined for expanding active relation scopes until yii2 resolves traits issue.
 */
trait RelatedRecords
{
    /**
     * This is here to allow base classes to modify the query before finding the count.
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCount($link = null)
    {
        $primaryKey = current($this->primaryKey());
        $link = is_array($link) ? $link : [$primaryKey => $primaryKey];
        $tableName = static::tableName();
        $tableNameAlias = $tableName.'_alias';
        $query = $this->hasOne(static::className(), $link)
            ->select([
                '_count' => 'COUNT('.$primaryKey.')',
            ])
            ->groupBy(array_values($link));
        foreach (['where', 'orwhere', 'andwhere'] as $option) {
            if (isset($this->queryOptions[$option])) {
                $query->$option($this->queryOptions[$option]);
            }
        }

        return $query;
    }

    public function count($returnNull = false)
    {
        $ret_val = RelationsHelper::getRelatedRecord($this, 'count', static::className(), [
            '_count' => 0,
        ])['_count'];

        if ($ret_val == 0 && $returnNull) {
            return null;
        }

        return $ret_val;
    }

    /**
     * User based relations.
     */
    protected function getUserRelationQuery($link, $options = [], $className = null)
    {
        if (is_null($className)) {
            if (\Yii::$app->hasProperty('user')) {
                // $className = \Yii::$app->user->identityClass;
                $className = \nitm\models\User::class;
            } else {
                $className = ClassHelper::resolveClass(\nitm\models\User::className());
            }
        }
        $options['select'] = isset($options['select']) ? $options['select'] : ['id', 'username'];
        $options['with'] = isset($options['with']) ? $options['with'] : ['profile'];
        $options['where'] = [];

        return $this->getRelationQuery($className, $link, $options);
    }

    protected static function getNitmModule()
    {
        return \Yii::$app->getModule('nitm');
    }

    protected function getCachedUserModel($idKey, $className = null)
    {
        $className = is_null($className) ? \Yii::$app->user->identityClass : $className;
        if ($this->nitmModule && $this->nitmModule->useModelCache) {
            return $this->getCachedRelation($idKey, $className, [], false, ClassHelper::getCallerName(), 'user');
        } else {
            return RelationsHelper::getRelatedRecord($this, ClassHelper::getCallerName(), $className, [], false);
        }
    }

    /**
     * Get user relation.
     *
     * @param array $options Options for the relation
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser($options = [])
    {
        return $this->getUserRelationQuery(['id' => 'user_id'], $options)->with('profile');
    }

    /**
     * Get user relation.
     *
     * @param array $options Options for the relation
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor($options = [])
    {
        return $this->getUserRelationQuery(['id' => 'author_id'], $options)->with('profile');
    }

    /**
     * Get user relation.
     *
     * @param array $options Options for the relation
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEditor($options = [])
    {
        return $this->getUserRelationQuery(['id' => 'editor_id'], $options)->with('profile');
    }

    /**
     * Get user relation.
     *
     * @param array $options Options for the relation
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCompletedBy($options = [])
    {
        return $this->getUserRelationQuery(['id' => 'completed_by'], $options)->with('profile');
    }

    /**
     * Get user relation.
     *
     * @param array $options Options for the relation
     *
     * @return \yii\db\ActiveQuery
     */
    public function getResolvedBy($options = [])
    {
        return $this->getUserRelationQuery(['id' => 'resolved_by'], $options)->with('profile');
    }

    /**
     * Get user relation.
     *
     * @param array $options Options for the relation
     *
     * @return \yii\db\ActiveQuery
     */
    public function getClosedBy($options = [])
    {
        return $this->getUserRelationQuery(['id' => 'closed_by'], $options)->with('profile');
    }

    /**
     * Get user relation.
     *
     * @param array $options Options for the relation
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDisabledBy($options = [])
    {
        return $this->getUserRelationQuery(['id' => 'disabled_by'], $options)->with('profile');
    }

    /**
     * Get user relation.
     *
     * @param array $options Options for the relation
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDeletedBy($options = [])
    {
        return $this->getUserRelationQuery(['id' => 'deleted_by'], $options)->with('profile');
    }

    /**
     * Category based relations.
     */
    protected function getCategoryRelation($link, $options = [], $className = null, $many = false)
    {
        $className = is_null($className) ? ClassHelper::resolveClass(\nitm\models\Category::className()) : $className;
        $options['select'] = isset($options['select']) ? $options['select'] : ['*'];
        $options['with'] = isset($options['with']) ? $options['select'] : [];
        $options['orderBy'] = isset($options['orderBy']) ? $options['orderBy'] : ['id' => SORT_DESC];

        return $this->getRelationQuery($className, $link, $options, $many);
    }

    protected function getCachedCategoryModel($idKey, $className = null, $relation = null, $many = false)
    {
        $className = is_null($className) ? ClassHelper::resolveClass(\nitm\models\Category::className()) : $className;
        $relation = is_null($relation) ? ClassHelper::getCallerName() : $relation;
        if ($this->nitmModule && $this->nitmModule->useModelCache) {
            return $this->getCachedRelation($idKey, $className, [], $many, $relation);
        } else {
            return RelationsHelper::getRelatedRecord($this, $relation, $className, [], $many);
        }
    }

    /**
     * Get type relation.
     *
     * @param array $options Options for the relation
     *
     * @return \yii\db\ActiveQuery
     */
    public function getType($options = [])
    {
        $options['where'] = !isset($options['where']) ? [] : $options['where'];
        return $this->getCategoryRelation(['id' => 'type_id'], $options);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLevel()
    {
        return $this->getCategoryRelation(['id' => 'level_id']);
    }

    /**
     * Get category relation.
     *
     * @param array $options Options for the relation
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCategory($options = [])
    {
        $options['where'] = !isset($options['where']) ? [] : $options['where'];

        return $this->getCategoryRelation(['id' => 'category_id'], $options);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMetadata()
    {
        $metadataClass = $this->getMetadataClass();

        return $this->hasMany($metadataClass, $metadataClass::metadataLink())
            ->indexBy('key');
    }

    /**
     * Get metadata, either from key or all metadata.
     *
     * @param string $key
     *
     * @return mixed
     */
    public function metadata($key = null)
    {
        return ArrayHelper::getValue($this->metadata, $key, is_null($key) ? $this->metadata : null);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParentMap()
    {
        $options = [
            'from' => [
                'parentMap' => ParentMap::tableName(),
            ],
            'where' => ['parentMap.remote_type' => $this->isWhat()],
        ];

        return $this->getRelationQuery(ParentMap::className(), ['remote_id' => 'id'], $options);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParentsMap()
    {
        $options = [
            'from' => [
                'parentsMap' => ParentMap::tableName(),
            ],
            'where' => ['parentsMap.remote_type' => $this->isWhat()],
        ];

        return $this->getRelationQuery(ParentMap::className(), ['remote_id' => 'id'], $options, true);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent()
    {
        /**
         * parent_id represents the outter part of the query and will matcch to the static::className's id
         * This is the parent_id in the ParentMap table
         * remote_id maps to the current class's id.
         */
        return $this->getRelationQuery($this->className(), ['id' => 'parent_id'])
             ->from([
                 '_parent' => $this->tableName(),
             ])
            ->viaTable(ParentMap::tableName(), ['remote_id' => 'id'], function ($query) {
                $alias = QueryFilter::getAlias($query, $this, 'parentMap');
                $query->from([
                    $alias => $query->from[0],
                ]);
                $query->where([$alias.'.remote_class' => $this->className()]);

                return $query;
            });
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParents()
    {
        /**
         * parent_id represents the outter part of the query and will matcch to the static::className's id
         * This is the parent_id in the ParentMap table
         * remote_id maps to the current class's id.
         */
        return $this->getRelationQuery($this->className(), ['id' => 'parent_id'], ['where' => []], true)
            ->viaTable(ParentMap::tableName(), ['remote_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChildren()
    {
        /**
         * parent_id represents the outter part of the query and will matcch to the static::className's id
         * This is the parent_id in the ParentMap table
         * remote_id maps to the current class's id.
         */
        return $this->getRelationQuery($this->className(), ['id' => 'remote_id'], ['where' => []], true)
            ->viaTable(ParentMap::tableName(), ['parent_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSiblings()
    {
        /**
         * parent_id represents the outter part of the query and will matcch to the static::className's id
         * This is the parent_id in the ParentMap table
         * remote_id maps to the current class's id.
         */
        return $this->getRelationQuery($this->className(), ['id' => 'id'], [], true)
            ->from([
                'siblings' => $this->tableName(),
            ])
            ->joinWith([
                'parent' => function ($query) {
                    $query->joinWith('children');
                },
            ]);
    }
}
