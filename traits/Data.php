<?php

namespace nitm\traits;

use yii\helpers\Inflector;

/**
 * Traits defined for expanding query scopes until yii2 resolves traits issue.
 */
trait Data
{
    public $queryOptions = [];
    public static $slugIs = [];

    protected static $is;

    protected static $tableName;
    protected static $flags;

    public static function tableName()
    {
        if (!static::$tableName) {
            $class = parent::class;
            if (method_exists($class, 'tableName')) {
                return $class::tableName();
            }
        }

        return static::$tableName;
    }

    public static function setIs($is)
    {
        static::$is = $is;
    }

    public static function isWhat(...$arguments)
    {
        return static::getIsA(...$arguments);
    }

    public function getIs(...$arguments)
    {
        return static::isWhat(...$arguments);
    }

    /*
     * What does this claim to be?
     * @param bollean|null $pluralize Should the returnvalue be pluralized or singularized.
     * When set to null nothing is done
     * @param boolean $forceClassType resolution Don't check for type if this is set to type
     */
    public static function getIsA($pluralize = null, $forceClassType = false)
    {
        $slugify = function ($value) {
            $stack = explode('\\', $value);

            return Inflector::slug(implode(' ', preg_split('/(?=[A-Z])/', array_pop($stack), -1, PREG_SPLIT_NO_EMPTY)));
        };

        $class = static::class ;

        if (!$forceClassType && method_exists($class, 'type') && !empty($class::type())) {
            $ret_val = $class::type();
        } else {
            $ret_val = $class;
        }

        if (is_null($pluralize)) {
            $inflector = 'slug';
        } else {
            $inflector = $pluralize === true ? 'pluralize' : 'singularize';
        }

        if (!isset($class::$slugIs[$inflector][$ret_val])) {
            $ret_val = $class::$slugIs[$inflector][$ret_val] = Inflector::$inflector($slugify($ret_val));
        } else {
            $ret_val = $class::$slugIs[$inflector][$ret_val];
        }

        //If we didn't set the inflector then set the $is value to the return value
        if (is_null($pluralize) && !isset($class::$is)) {
            $class::$is = $ret_val;
        }

        return $ret_val;
    }

    /**
     * Get the underlying class.
     *
     * @return string The class this model really represents
     */
    public function getMorphClass()
    {
        return static::class;
    }

    public function title()
    {
        return $this->{$this->titleAttribute};
    }

    public function getTitleAttribute()
    {
        return 'title';
    }

    /**
     * Merge the fields and extraFields for the specified class.
     *
     * @param string|null $modelClass The class to get all fields for. If null then use the current class name
     *
     * @return array An array containing the original fields, extra fields and merged fields
     */
    public function allFields(string $modelClass = null)
    {
        $modelClass = $modelClass ?: static::className();

        $fields = array_map(function ($k, $v) {
            if (is_int($k)) {
                return $v;
            }

            return $k;
        }, array_keys($modelClass::fields()), $modelClass::fields());
        $extraFields = array_map(function ($k, $v) {
            if (is_int($k)) {
                return $v;
            }

            return $k;
        }, array_keys($modelClass::extraFields()), $modelClass::extraFields());

        return [$fields, $extraFields, array_unique(array_merge($fields, $extraFields))];
    }

    public function onScenario($scenario = null)
    {
        return [];
    }


    public function hasRelation($name, $model = null)
    {
        $ret_val = null;
        $model = is_null($model) ? $this : $model;
        $method = 'get'.$name;
        try {
            if ($model->hasMethod($method) && (new \ReflectionMethod($model, $method))->isPublic()) {
                $ret_val = call_user_func([$model, $method]);
                if (!($ret_val instanceof \yii\db\ActiveQuery)) {
                    $ret_val = null;
                }
            }
        } catch (\Exception $e) {
        }

        return $ret_val;
    }
}
