<?php

namespace nitm\traits;

use Yii;
use yii\helpers\ArrayHelper;

trait FlatTrait
{
    public static $FLAT = [];

    /**
     * Get cached flat array of category objects.
     *
     * @return array
     */
    public static function cats()
    {
        $cache = Yii::$app->cache;
        $key = static::tableName().'_flat';

        if (empty(static::$FLAT[$key])) {
            $flat = $cache->get($key);
            if (!$flat) {
                $flat = static::generateFlat();
                $cache->set($key, $flat, 3600);
            }

            foreach ($flat as $id => $cat) {
                $model = new static([
                    'id' => $id,
                    'parent' => $cat->parent,
                    'children' => $cat->children,
                ]);
                $model->load((array) $cat, '');
                $model->populateRelation('seo', new SeoText($cat->seo));
                $model->setTagNames($cat->tags);
                $model->afterFind();
                static::$FLAT[$key][] = $model;
            }
        }

        return ArrayHelper::getValue(static::$FLAT, $key);
    }

    /**
     * Generates flat array of categories.
     *
     * @return array
     */
    public static function generateFlat()
    {
        $collection = static::find()->with(['seo', 'tags'])->sort()->asArray()->all();
        $flat = [];

        if (count($collection) > 0) {
            $depth = 0;
            $lastId = 0;
            foreach ($collection as $node) {
                $node = (object) $node;
                $id = $node->id;
                $node->parent = '';

                if ($node->depth > $depth) {
                    $node->parent = $flat[$lastId]->id;
                    $depth = $node->depth;
                } elseif ($node->depth == 0) {
                    $depth = 0;
                } else {
                    if ($node->depth == $depth) {
                        $node->parent = $flat[$lastId]->parent;
                    } else {
                        foreach ($flat as $temp) {
                            if ($temp->depth == $node->depth) {
                                $node->parent = $temp->parent;
                                $depth = $temp->depth;
                                break;
                            }
                        }
                    }
                }
                $lastId = $id;
                unset($node->lft, $node->rgt);
                $flat[$id] = $node;
            }
        }

        foreach ($flat as &$node) {
            $node->children = [];
            foreach ($flat as $temp) {
                if ($temp->parent == $node->id) {
                    $node->children[] = $temp->id;
                }
            }
            if (is_array($node->tags) && count($node->tags)) {
                $tags = [];
                foreach ($node->tags as $tag) {
                    $tags[] = $tag['name'];
                }
                $node->tags = $tags;
            }
        }

        return $flat;
    }
}
