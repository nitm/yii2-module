<?php

namespace nitm\traits;

use Yii;
use yii\db\ActiveRecord;
use nitm\helpers\Relations as RelationsHelper;
use nitm\helpers\ArrayHelper;

/**
* Traits defined for expanding active relation scopes until yii2 resolves traits issue.
*/
trait Relations
{
    /**
    * The dynamically created methods for this model
    * @var [type]
    */
    private $_methods = [];
    
    /**
    * Calls the named method which is not a class method.
    *
    * Do not call this method directly as it is a PHP magic method that
    * will be implicitly called when an unknown method is being invoked.
    *
    * @param string $name   the method name
    * @param array  $params method parameters
    *
    * @throws UnknownMethodException when calling unknown method
    *
    * @return mixed the method return value
    */
    public function __call($name, $params)
    {
        $method = strtolower($name);
        if (array_key_exists($method, $this->_methods)) {
            return call_user_func_array($this->_methods[$name], $params);
        } else {
            try {
                return parent::__call($name, $params);
            } catch (\yii\base\UnknownMethodException $e) {
                //    if ($name == 'count') {
                if (strpos($name, 'Count') !== false) {
                    if (method_exists($this, 'get'.$name)) {
                        $relation = call_user_func_array([$this, 'tryRelation'], array_merge([$name], (empty($params) ? [[]] : $params)));
                        if ($relation instanceof ActiveRecord && $relation->isNewRecord) {
                            return 0;
                        }
                        return ArrayHelper::getValue($relation, 'count', 0);
                    } else {
                        throw $e;
                    }
                } elseif (method_exists($this, 'get'.$name)) {
                    return call_user_func_array([$this, 'tryRelation'], array_merge([$name], (empty($params) ? [[]] : $params)));
                } else {
                    throw $e;
                }
            }
        }
    }
    
    /**
    * Modified set function to properly set relations
    * @param [type] $name   [description]
    * @param [type] $params [description]
    */
    public function __set($name, $params)
    {
        try {
            if ($this->hasMethod('set'.$name)) {
                $params = is_array($params) && ArrayHelper::isIndexed($params) ? $params : [$params];
                return call_user_func_array([$this, 'set'.$name], $params);
            } else {
                return parent::__set($name, $params);
            }
        } catch (\yii\base\InvalidCallException $e) {
            //Tried to set a relation property. Let's populate the relation!
            return $this->tryRelation($name, $params);
        }
    }
    
    /**
    * [addMethod description]
    * @param [type] $name   [description]
    * @param [type] $method [description]
    */
    public function addMethod($name, $method)
    {
        $this->_methods[strtolower($name)] = \Closure::bind($method, $this, get_called_class());
    }
    
    /**
    * [getMethod description]
    * @param  [type] $name [description]
    * @return [type]       [description]
    */
    public function getMethod($name)
    {
        return $this->_methods[strtolower($name)];
    }
    
    /**
    * [hasMethod description]
    * @param  [type]  $method         [description]
    * @param  boolean $checkBehaviors [description]
    * @return boolean                 [description]
    */
    public function hasMethod($method, $checkBehaviors = true, $localOnly = false)
    {
        if ($localOnly) {
            //We're only looking for custom non standard set methods
            return isset($this->_methods[strtolower($method)]);
        } else {
            //Otherwise check for the general set method
            return isset($this->_methods[strtolower($method)]) || method_exists($this, $method) || parent::hasMethod($method, $checkBehaviors);
        }
    }
    
    /**
    * [tryRelation description]
    * @param  [type] $name   [description]
    * @param  [type] $params [description]
    * @return [type]         [description]
    */
    public function tryRelation($name, $params)
    {
        if (method_exists($this, 'get'.$name)) {
            $params = is_array($params) && count($params) ? $params : [$params];
            $ret_val = $this->prepareAndPopulateRelation($name, $params, true);
            return $ret_val;
        }
    }
    
    /**
    * Prepare teh relations and then automatically populate them
    * @param  string $name       [description]
    * @param  mixed $attributes [description]
    * @return ActiveRecord|array             [description]
    */
    protected function prepareAndPopulateRelation($name, $params, $getDummy = false)
    {
        $relation = $ret_val = null;
        try {
            if ($this->hasRelation($name)) {
                $relation = $this->getRelation($name, false);
            }
        } catch (\Exception $e) {
            $relation = null;
        }
        if ($relation instanceof \yii\db\ActiveQuery) {
            $ret_val = $relation->multiple ? [] : null;
            $relationClass = $relation->modelClass;
            $params = ArrayHelper::getValue($params, (new $relationClass)->formName(), $params);
            
            /**
            * Load the models we need if they are empty
            */
            if ($relation->multiple) {
                $relationModels = $this->initRelations($name, $relation, count(array_filter($params)) ? $params : [], false);
            } else {
                $relationModels = $this->initRelation($name, $relation, $params, $getDummy);
            }
            $ret_val = $relationModels;
        }
        return $ret_val;
    }
    
    /**
    * Init multiple relations by merging new data with existing data
    * @param  string  $name       The relation name
    * @param  ActiveQuery  $relation   The relation
    * @param  array  $attributes The related model information
    * @param  boolean $getDummy   Should a dummy model be returned if one can't be found?
    * @return array              The related models
    */
    protected function initRelations($name, $relation, $attributes, $getDummy = false)
    {
        $wasPopulated = false;
        if ($this->isRelationPopulated($name)) {
            $wasPopulated = true;
            //Need to switch to enable using Models and not just attributes
            $relationModels = (array)$this->{$name};
            foreach ($attributes as $k => $v) {
                $v = is_object($v) ? $v->attributes : $v;
                if (isset($v['id'])) {
                    //If this item exists in the existing relations then update it by merging values
                    foreach ($relationModels as $key => $value) {
                        if ($v['id'] == $value['id']) {
                            if (is_object($relationModels[$key])) {
                                $relationModels[$key]->setAttributes(array_merge($value, $v));
                            } else {
                                $relationModels[$key] = array_merge($value, $v);
                            }
                            break;
                    }
                }
            } elseif (!ArrayHelper::isSubset($v, $relationModels)) {
                $relationModels[] = $v;
            }
        }
    } else {
        $relationModels = $relation->all() ?: $attributes;
    }
    
    $relationModels = array_filter(array_map(function ($attributes) use ($name, $relation, $getDummy) {
        $model = $this->initRelation($name, $relation, $attributes, false, $attributes);
        return $model;
    }, (array)$relationModels));
    return $relationModels;
}

/**
* Init a single relation. If the relation is loaded then simply update the attribtues
* @param  string  $name       The relation name
* @param  ActiveQuery  $relation   The relation
* @param  array  $attributes The related model information
* @param  boolean $getDummy   [description]
* @return ActiveRecord              The related model
*/
protected function initRelation($name, $relation, $attributes, $getDummy = false, $relationModel = null)
{
    $relationModel = $this->determineRelation($name, $relation, $attributes, $relationModel);
    
    if (!$this->isRelationPopulated($name) && !$relation->multiple) {
        $this->populateRelation($name, $relationModel);
    }
    
    return $relationModel;
}

/**
* Determine the relation link and setup the connection if necessary
* @param  string $name          The relation name
* @param  ActiveQuery $relation      The relation
* @param  array $attributes    Model attributes
* @param  ActiveRecord $relationModel The relation model
* @return ActiveRecord                The models or array of models
*/
protected function determineRelation($name, $relation, $attributes, $relationModel = null)
{
    $attributes = (array)$attributes;
    $modelClass = $relation->modelClass;
    
    if (!$relation->multiple && $this->isRelationPopulated($name)) {
        $relationModel = $this->{$name};
        $wasPopulated = true;
    } elseif (!$relation->multiple) {
        // echo "Relation $name wasn't populated for {$this->classname()}\n";
        $relationModel = $relation->one();
    }
    
    if (is_array($relationModel)) {
        $attributes = $relationModel;
        $relationModel = null;
    }
    
    $attributes = array_filter((array)$attributes);
    
    if (!$relationModel || !is_object($relationModel)) {
        $relationModel = new $modelClass();
        if (!empty($attributes)) {
            $relationModel->setAttributes($attributes);
        }
    }
    
    $link = $relationModel->hasProperty('link') ? $relationModel->link : $relation->link;
    
    /**
    * If the parameters are not a single array then it's most likely
    */
    foreach ($link as $remoteKey => $localKey) {
        if (ArrayHelper::isAssociative($attributes)) {
            $value = ArrayHelper::getValue($attributes, $remoteKey);
        } else {
            $value = current($attributes);
            next($attributes);
        }
        if ($value && $relationModel->hasAttribute($remoteKey) && !$relationModel->$remoteKey) {
            $relationModel->$remoteKey = $value;
            if (!$relation->multiple && $localKey != 'id' && $this->hasAttribute($localKey)) {
                $this->$localKey = $value;
            }
        }
    }
    $isDependent = true;
    //This means there's no related information for this model
    if (empty($relationModel->attributes)) {
        $relationModel->setAttributes($attributes);
        $isDependent = false;
        //If we're populating a new record we should problably load the relation from the database
    } elseif ($relationModel->isNewRecord) {
        //Only get the relation from the database if the primary model attributes are not empty
        $loadedAttributes = array_filter(array_intersect_key($attributes, $link));
        if (!empty($loadedAttributes)) {
            try {
                $relationModel = $relationModel->isNewRecord ? $relation->one() ?: $relationModel : $relationModel;
                if ($relationModel->isNewRecord) {
                    $relationModel->setAttributes($loadedAttributes);
                }
            } catch (\Exception $e) {
                $relationModel->setAttributes($loadedAttributes);
            }
        } elseif ($relationModel && $relationModel->hasAttribute('id') && $relationModel->id) {
            $relationModel->refresh();
        }
    } else {
        //We have an existing model that needs to be updated
        $relationModel->setAttributes($attributes);
    }
    $event = $this->isNewRecord ? 'afterInsert' : 'afterUpdate';
    //Attach relations only if they are new
    if ($relationModel instanceof $modelClass && $isDependent) {
        if ($relationModel->isNewRecord) {
            $this->on($event, function () use ($name, $relationModel) {
                /**
                * Sometimes the relation may have already been populated
                * Check to see if it is available before loading it.
                */
                $relationModel = $this->isRelationPopulated($name) ? $this->$name : $relationModel;
                $attributes = array_filter($relationModel->attributes);
                if (!empty($attributes)) {
                    $attributes = array_merge($attributes, $relationModel->attributes);
                    $relationModel->load($attributes, '');
                }
                try {
                    if ($relationModel instanceof \nitm\filemanager\models\Image) {
                        // print_r($relationModel);
                        // exit;
                    }
                    $this->link($name, $relationModel);
                } catch (\Exception $e) {
                    \Yii::error($e);
                }
            });
            //This model has been updated with associative data
        } elseif (ArrayHelper::isAssociative($attributes)) {
            //Otherwise lets try saving a changed model
            $this->on($event, function () use ($name, $relationModel) {
                if (!empty($relationModel->dirtyAttributes)) {
                    $relationModel->save();
                }
            });
        }
    }
    
    //Add simple scalar values from whre clause to attributes
    $where = $relation->where;
    if (is_array($where) && count($where)) {
        foreach ($where as $key => $value) {
            if (is_scalar($value) && ($relationModel->hasProperty($key) || $relationModel->hasAttribute($key))) {
                $relationModel->{$key} = $value;
                continue;
            }
        }
    }
    
    return $relationModel;
}

/**
* Use this method to populate relations.
*
* @param string $name  the unsafe attribute name
* @param mixed  $value the attribute value
*/
public function onUnsafeAttribute($name, $value)
{
    if (is_string($name)) {
        try {
            $setMethod = strtolower('set'.$name);
            if ($this->hasMethod($setMethod, true, true)) {
                call_user_func_array($this->_methods[$setMethod], [$value]);
            } else if($this->hasMethod($setMethod)) {
                call_user_func_array([$this, $setMethod], [$value]);
            }
            return $this->tryRelation($name, $value);
        } catch (\Exception $e) {
            if (YII_DEBUG) {
                Yii::trace("Failed to set unsafe attribute '$name' in '".get_class($this)."'.", __METHOD__."\n".$e->__toString());
            }
        }
    }
}

public function resolveRelation($idKey, $modelClass, $useCache = false, $options = [], $many = false, $relation = null)
{
    $relation = is_null($relation) ? ClassHelper::getCallerName() : $relation;
    
    return RelationsHelper::resolveRelation($this, $idKey, $modelClass, $useCache, $many, $options, $relation);
}

protected function getRelationCountQuery($class, $queryOptions = [])
{
    $link = ArrayHelper::remove($queryOptions, 'link', null);
    if (is_null($link)) {
        throw new \yii\base\InvalidParamException('You need to specify the link for this count relation');
        }
    $groupBy = ArrayHelper::remove($queryOptions, 'groupBy', null);
    if (is_null($groupBy)) {
        throw new \yii\base\InvalidParamException('You need to specify the grouping for this count relation');
        }
    $select = array_unique(array_merge($groupBy, ArrayHelper::remove($queryOptions, 'select', [])));
    $select[] = 'COUNT(*) AS count';
    $where = ArrayHelper::remove($queryOptions, 'where', null);
    $query = $this->hasOne($class, $link)
    ->select($select)
    ->groupBy($groupBy);
    if ($where) {
        $query->andWhere($where);
    }
    
    return $this->applyQueryOptions($query, $queryOptions, true);
}

protected function getRelationQuery($className, $link, $options = [], $many = false)
{
    $className = $this->getRelationClass($className, get_called_class());
    $relationFunction = ($many === true) ? 'hasMany' : 'hasOne';
    $ret_val = $this->$relationFunction($className, $link);
    
    $callers = debug_backtrace(null, 3);
    
    $relation = $callers[2]['function'];
    $options['select'] = isset($options['select']) ? $options['select'] : null;
    /*$options['groupBy'] = array_map(function ($group){
    if(strpos($group, $this->tableName()) === false)
    $group = $this->tableName().'.'.$group;
    return $group;
    }, array_keys(isset($options['groupBy']) ? $options['groupBy'] : $link));*/
    if (is_array($options) && !empty($options)) {
        foreach ($options as $option => $params) {
            if (is_string($option)) {
                $params = is_array($params) ? array_filter($params) : $params;
                $ret_val->$option($params);
            }
        }
    }
    
    return $ret_val;
}

public function getCachedRelation($idKey, $modelClass, $options = [], $many = false, $relation = null)
{
    $relation = is_null($relation) ? ClassHelper::getCallerName() : $relation;
    
    return RelationsHelper::getCachedRelation($this, $idKey, $many, $modelClass, $relation, $options);
}

public function setCachedRelation($idKey, $modelClass, $options = [], $many = false, $relation = null)
{
    $relation = is_null($relation) ? ClassHelper::getCallerName() : $relation;
    
    return RelationsHelper::setCachedRelation($this, $idKey, $many, $modelClass, $relation);
}

public function deleteCachedRelation($idKey, $modelClass, $options = [], $many = false, $relation = null)
{
    $relation = is_null($relation) ? ClassHelper::getCallerName() : $relation;
    
    return RelationsHelper::deleteCachedRelation($this, $idKey, $many, $modelClass, $relation);
}

public static function getRelationClass($relationClass, $callingClass)
{
    $parts = explode('\\', $relationClass);
    $baseName = array_pop($parts);
    if (\nitm\search\traits\SearchTrait::useSearchClass($callingClass) !== false && (strpos($callingClass, 'search') === false)) {
        $parts[] = 'search';
    }
    $parts[] = $baseName;
    
    return implode('\\', $parts);
}
}