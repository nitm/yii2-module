<?php

namespace nitm\traits;

use Yii;
use nitm\helpers\ArrayHelper;
use nitm\helpers\Cache as CacheHelper;
use nitm\helpers\Model as ModelHelper;

trait TreeTrait
{
    public static $TREE = [];

    /**
     * Get cached tree structure of category objects.
     *
     * @return array
     */
    public static function tree()
    {
        $cache = Yii::$app->cache;
        $key = static::tableName().'_tree';

        $tree = $cache->get($key);
        if (!$tree) {
            $tree = static::generateTree();
            $cache->set($key, $tree, 3600);
        }

        return $tree;
    }

    /**
     * Generates tree from categories.
     *
     * @return array
     */
    public static function generateTree()
    {
        $collection = static::find()->sort()->asArray()->all();
        $trees = array();
        $l = 0;

        if (count($collection) > 0) {
            // Node Stack. Used to help building the hierarchy
            $stack = array();

            foreach ($collection as $node) {
                $item = $node;
                unset($item['lft'], $item['rgt'], $item['priority']);
                $item['children'] = array();

                // Number of stack items
                $l = count($stack);

                // Check if we're dealing with different levels
                while ($l > 0 && $stack[$l - 1]->depth >= $item['depth']) {
                    array_pop($stack);
                    --$l;
                }

                // Stack is empty (we are inspecting the root)
                if ($l == 0) {
                    // Assigning the root node
                    $i = count($trees);
                    $trees[$i] = (object) $item;
                    $stack[] = &$trees[$i];
                } else {
                    // Add node to parent
                    $item['parent'] = $stack[$l - 1]->id;
                    $i = count($stack[$l - 1]->children);
                    $stack[$l - 1]->children[$i] = (object) $item;
                    $stack[] = &$stack[$l - 1]->children[$i];
                }
            }
        }

        return $trees;
    }

    public function getNestedList($id = null, $label = null, $options = [])
    {
        $id = $id ?: $this->idAttribute;
        $label = $label ?: $this->labelAttribute;

        return CacheHelper::remember([$this->owner->isWhat(), $options, 'nested-list', true], function () use ($options, $label, $id) {
            $ret_val = [null => 'Select...'];
            $options['where']['depth'] = 0;
            $options['with'] = ['children', 'parent'];
            $options['select'] = array_merge((array) ArrayHelper::getValue($options, 'select', []), ['depth', 'parent_id', 'lft', 'rgt', 'priority']);
            list($items, $id, $label, $realLabel, $separator) = $this->getNestedItems($id, $label, $options);
            if (count($items) >= 1) {
                $ret_val = $ret_val + self::populateNestedList($items, function ($item, $label, $separator) {
                    return ModelHelper::getLabel($item, $label, $separator);
                }, [
                     'label' => $label,
                     'separator' => $separator,
                ]);
            }

            return $ret_val;
        });
    }

    protected function getNestedItems($id = null, $label = null, $options = [])
    {
        $separator = ArrayHelper::remove($options, 'separator');
        $class = $this->owner->className();
        $separator = is_null($separator) ? ' ' : $separator;
        $realLabel = is_callable($label) ? ModelHelper::getTitleAttribute($this->owner) : $label;
        $options['select'] = array_merge((array) ArrayHelper::getValue($options, 'select', []), [$id, $realLabel]);
        if (!isset($options['orderBy'])) {
            $options['orderBy'] = [(is_array($realLabel) ? end($realLabel) : $realLabel) => SORT_ASC];
        }
        $options['groupBy'] = ArrayHelper::getValue($options, 'groupBy', [$id]);
        $options['indexBy'] = ArrayHelper::getValue($options, 'indexBy', $id);

        return [ModelHelper::locateItems($this->owner, $options), $id, $label, $realLabel, $separator];
    }

    protected function populateNestedList($items, $callable, $options, $canStop = false)
    {
        extract($options);
        $ret_val = [];
        foreach ($items as $idx => $item) {
            $itemLabel = str_repeat('&nbsp;&nbsp;&nbsp;&nbsp;', $item->depth).call_user_func_array($callable, [
                $item, $label, $separator,
             ]);
            $ret_val[$item[$this->idAttribute]] = $itemLabel;
            if (count($item->children)) {
                $ret_val += static::populateNestedList($item->children, $callable, $options);
            }
        }

        return $ret_val;
    }
}
