<?php

namespace nitm\traits\relations;

use nitm\helpers\Cache;
use nitm\models\Profile as ProfileModel;
use yii\helpers\ArrayHelper;

/**
 * Traits defined for expanding active relation scopes until yii2 resolves traits issue.
 */
trait User
{
    public function properName($value = null, $plural = false)
    {
        return static::formName();
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfile()
    {
        return $this->hasOne(\Yii::$app->getModule('user')->modelMap['Profile'], ['user_id' => 'id']);
    }

    /**
     * Get the status value for a user.
     *
     * @return string
     */
    public function status()
    {
        return \nitm\models\User::getStatus($this);
    }

    public function indicator($user)
    {
        return \nitm\models\User::getIndicator($user);
    }

    /**
     * Get the role value for a user.
     *
     * @return string name of role
     */
    public function role()
    {
        return \nitm\models\User::getRole($this);
    }


    public function getRole()
    {
        $roles = $this->getRoles();
        if (count($roles)) {
            return array_pop($roles);
        } else {
            return null;
        }
    }

    public function getRoles($clear = false)
    {
        if (!$this->id) {
            return [];
        }
        $key = ['user-roles', $this->id];
        if ($clear) {
            Cache::forget($key);
        }

        return Cache::remember($key, function () {
            $auth = \Yii::$app->authManager;

            return $auth->getRolesByUser($this->id);
        }, true, 3600);
    }

    public function isAdmin()
    {
        $module = \Yii::$app->getModule('user');

        return (\Yii::$app->getAuthManager() && $this->getRole() && strtolower($this->getRole()->name) == 'admin') || in_array($this->username, $module->admins);
    }

    /**
     * Does this user have tokens?
     *
     * @param User $user object
     *
     * @return string
     */
    public function getApiTokens()
    {
        return $this->hasMany(\nitm\models\api\Token::className(), ['userid' => 'id'])->all();
    }

    public function url($fullName = false, $url = null, $options = [], $text = null)
    {
        $url = is_null($url) ? 'user/profile/'.$this->id : $url;
        $urlOptions = array_merge([$url], $options, ['id' => $this->id]);
        $text = $text ?: (($fullName === false) ? $this->username : $this->fullname());
        $htmlOptions = [
            'href' => \Yii::$app->urlManager->createUrl($urlOptions),
            'role' => 'userLink',
            'id' => 'user'.uniqid(),
        ];

        return \nitm\helpers\Html::tag('a', $text, $htmlOptions);
    }

    public function setAvatar($value) {
      if(class_exists('\nitm\filemanager\Module')) {
        $imageModel = new \nitm\filemanager\models\Image;
        $avatar = \nitm\filemanager\helpers\ImageHelper::saveImages($imageModel, 'avatar', $this->id, 'avatar', \yii\web\UploadedFile::getInstances($imageModel, 'avatar'));
        $this->setAttribute('avatar',$avatar[0]->url());
      } else {
        $this->setAttribute('avatar', $value->saveAs("avatars/$this->id/".$value->name, true));
      }
    }

    public function avatarImg($options = [])
    {
        return \nitm\helpers\Html::img($this->avatar(), $options);
    }

    /**
     * Get the avatar.
     *
     * @param mixed $options
     *
     * @return string
     */
    public function avatar()
    {
      if($this->avatar) {
        return $this->avatar;
      }

      switch (Cache::cache()->exists('user-avatar'.json_encode($this->getId()))) {
          case false:
          $profile = $this->profile();
          $url = $this->resolveAvatar($this->email, $profile);
          Cache::cache()->set('user-avatar'.json_encode($this->getId()), urlencode($url), 3600);
          break;

          default:
          $url = urldecode(Cache::cache()->get('user-avatar'.json_encode($this->getId())));
          break;
      }

      return [
        'url' => $url,
        'metadata' => []
      ];
    }

    public static function resolveAvatar($key=null, $profile = null)
    {
        if (is_array($key) || is_object($key)) {
            if (is_null($profile)) {
                $profile = $key['profile'];
            }
        }
        $profile = ArrayHelper::toArray($profile);
        if (is_array($profile)) {
            switch (1) {
                case !empty($email = ArrayHelper::getValue($profile, 'gravatar_email')):
                $key = $profile['gravatar_email'];
                break;

                case !empty($email = ArrayHelper::getValue($profile, 'gravatar_id')):
                $key = $profile['gravatar_id'];
                break;

                default:
                $key = ArrayHelper::getValue($profile, 'public_email');
                break;
            }
        }

        return 'https://gravatar.com/avatar/'.md5($key);
    }

    public function getFullName()
    {
        return $this->fullName();
    }

    /**
     * Get the fullname of a user.
     *
     * @param bool $withUsername
     *
     * @return string
     */
    public function fullName($withUsername = false)
    {
        switch (is_object(\yii\helpers\ArrayHelper::getValue($this->getRelatedRecords(), 'profile', null))) {
            case true:
            $ret_val = $this->profile->name.($withUsername ? '('.$this->username.')' : '');
            break;

            default:
            $ret_val = $this->username;
            break;
        }

        return $ret_val ?? $this->username;
    }

    public function getSort()
    {
        $sort = [
            'username' => [
                'asc' => [$this->tableName().'.username' => SORT_ASC],
                'desc' => [$this->tableName().'.username' => SORT_DESC],
                'default' => SORT_DESC,
                'label' => 'Username',
            ],
        ];

        return array_merge(parent::getSort(), $sort);
    }
}
