<?php

namespace nitm;

use yii\helpers\Inflector;
use nitm\helpers\Session;
use nitm\models\DB;
use nitm\helpers\ArrayHelper;
use nitm\components\Logger;
use nitm\components\Dispatcher;

class Module extends \yii\base\Module implements \yii\base\BootstrapInterface
{
    /**
     * @string the module id
     */
    public $id = 'nitm';

    public $controllerNamespace = 'nitm\controllers';

    public $useFullnames;

    /**
     * Should ajax forms be enabled?
     * @param boolean
     */
    public $enableAjaxForms = true;

    /**
     * Should uuid support be enabled?
     * @param boolean
     */
    public $enableUuid = true;

    /**
     * Enable routes for the module?
     * @param boolean
     */
    public $enableRoutes = true;

    /**
     * Model caching options
     * Disable this if using a slower caching system.
     */
    public $useModelCache = true;

    /**
     * Should the configuration engine be loaded?
     */
    public $enableConfig = false;

    /**
     * Should the configuration admin interface be enabled?
     */
    public $enableConfigAdmin = false;

    /**
     * Should the logging engine be loaded?
     */
    public $enableLogger = false;

    /**
     * Should the logging route be loaded?
     */
    public $enableLogs = false;
    /**
     * Should the alerts engine be loaded?
     */
    public $enableAlerts = false;

    /**
     * Should Semantic UI be loaded?
     */
    public $enableSemanticUI = false;
    
    /**
     * Should Foundation UI be loaded?
     */
    public $enableFoundationUI = false;

    /*
     * @var array options for olliday\uuid\Uuid
     */
    public $uuid;

    /*
     * @var array options for nitm\models\Configer
     */
    public $config;

    /*
     * @var array options for nitm\models\Logger
     */
    public $logger;

    /**
     * The log collections that can be displayed.
     */
    public $logCollections = ['nitm-log'];

    /*
     * @var array options for nitm\models\Alerts
     */
    public $alerts;

    /**
     * Use this to map the carious types to their appropriate classes.
     */
    public $classMap = [];

    /*
     * @var array The arrap mapping for search classes
     */
    public $searchClassMap = [];

    //For Logger events
    const LOGGER_EVENT_PREPARE = 'nitm.logger.prepare';
    const LOGGER_EVENT_PROCESS = 'nitm.logger.process';

    public function init()
    {
        parent::init();
        //Check and start the session;
        Session::touchSession();
    }

    public function bootstrap($app)
    {
        /**
         * Setup urls.
         */
         $this->initUrlRules();
        $this->initMigrations([
             '@yii/rbac/migrations',
             '@dektrium/user/migrations',
             '@nitm/yii2-module/migrations',
         ]);
        $this->initGiiGenerators([
             'form' => [
                'templates' => [ //setting for out templates
                    'nitmForm' => \Yii::getAlias('@vendor/nitm/generators/form/default'), // template name => path to template
                ]
            ]
        ]);

        $this->initUITheme();

        $this->initContainerReplacements();
    }

    public function resolveClass($class)
    {
        if (isset($this->classMap[$class])) {
            return $this->classMap[$class];
        }

        return $class;
    }

    public function getSearchClass($modelName, $namespace = null)
    {
        $modelName = models\Data::properFormName(strtolower($modelName));
        $namespace = rtrim($namespace, '\\').'\\' ?: '\\nitm\\models\\search\\';
        if (!isset($this->searchClassMap[$modelName])) {
            $this->searchClassMap[$modelName] = $namespace.$modelName;
        }

        return $this->searchClassMap[$modelName];
    }

    protected function initContainerReplacements()
    {
        $replacements = [
            \yii\helpers\Html::class => \nitm\helpers\Html::class
        ];
        foreach ($replacements as $source=>$replacement) {
            \Yii::$container->set($source, $replacement);
        }
    }

    protected function initGiiGenerators($generators = [])
    {
        $app = \Yii::$app;
        if (YII_ENV_DEV) {
            $gii = $app->getModule('gii');
            if ($gii && !empty($generators)) {
                foreach ($generators as $generator => $config) {
                    if (!is_array($config)) {
                        $config = [
                            'templates' => $config
                        ];
                    }
                    $class = ArrayHelper::getValue($config, 'class', "yii\gii\generators\\{$generator}\Generator");
                    $currentGenerator = ArrayHelper::getValue($gii, 'generators.'.$generator);
                    $currentGenerator['class'] = $class;
                    if ($class && class_exists($class)) {
                        if (!$currentGenerator || $currentGenerator && is_array($currentGenerator) && !(is_a($currentGenerator['class'], $class)
                            || is_object($currentGenerator) && !($currentGenerator instanceof $class))) {
                            $currentGenerator = is_object($currentGenerator) ? ArrayHelper::toArray($currentGenerator) : $currentGenerator;
                            $currentGenerator = array_merge((array)$currentGenerator, $config);
                        }
                    }
                    $currentGenerator['templates'] = array_merge($currentGenerator['templates'], $config['templates']);
                    $gii->generators[$generator] = $currentGenerator;
                }
            }
        }
    }

    /**
     * Get the current module theme
     *
     * @return void
     */
    public function getUITheme() {
        if ($this->enableSemanticUI) {
            return 'semantic';
        } elseif ($this->enableFoundationUI) {
            return 'foundation';
        }
    }

    /**
     * init the UI Theme
     *
     * @param [type] $theme
     * @param array $widgets
     * @return void
     */
    public function initUITheme($theme=null, $widgets = [])
    {
        $theme = $theme ?: $this->UITheme;
        if($theme) {
            $replacements = \nitm\helpers\WidgetHelper::getUIClassMap($widgets, $theme);
            foreach ((array)$replacements as $source=>$replacement) {
                \Yii::$container->set($source, $replacement);
            }
        }
    }

    protected function initMigrations($migrations=[])
    {
        $app = \Yii::$app;
        if (!isset($app->controllerMap['migrate'])) {
            $app->controllerMap['migrate'] = [
                'class' => 'dmstr\console\controllers\MigrateController'
            ];
        }
        $app->params['yii.migrations'] = array_merge(ArrayHelper::getValue($app->params, 'yii.migrations', []), $migrations);
    }

    protected function initViews($views=[])
    {
        if (\Yii::$app->view->theme) {
            $theme = \Yii::$app->view->theme;
            $theme->pathMap = array_merge($theme->pathMap, $views);
        }
    }

    protected function initUrlRules()
    {
        if ($this->enableRoutes) {
            $manager = \Yii::$app->urlManager;
            $manager->enablePrettyUrl = true;
            $manager->showScriptName = false;
            $manager->addRules($this->urls);
        }
    }

    /**
     * Generate routes for the module.
     *
     * @method getUrls
     *
     * @param string $id The id of the module
     *
     * @return array The routes
     */
    public function getUrls($id = 'nitm')
    {
        $parameters = [];
        $routeHelper = new helpers\Routes([
            'moduleId' => $id,
            'map' => [
            ],
            'controllers' => [],
        ]);
        $parameters = [];
        if ($this->enableConfigAdmin) {
            $routeHelper->addRules('configuration', [
                'config-index' => ['configuration' => 'configuration/index'],
                'config-engine' => ['configuration/load/<engine>' => 'configuration'],
                'config-action' => ['configuration/<action>' => 'configuration'],
                'config-container' => ['configuration/load/<engine:\w+>/<container>' => 'configuration'],
            ]);
            $parameters += [
                'config-index' => ['configuration'],
                'config-engine' => ['configuration'],
                'config-container' => ['configuration'],
            ];
            $parameters['action-only'] = $routeHelper->getControllerMap(['configuration']);
        }
        if ($this->enableLogs) {
            $routeHelper->addRules('log', [
                'log-index-base' => ['log' => 'log/index'],
                'log-type' => ['log/<type:(!index)>' => 'log/index'],
                'log-index' => ['log/index' => 'log/index'],
                'log-index-type' => 'log/index/<type>',
            ]);
            $parameters += [
                'log-index-base' => ['log'],
                'log-type' => ['log'],
                'log-index' => ['log'],
                'log-index-type' => ['log'],
            ];
        }
        $routes = $routeHelper->create($parameters);

        return $routes;
    }

    /**
     * Can the module log this information?
     *
     * @method canLog
     *
     * @param int $level The log level
     *
     * @return bool Whether the module can log
     */
    public function canLog($level = null)
    {
        return (bool) ($this->enableLogger ? $this->getLogger()->canLog($level) : false);
    }

    /**
     * Commit the entire log tree.
     *
     * @method commitLog
     *
     * @return bool The result of the log operation
     */
    public function commitLog()
    {
        return $this->enableLogger ? $this->getLogger()->trigger(Logger::EVENT_END) : false;
    }

    public function hasComponent($name)
    {
        return $this->hasMethod('get'.$name) && is_object(call_user_func([$this, 'get'.$name]));
    }

    /**
     * Add a message to the log queue.
     *
     * @method log
     *
     * @param string|int $level   The level to log at
     * @param array      $options The optional extra data to log
     * @param object     $sender  The model to use for this log operation
     *
     * @return bool [description]
     */
    public function log($options, $level, $category, $sender)
    {
        if ($this->canLog($level)) {
            try {
                $collectionName = $this->getCollectionName($options);
                $options = array_merge([
                    'db_name' => models\DB::getDbName(),
                    'level' => $level,
                    'timestamp' => time(),
                    'collectionName' => $collectionName,
                ], $options);
                $this->getLogger()->log($options, $level, ArrayHelper::getValue($options, 'category', $category), $collectionName);
            } catch (\Exception $e) {
                //if(defined("YII_DEBUG"))
                    throw $e;
            }
        }

        return true;
    }

    public function getCollectionName(&$from = [])
    {
        return ArrayHelper::remove($from, 'collection_name', (($this->logCollections != []) ? $this->logCollections[0] : 'nitm-log'));
    }

    /**
     * Can this user send this alert?
     *
     * @method canSendAlert
     *
     * @return bool Can the user send the alert?
     */
    public function canSendAlert()
    {
        $ret_val = $this->enableAlerts
            && \Yii::$app->getRequest()->get(Dispatcher::SKIP_ALERT_FLAG) != true;

        return (bool) $ret_val;
    }

    /**
     * Get the alert object.
     *
     * @method getAlert
     *
     * @param [type] $force [description]
     *
     * @return [type] [description]
     */
    public function getAlerts($force = false)
    {
        if (!is_object($this->alerts) && $this->enableAlerts) {
            $this->alerts = \Yii::createObject(array_merge([
                'class' => '\nitm\components\Dispatcher',
            ], (array) $this->alerts));
        }

        return $this->alerts;
    }

    /**
     * Get the logger.
     *
     * @method getLogger
     *
     * @param [type] $force [description]
     *
     * @return [type] [description]
     */
    public function getLogger($force = false)
    {
        if (!is_object($this->logger) && $this->enableLogger) {
            $this->logger = \Yii::createObject(array_merge([
                'class' => '\nitm\components\Logger',
                'categories' => [$this->id]
            ], (array) $this->logger));
        }

        return $this->logger;
    }

    /**
     * Get the config component.
     *
     * @method getConfiger
     *
     * @param [type] $force [description]
     *
     * @return [type] [description]
     */
    public function getConfig($force = false)
    {
        if (!is_object($this->config) && $this->enableConfig) {
            $this->config = \Yii::createObject(array_merge([
                'class' => '\nitm\components\Configer',
            ], (array) $this->config));
        }

        return $this->config;
    }

    /**
     * Get the config component.
     *
     * @method getConfiger
     *
     * @param [type] $force [description]
     *
     * @return [type] [description]
     */
    public function getUuid($force = false)
    {
        if (!is_object($this->uuid) && $this->enableUuid) {
            $this->uuid = \Yii::createObject(array_merge([
                'class' => '\ollieday\uuid\Uuid',
            ], (array) $this->uuid));
        }

        return $this->uuid;
    }
}
