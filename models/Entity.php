<?php

namespace nitm\models;

class Entity extends Data
{
    use \nitm\traits\Nitm, \nitm\traits\RelatedRecords;

    public $hasNewActivity;

    public function scenarios()
    {
        return array_merge(parent::scenarios(), $this->nitmScenarios());
    }

    public function behaviors()
    {
        $behaviors = [];
        if ($this->hasAttribute('deleted_at')) {
            $behaviors['softDelete'] = [
               'class' => \nitm\behaviors\SoftDelete::className(),
            ];
        }

        if ($this->hasAttribute('author_id')) {
            $behaviors['blamable'] = [
            'class' => \yii\behaviors\BlameableBehavior::className(),
                'attributes' => [
                    \yii\db\ActiveRecord::EVENT_BEFORE_INSERT => 'author_id',
                ],
                'value' => function ($event) {
                    if (\Yii::$app->user->identity instanceof \nitm\cms\models\Admin) {
                        return \Yii::$app->user->identity->publicUser->id;
                    } else {
                        return \Yii::$app->user->identity->id;
                    }
                }
            ];
        }

        if ($this->hasAttribute('editor_id')) {
            $behaviors['blamable']['attributes'][\yii\db\ActiveRecord::EVENT_BEFORE_UPDATE] = 'editor_id';
        }

        if ($this->hasAttribute('created_at')) {
            $behaviors['timestamp'] = [
             'class' => \yii\behaviors\TimestampBehavior::className(),
                 'attributes' => [
                     \yii\db\ActiveRecord::EVENT_BEFORE_INSERT => 'created_at',
                 ],
                 'value' => new \yii\db\Expression('NOW()'),
            ];
        }
        if ($this->hasAttribute('updated_at')) {
            $behaviors['timestamp']['attributes'][\yii\db\ActiveRecord::EVENT_BEFORE_UPDATE] = 'updated_at';
        }

        $behaviors['dates'] = [
            'class' => \nitm\behaviors\DateAttributes::className(),
            'attributes' => ['deleted_at', 'created_at', 'updated_at'],
        ];

        return array_merge(parent::behaviors(), $behaviors);
    }

    public function initalizeEventData(&$event)
    {
        $event->data = [
            'action' => $event->sender->getScenario(),
            'variables' => [
                '%id%' => $event->sender->getId(),
                '%viewLink%' => \nitm\helpers\Html::a(
                    \Yii::$app->urlManager->createAbsoluteUrl($event->sender->isWhat().'/view/'.$event->sender->getId()),
                    \Yii::$app->urlManager->createAbsoluteUrl($event->sender->isWhat().'/view/'.$event->sender->getId())
                ),
            ],
        ];
    }

    /**
     * Get the query that orders items by their activity.
     */
    public function getSort()
    {
        $ret_val = [];
        //Create the user sort parameters
        static::addSortParams($ret_val, [
            'author_id' => ['author', 'Author', 'username'],
            'editor_id' => ['editor', 'Updateor', 'username'],
            'resolved_by' => ['resolvedBy', 'Resolved By', 'username'],
            'closed_by' => ['closedBy', 'Closed By', 'username'],
            'disabled_by' => ['disabledBy', 'Disabled By', 'username'],
            'deleted_by' => ['deletedBy', 'Deleted By', 'username'],
            'completed_by' => ['completedBy', 'Completed By', 'username'],
        ]);

        //Create the date sort parameters
        static::addSortParams($ret_val, [
            'created_at' => [null, 'Created At'],
            'updated_at' => [null, 'Updated At'],
            'resolved_at' => [null, 'Resolved At'],
            'closed_at' => [null, 'Closed At'],
            'disabled_at' => [null, 'Disabled At'],
            'deleted_at' => [null, 'Deleted At'],
            'completed_at' => [null, 'Completed At'],
        ]);

        $ret_val['date'] = [
            'asc' => ['created_at' => SORT_ASC, 'updated_at' => SORT_ASC],
            'desc' => ['created_at' => SORT_DESC, 'updated_at' => SORT_DESC],
            'default' => SORT_DESC,
            'label' => 'Date',
        ];

        //Create the category sort parameters
        static::addSortParams($ret_val, [
            'type_id' => ['type', 'Type', 'name'],
            'category_id' => ['category', 'Category', 'name'],
            'level_id' => ['level', 'Level', 'name'],
        ]);

        return $ret_val;
    }

    public function getAlertOptions($event)
    {
        $event->sender->initalizeEventData($event);
        $options = [
            'criteria' => [
                'action' => $event->sender->getScenario(),
                'priority' => 'normal',
                'remote_type' => $event->sender->isWhat(),
                'remote_for' => 'any',
            ],
        ];
        foreach (['created_at', 'updated_at', 'resolved_at', 'completed_at', 'disabled_at'] as $dateAttribute) {
            if ($event->sender->hasAttribute($dateAttribute)) {
                $attribute = '%'.\nitm\helpers\ClassHelper::variableName($dateAttribute).'%';
                $date = $event->sender->$dateAttribute instanceof \yii\db\Expression ? strtotime('now') : $event->sender->$dateAttribute;
                $options['variables'][$attribute] = \Yii::$app->formatter->asDatetime(strtotime($date));
            }
        }

        switch ($event->sender->getScenario()) {
            case 'create':
                $options = array_merge($options, [
                'subject' => ['view' => '@app/mail/subjects/new/'.$event->sender->isWhat()],
                'message' => [
                    'email' => ['view' => '@app/mail/email/new/'.$event->sender->isWhat()],
                    'mobile' => ['view' => '@app/mail/mobile/new/'.$event->sender->isWhat()],
                ],
                ]);
                break;

            case 'complete':
            case 'resolve':
            case 'disable':
            case 'update':
            case 'close':
                $options = array_merge($options, [
                'subject' => ['view' => '@app/mail/subjects/update/'.$event->sender->isWhat()],
                'message' => [
                    'email' => ['view' => '@app/mail/email/update/'.$event->sender->isWhat()],
                    'mobile' => ['view' => '@app/mail/mobile/update/'.$event->sender->isWhat()],
                ],
                ]);
                break;
        }
        if (!count($options) && !$event->sender->getId()) {
            $options['owner_id'] = $event->sender->hasAttribute('author_id') ? $event->sender->author_id : null;
        }

        return $options;
    }
}
