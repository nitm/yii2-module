<?php

/**
 * Base Data getter/operation class.
 */

namespace nitm\models;

use yii\base\Event;
use nitm\db\ActiveRecord;
use nitm\helpers\ArrayHelper;

class Data extends ActiveRecord implements \nitm\interfaces\DataInterface
{
    use \nitm\traits\Parents, \nitm\traits\Log, \nitm\traits\Alerts;

    protected static $supported;

    //Event
    const AFTER_ADD_PARENT_MAP = 'afterAddParentMap';

    public function rules()
    {
        return [
            [['filter'], 'required', 'on' => ['filtering']],
            [['unique'], 'safe'],
        ];
    }

    public function scenarios()
    {
        return array_merge(parent::scenarios(), [
            'filter' => ['filter'],
            'deleted' => ['deleted_at'],
        ]);
    }

    public function attributes()
    {
        if (static::tableName()) {
            return array_merge(parent::attributes(), [
                '_count', '_newCount',
            ]);
        } else {
            return [];
        }
    }

    public function behaviors()
    {
        $behaviors = [
           'config' => \nitm\behaviors\Settings::class
        ];

        return array_merge(parent::behaviors(), $behaviors);
    }

    protected function attachHasBehaviors(&$behaviors, $behaviorOptions = [])
    {
        extract($behaviorOptions);
        $extra = is_array($extra) && ArrayHelper::isIndexed($extra) ? [] : (!is_array($extra) ? [] : $extra);
        $toAttach = array_filter(array_merge($default, $extra), [$this, 'hasProperty']);
        if (count($toAttach)) {
            $behaviors[$name] = [
                'class' => $class,
            ];
            if (isset($value)) {
                $behaviors[$name]['value'] = $value;
            }
            foreach ($toAttach as $attribute => $options) {
                $options = is_array($options) ? $options : ['on' => $options];
                switch ($options['on']) {
                    case 'create':
                        $event = ActiveRecord::EVENT_BEFORE_INSERT;
                        break;

                    case 'delete':
                        $event = ActiveRecord::EVENT_BEFORE_DELETE;
                        break;

                    default:
                        $event = ActiveRecord::EVENT_BEFORE_UPDATE;
                        break;
                }
                $behaviors[$name]['attributes'][$event] = $attribute;
            }
        }
    }

    public function afterSave($insert, $changedAttributes)
    {
        /**
         * If this has parents specified then check and add them accordingly.
         */
        if (!empty($this->parent_ids)) {
            $this->addParentMap($this->parent_ids);
        }

        return parent::afterSave($insert, $changedAttributes);
    }

    public static function filters()
    {
        return [
            'author' => null,
            'editor' => null,
            'status' => null,
            'order' => null,
            'order_by' => null,
            'index_by' => null,
            'show' => null,
            'limit' => null,
            'unique' => null,
            'boolean' => null,
        ];
    }

    public static function has()
    {
        return [
              'created_at' => null,
              'updated_at' => null,
            ];
    }

    public function getSearchClass()
    {
        $namespace = explode('\\', $this->namespace);
        if (end($namespace) != 'search') {
            $namespace[] = 'search';
        }

        return \Yii::$app->getModule('nitm')->getSearchClass($this->isWhat(), implode('\\', $namespace));
    }

    public function getCategoryClass()
    {
        $parts = explode('\\', static::className());
        $modelName = array_shift($parts);
        $class = implode('\\', $parts).'\\'.((strpos($modelName, 'Category') !== false) ? $modelName : $modelName.'Category');

        return class_exists($class) ? $class : \nitm\models\Category::className();
    }

    /*
     * Check to see if somethign is supported
     * @param mixed $what
     */
    public function isSupported($what)
    {
        $thisSupports = [$what => false];
        switch (is_array(static::$supported)) {
            case true:
                $thisSupports = static::$supported;
                break;

            default:
                $thisSupports = $this->setting('supported');
                break;
        }

        return isset($thisSupports[$what]) && ($thisSupports[$what] == true);
    }

    /**
     * Return data from source specified by $getter.
     *
     * @param array           $source. Indexed by the ids of the items
     * @param bool            $dsOnly  Only return the ids
     * @param array|callablke $getter
     *
     * @return array
     */
    protected function filterData(array $source, $idsOnly = false, $getter = null)
    {
        return ArrayHelper::filter($source, $idsOnly, $getter);
    }
}
