<?php

namespace nitm\models\query;

/**
 * This is the ActiveQuery class for [[\nitm\models\Activity]].
 *
 * @see \nitm\models\Activity
 */
class ActivityQuery extends \nitm\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return \nitm\models\Activity[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \nitm\models\Activity|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
