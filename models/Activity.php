<?php

namespace nitm\models;

use Yii;

/**
 * This is the model class for table "nitm_activity".
 *
 * @property integer $id
 * @property string $title
 * @property string $verb
 * @property string $created_at
 * @property string $actor
 * @property string $object
 * @property integer $user_id
 * @property boolean $is_admin_action
 * @property string $updated_at
 */
class Activity extends \nitm\db\ActiveRecord
{
    public function behaviors()
    {
        $behaviors = [
            'jsonable' => [
                'class' => \nitm\behaviors\JsonableAttributes::className(),
                'attributes' => ['object', 'actor', 'target']
            ],
            'timestamp' => [
                'class' => \yii\behaviors\TimestampBehavior::className(),
                'attributes' => [
                    \yii\db\ActiveRecord::EVENT_BEFORE_INSERT => 'created_at',
                    \yii\db\ActiveRecord::EVENT_BEFORE_UPDATE => 'created_at',
                ],
                'value' => new \yii\db\Expression('NOW()'),
            ],
        ];

        return array_replace_recursive(parent::behaviors(), $behaviors);
    }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'nitm_activity';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['verb', 'user_id'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['actor', 'object', 'target'], 'safe'],
            [['user_id'], 'integer'],
            [['is_admin_action'], 'boolean'],
            [['title', 'verb'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'verb' => Yii::t('app', 'Verb'),
            'created_at' => Yii::t('app', 'Created At'),
            'actor' => Yii::t('app', 'Actor'),
            'object' => Yii::t('app', 'Object'),
            'user_id' => Yii::t('app', 'User ID'),
            'is_admin_action' => Yii::t('app', 'Is Admin Action'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @inheritdoc
     * @return \nitm\models\query\ActivityQuery the active query used by this AR class.
     */
    public static function find($model=null, $options=[])
    {
        return new \nitm\models\query\ActivityQuery(get_called_class());
    }

    public function fields()
    {
        return [
            'id',
            'verb',
            'published' => 'created_at',
            'title',
            'displayName' => 'title',
            'actor' => function ($model) {
                return array_intersect_key($model->attributeToArray('actor'), array_flip([
                    'id', 'username', 'avatar'
                ]));
            },
            'object' => function ($model) {
                return $model->attributeToArray('object');
            },
            'target' => function ($model) {
                return $model->attributeToArray('target');
            },
            'objectType' => 'object_type'
        ];
    }
}
