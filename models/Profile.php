<?php

namespace nitm\models;

use dektrium\user\models\Profile as UserProfile;
use nitm\helpers\ArrayHelper;

/**
 * Class Profile.
 */
class Profile extends UserProfile
{
    use \nitm\traits\Data, \nitm\traits\Query, \nitm\traits\relations\Profile, \nitm\traits\Relations;

    public function behaviors()
    {
        $behaviors = [
            'relationManager' => [
                'class' => \nitm\behaviors\RelationManagement::className(),
            ],
            'changedAttributes' => [
                'class' => \nitm\behaviors\ChangedAttributes::className(),
            ],
            'list' => [
              'class' => \nitm\behaviors\RecordList::class,
            ],
            'search' => [
                'class' => \nitm\search\behaviors\SearchModel::class,
            ],
        ];

        return array_merge(parent::behaviors(), $behaviors);
    }

    /** {@inheritdoc} */
    public function scenarios()
    {
        $scenarios = parent::scenarios();

        return ArrayHelper::merge($scenarios, [
            'update' => array_merge($this->attributes(), ['user', 'avatar']),
            'default' => array_merge($this->attributes(), ['user', 'avatar']),
        ]);
    }

    /**
     * [setTimeZone description].
     *
     * @param [type] $timezone [description]
     */
    public function setTimeZone(\DateTimeZone $timezone = null)
    {
        $timezone = $timezone ?: new \DateTimeZone(\Yii::$app->timezone);
        parent::setTimeZone($timezone);
    }

    /**
     * [getSort description].
     *
     * @return [type] [description]
     */
    public function getSort()
    {
        $sort = [];

        return array_merge(\nitm\models\Data::getSort(), $sort);
    }
}
