<?php
namespace nitm\models\log;

trait EntryTrait {

    public static $collectionName = 'nitm-log';
    public static $namespace = "\nitm\models\log";

    public function getStatus()
    {
        $ret_val = 'default';
        switch (1) {
            case $this->level >= 4:
            $ret_val = 'danger';
            break;

            case $this->level == 3:
            $ret_val = 'warning';
            break;

            case $this->level == 2:
            $ret_val = 'info';
            break;

            case $this->level == 1:
            $ret_val = 'success';
            break;

            default:
            $ret_val = 'default';
            break;
        }
        return $ret_val;
    }

    public function getIp() {
        try {
            return inet_pton($this->ip_addr);
        } catch (\Exception $e) {
            return $this->ip_addr;
        }
    }
}

?>