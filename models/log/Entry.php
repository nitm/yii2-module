<?php

namespace nitm\models\log;

use Yii;
use yii\helpers\Inflector;

/**
 * This is the model class for collection "lab1-provisioning-log".
 *
 * @property \MongoId|string $_id
 * @property mixed $message
 * @property mixed $level
 * @property mixed $internal_category
 * @property mixed $category
 * @property mixed $timestamp
 * @property mixed $action
 * @property mixed $db_name
 * @property mixed $table_name
 * @property mixed $user_id
 * @property mixed $ip_addr
 * @property mixed $host
 */
class Entry extends \nitm\search\BaseSearch
{
    use EntryTrait;
}
