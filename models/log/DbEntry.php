<?php

namespace nitm\models\log;

use Yii;
use nitm\models\User;

/**
 * This is the model class for table "logs".
 *
 * @property integer $id
 * @property string $ip_addr
 * @property string $host
 * @property integer $user_id
 * @property string $action
 * @property string $db_name
 * @property string $table_name
 * @property string $message
 * @property string $prefix
 * @property string $category
 * @property integer $level
 * @property string $log_time
 *
 * @property User $user
 */
class DbEntry extends \nitm\db\ActiveRecord
{
    use EntryTrait;
    
    protected $skipInit = false;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'nitm_logs';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ip_addr', 'host', 'user_id', 'action', 'db_name', 'message', 'prefix', 'category', 'log_time'], 'required'],
            [['ip_addr', 'message', 'prefix', 'category'], 'string'],
            [['user_id', 'level'], 'integer'],
            [['log_time'], 'safe'],
            [['host', 'action'], 'string', 'max' => 128],
            [['db_name', 'table_name'], 'string', 'max' => 64]
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
