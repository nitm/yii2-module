<?php

namespace nitm\models;

use yii\helpers\Security;
use nitm\helpers\Cache;
use nitm\helpers\ArrayHelper;

/**
 * Class User.
 *
 * @property int $id
 * @property string $username
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $token
 * @property string $email
 * @property string $auth_key
 * @property int $role
 * @property int $status
 * @property int $create_time
 * @property int $update_time
 * @property string $password write-only password
 */
class User extends \dektrium\user\models\User
{
    use \nitm\traits\Data, \nitm\traits\Query, \nitm\traits\relations\User, \nitm\traits\Relations;


    /**
     * @var string Default username regexp
     * Custom regexp for username that allows special characters: [+, ., @]
     */
    public static $usernameRegexp = '/^[-a-zA-Z0-9_\.@\+]+$/';

    public $updateActivity;
    public $useFullnames;

    protected $useToken;

    private $_lastActivity = '__lastActivity';

    public function init()
    {
        if ($this->updateActivity) {
            $this->updateActivity();
        }
        parent::init();
    }

    public static function tableName()
    {
        return parent::tableName();
    }

    public function behaviors()
    {
        $behaviors = [
            'relationManager' => [
                'class' => \nitm\behaviors\RelationManagement::className(),
            ],
            'changedAttributes' => [
                'class' => \nitm\behaviors\ChangedAttributes::className(),
            ],
            'list' => [
              'class' => \nitm\behaviors\RecordList::class,
            ],
      ];
        if (class_exists('\nitm\search\Module')) {
            $behaviors['search'] = [
             'class' => \nitm\search\behaviors\SearchModel::class,
          ];
        }

        return array_merge(parent::behaviors(), $behaviors);
    }

    /** {@inheritdoc} */
    public function scenarios()
    {
        $scenarios = parent::scenarios();

        return ArrayHelper::merge($scenarios, [
            'create' => ['avatar'],
            'update' => ['avatar'],
            'default' => ['avatar'],
        ]);
    }

    /**
     * Get the records for this provisioning template.
     *
     * @param bool $idsOnly
     *
     * @return mixed Users array
     */
    public function getAll($idsOnly = true)
    {
        $ret_val = array();
        switch (Cache::cache()->exists('nitm-user-list')) {
            case false:
            $users = $this->find()->asArray()->all();
            switch ($idsOnly) {
                case true:
                foreach ($users as $user) {
                    $ret_val[$user['id']] = $user['f_name'].' '.$user['l_name'];
                }
                break;

                default:
                $ret_val = $users;
                break;
            }
            Cache::cache()->set('nitm-user-list', urlencode($url), 3600);
            break;

            default:
            $ret_val = urldecode(Cache::cache()->get('nitm-user-list'));
            break;
        }

        return $ret_val;
    }

    /**
     * Get the actvity counter.
     *
     * @param bool $update Should the activity be updated
     *
     * @return bool
     */
    public function lastActive($update = false)
    {
        $ret_val = strtotime('now');
        if (\Yii::$app instanceof \yii\web\Application) {
            $sessionActivity = \Yii::$app->getSession()->get($this->_lastActivity);
            switch (is_null($sessionActivity)) {
                case true:
                $user = \Yii::$app->getUser()->getIdentity();
                if ($user) {
                    $ret_val = !$user->getId() ? strtotime('now') : $user->last_active_at;
                }
                break;

                default:
                $ret_val = $sessionActivity;
                break;
            }
            if ($update) {
                $this->updateActivity();
            }
        }

        return date('Y-m-d G:i:s', strtotime($ret_val));
    }

    /**
     * Update the user activity counter.
     */
    public function updateActivity()
    {
        if (\Yii::$app instanceof \yii\console\Application) {
            return false;
        }

        return \Yii::$app->getSession()->set($this->_lastActivity, strtotime('now'));
    }

    /**
     * Should we use token authentication for this user?
     *
     * @param bool $use
     */
    public function useToken($use = false)
    {
        $this->useToken = ($use === true) ? true : false;
    }

    /**
     * Does this user have tokens?
     *
     * @param User $user object
     *
     * @return string
     */
    public function hasApiTokens()
    {
        return security\User::hasApiTokens($this);
    }

    /**
     * @inheritdoc
     * @return \nitm\models\query\UserQuery the active query used by this AR class.
     */
    public static function find($model=null, $options=[])
    {
        return (new \nitm\models\query\UserQuery(get_called_class()))
            ->with(['profile']);
    }

    public function getSort()
    {
        $sort = [
        ];

        return array_merge(\nitm\models\Data::getSort(), $sort);
    }

    public function getList($id ='id', $label='profile.name', $options = [])
    {
        return $this->getBehavior('list')->getList($id, $label, array_merge([
           'select' => ['id', 'username', 'email'],
           'joinWith' => 'profile',
           'with' => ['profile']
      ], $options));
    }

    public function getRoleName()
    {
        return $this->role ? $this->role->name : 'User';
    }

    public function getTitleAttribute()
    {
        return 'fullName';
    }

    public function title()
    {
        return $this->fullName(true);
    }

    public function getPublicId()
    {
        return '@'.$this->username;
    }
}
