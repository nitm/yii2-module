<?php

namespace nitm\helpers;

use yii\db\Expression;

/**
 * This is a utility classs for date formatting.
 */
class DateFormatter extends \yii\base\BaseObject
{
    /**
     * The \Yii::$app->formatter formatting function.
     *
     * @var string
     *             One of [asTimestamp, asDuration, asRelativeTime]
     */
    public $as;
    /**
     * Any time format supported by \Yii::$app->formatter.
     *
     * @var [type]
     */
    public $format;
    public $event = null;
    public $_value;
    protected $_valueOnEmpty;

    private static $_formats = [
        'mysql_hr' => '%a %b %d %Y %l:%i%p',
        'mysql_ts' => '%Y-%m-%d %H:%i:%s',
        'mysql_today' => 'Y-m-d H:i:s',
        'timestamp' => 'Y-m-d H:i:s',
        'default' => 'medium',
        'today' => 'D M j, Y',
    ];

    const FORMAT_MYSQL_READABLE = 'mysql_hr';
    const FORMAT_MYSQL_TIMESTAMP = 'mysql_ts';
    const FORMAT_TIMESTAMP = 'timestamp';
    const FORMAT_TODAY = 'today';
    const FORMAT_DEFAULT = 'default';

    public function __toString()
    {
        $value = $this->getValue();
        return (string)$value;
    }

    public function isEmpty()
    {
        return strlen($this->getValue());
    }

    protected function getDefault()
    {
        return in_array($this->event, ['init', 'beforeInsert', 'beforeUpdate', 'beforeDelete']) ? new Expression('NULL') : null;
    }

    public function getRawValue()
    {
        return strtotime($this->_value);
    }

    public function setValue($value)
    {
        $this->_value = $value instanceof self ? $value->rawValue : $value;
    }

    public function getValue()
    {
        $as = !$this->as ? 'asDbTimestamp' : (strpos($this->as, 'as') === false ? 'as'.$this->as : $this->as);
        $format = $this->getFormat($this->format);
        $value = $this->_value ?: $this->default;
        if ($value instanceof Expression || $value instanceof ActiveQuery) {
            return $value;
        }
        if (!empty($value)) {
            return $this->$as(is_numeric($value) ? $value : strtotime($value), 'php:'.$format);
        } else {
            return null;
        }
    }

    /**
     * If the date value is empty then display this value
     *
     * @param string $value
     * @return $this
     */
    public function valueOnEmpty($value)
    {
        $this->_valueOnEmpty = $value;
        return $this;
    }

    public function __call($name, $params)
    {
        if (method_exists($this, $name)) {
            return call_user_func_array([$this, $name], $params);
        } else {
            if ($params instanceof self) {
                $params = strtotime($params->rawValue);
                if (!$params) {
                    return null;
                }
            }
            if (empty($params)) {
                if (strtolower($name) == 'asdatetime') {
                    $params = [$this->_value, $this->getFormat($this->format)];
                } else {
                    $params = [$this->_value];
                }
            }
            if ($this->_value && $this->_value !== 'NULL') {
                try {
                    return call_user_func_array([\Yii::$app->formatter, $name], $params);
                } catch (\Exception $e) {
                    return $this->default;
                }
            }
            return $this->_valueOnEmpty ?? $this->default;
        }
    }

    public function asDbTimestamp()
    {
        $value = is_numeric($this->_value)  ? $this->_value : strtotime($this->_value);
        return date(static::getFormat(static::FORMAT_TIMESTAMP, true), $value);
    }

    public function asMysqlDbTimestamp()
    {
        $value = is_numeric($this->_value)  ? $this->_value : strtotime($this->_value);
        return date(static::getFormat(static::FORMAT_MYSQL_TIMESTAMP, true), $value);
    }

    /**
     * Return a date and formate to use.
     *
     * @param string       $format
     * @param int | string $date
     */
    public static function formatDate($date = null, $format = null)
    {
        $format = is_null($format) ? static::$_formats['default'] : static::getFormat($format);

        return \Yii::$app->formatter->asDateTime(strtotime(is_null($date) ? 'now' : $date), $format);
    }

    /**
     * Return the format if it is supported.
     */
    public static function getFormat($format = 'default', $forLocal = false)
    {
        switch (isset(static::$_formats[$format])) {
            case true:
                $ret_val = static::$_formats[$format];
                break;

            default:
                $ret_val = static::$_formats['default'];
                break;
        }

        return $forLocal ? substr($ret_val, strpos($ret_val, 'php:') ?? 0) : $ret_val;
    }
}
