<?php

namespace nitm\helpers;

use yii\base\Behavior;
use yii\web\JsExpression;

//class that sets up and retrieves, deletes and handles modifying of contact data
class Response extends Behavior
{
    public static $view;
    public static $controller;
    public static $format;
    public static $forceAjax = false;
    public static $viewPath = '@nitm/views/response/index';
    public static $viewModal = '@nitm/views/response/modal';

    protected static $encodedAs;
    protected static $_isFormatSpecifiedExplicitly = false;
    protected static $viewOptions = [
        'content' => '',
        'view' => '@nitm/views/response/index', //The view file
        'options' => [
            'class' => null,
        ],
    ];
    protected static $layouts = [
        'column1' => '@nitm/views/layouts/column1',
    ];

    public static function initContext($controller = null, $view = null)
    {
        static::$controller = !($controller) ? \Yii::$app->controller : $controller;
        static::$view = !($view) ? static::$controller->getView() : $view;
    }

    public static function viewOptions($name = null, $value = null, $append = false)
    {
        return ArrayHelper::getOrSetValue(static::$viewOptions, $name, $value, $append);
    }

    /*
     * Determine how to return the data
     * @param mixed $result Data to be displayed
     */
    public static function render($result = null, $params = null, $partial = null)
    {
        $partial = is_null($partial) ? \Yii::$app->request->isAjax : $partial;
        $contentType = 'text/html';
        switch (1) {
            case \Yii::$app->request->isAjax:
            case static::$forceAjax === true:
            $render = 'renderAjax';
            break;

            case $partial === true:
            $render = 'renderPartial';
            break;

            default:
            $render = 'render';
            break;
        }
        if (is_callable($params)) {
            $params = [
               'content' => call_user_func($params),
            ];
        }
        $params = is_null($params) ? static::$viewOptions : $params;
        if (isset($params['js'])) {
            $params['js'] = $params['js'] instanceof JsExpression ? $params['js'] : (new JsExpression(is_array($params['js']) ? implode(PHP_EOL, $params['js']) : $params['js']));
        }
        $format = (!\Yii::$app->request->isAjax && (static::getFormat() == 'modal')) ? 'html' : static::getFormat();
        $params['view'] = ArrayHelper::getValue((array) $params, 'view', static::$viewPath);

        switch ($format) {
            case 'xml':
            //implement handling of XML responses
            $contentType = 'application/xml';
            $ret_val = $result;
            break;

            case 'html':
            $ret_val = static::renderInternal($render, $params, function ($render, $params) {
                return $ret_val = static::$controller->$render($params['view'], ArrayHelper::getValue($params, 'args', []), static::$controller);
            });
            break;

            //THis is used when rendering pre-rendered HTML. Such as a widget
            case 'prepared':
            $ret_val = static::renderInternal($render, $params, function ($render, $params) {
                return static::$controller->$render(static::$viewPath, [
                       'content' => static::$controller->$render($params['view'], $params['args'], static::$controller),
                   ],
                   static::$controller
               );
            });
            break;

            //THis is used when rendering pre-rendered HTML. Such as a widget
            case 'widget':
            $ret_val = static::renderInternal($render, $params, function ($render, $params) {
                $params['args']['content'] = $params['args']['widgetClass']::widget($params['args']['options']);

                return static::$controller->renderPartial(static::$viewPath, [
                       'content' => static::$controller->$render($params['view'], $params['args'], static::$controller),
                   ],
                   static::$controller
               );
            });
            break;

            case 'modal':
            $content = static::renderInternal($render, $params, function ($render, $params) {
                return static::$controller->$render($params['view'], $params['args'], static::$controller);
            });
            $ret_val = static::$controller->$render(static::$viewModal, [
                    'content' => $content,
                    'footer' => @$params['footer'],
                    'title' => \Yii::$app->request->isAjax ? @$params['title'] : '',
                    'modalOptions' => @$params['modalOptions'],
                ],
                static::$controller
            );
            break;

            case 'json':
            $contentType = 'application/json';
            $ret_val = $result;
            break;

            default:
            $contentType = 'text/plain';
            $ret_val = @strip_tags($result['data']);
            break;
        }
        \Yii::$app->response->getHeaders()->set('Content-Type', $contentType);
        if (static::$encodedAs) {
            self::setFormat(static::$encodedAs);
            self::$encodedAs = null;
            static::$viewOptions = [];
            $ret_val = self::render($ret_val);
        }

        return $ret_val;
    }

    protected static function renderInternal($render, $params, $callable)
    {
        $params['args']['options'] = ArrayHelper::getValue(static::$viewOptions, 'options', []);
        if (isset($params['js'])) {
            static::$view->registerJs($params['js']);
        }
        $content = ArrayHelper::getValue($params, 'content', null);
        if (!$content) {
            return $callable($render, $params);
        } else {
            return $content;

            return static::$controller->$render(static::$viewPath, [
               'content' => $content,
            ], static::$controller);
        }
    }

    public static function getFormat()
    {
        switch (empty(static::$format)) {
            case true:
            static::setFormat();
            break;
        }

        return static::$format;
    }

    /*
     * Get the desired display format supported
     * @param string $format Supports encoding format into a different format using colon separation:
     * i.e.: html:json will encode the HTML string as JSON
     * @return string format
     */
    public static function setFormat($format = null)
    {
      // We ignore this for the console response
      if(\Yii::$app->response instanceof \yii\console\Response) {
        return;
      }
        $ret_val = $formatWasEmpty = null;
        if (is_null($format)) {
            $formatWasEmpty = true;
            $format = ArrayHelper::getValue($_REQUEST, '_format', null);
        }
        $parts = explode(':', $format);
        $format = $parts[0];
        static::$encodedAs = isset($parts[1]) ? $parts[1] : null;
        switch ($format) {
            case 'text':
            case 'raw':
            $ret_val = 'raw';
            \Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
            break;

            case 'modal':
            case 'widget':
            $ret_val = $format;
            \Yii::$app->response->format = \yii\web\Response::FORMAT_HTML;
            break;

            case 'xml':
            $ret_val = $format;
            \Yii::$app->response->format = \yii\web\Response::FORMAT_XML;
            break;

            case 'jsonp':
            $ret_val = $format;
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSONP;
            break;

            case 'json':
            $ret_val = $format;
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            break;

            default:
            $ret_val = 'html';
            \Yii::$app->response->format = \yii\web\Response::FORMAT_HTML;
            break;
        }
        static::$format = $ret_val;
        static::$_isFormatSpecifiedExplicitly = $formatWasEmpty ? false : true;

        return $ret_val;
    }

    /**
     * @deprecated in favor of isFormatSpecified
     */
    public static function formatSpecified()
    {
        return static::isFormatSpecified();
    }

    /**
     * Has the user reuqested a specific format?
     *
     * @return bool
     */
    public static function isFormatSpecified()
    {
        return ArrayHelper::getValue($_REQUEST, '_format', null) != null;
    }

    /**
     * Has the user reuqested a specific format?
     *
     * @return bool
     */
    public static function isFormatSpecifiedExplicitly()
    {
        return static::$_isFormatSpecifiedExplicitly === true;
    }

    /**
     * Get the layout file.
     *
     * @param string $layout
     *
     * @return string
     */
    protected static function getLayoutPath($layout = 'column1')
    {
        switch (isset(static::$layouts[$layout])) {
            case false:
            $layout = 'column1';
            break;
        }

        return static::$layouts[$layout];
    }
}
