<?php

namespace nitm\helpers\Template;

class Template
{
    /**
     * Simple template render method to replace keys with values.
     *
     * @method renderTemplate
     *
     * @param [type] $string [description]
     * @param [type] $parts  [description]
     *
     * @return [type] [description]
     */
    public static function render($string, $parts)
    {
        $keys = array_map(function ($key) {
            return "{{{$key}}}";
        }, array_keys($parts));

        return str_replace($keys, array_values($parts), $string);
    }
}
