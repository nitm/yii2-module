<?php

namespace nitm\helpers;

use yii\helpers\Inflector;

/**
 * Provides helpers for rendering widgets and elements. Can support various CSS Libraries
 */

class WidgetHelper
{
    /**
     * Render a widget
     * @param string $class The widgets classname or the Elements method
     * @param string|array $options
     * @param array $realOptions
     * @return string THe widget
     */
    public static function widget($class, $options=null, array $realOptions=[]) {
        $module = static::getModule();
        $theme = $module->UITheme;
        if(!$module) {
            if(class_exists($class)) {
                return $class::widget($options);
            } else if(method_exists(Html::class, $class)) {
                return call_user_func_array([Html::class, $class], [$options, $options]);
            }
        }
        if($module->UITheme && static::getThemeHelper()) {
            $parts = explode('\\', $class);
            $method = 'get'.array_pop($parts).'Options';
            $libraryHelper = static::getLibrary($theme);
            if(method_exists(static::class, $method)) {
                list($method, $content, $resolvedOptions) = call_user_func([static::class, $method], $options);
                Html::convertCssClasses($resolvedOptions, $theme);
                if(method_exists($libraryHelper, $method)) {
                    return $libraryHelper::$method($content, $resolvedOptions);
                } else if(class_exists($uiClasss = static::getWidgetClass($class))) {
                    return $uiClasss::widget($resolvedOptions);
                } else {
                    return $class::widget($options);
                }
            } elseif(method_exists($libraryHelper, $class)) {
                Html::convertCssClasses($realOptions, $theme);
                return $libraryHelper::$class($options, $realOptions);
            } else if(class_exists($class)) {
                return $class::widget($options);
            } elseif(method_exists(Html::class, $class)) {
                Html::convertCssClasses($realOptions, $theme);
                return call_user_func_array([Html::class, $class], [$options, $realOptions]);
            }
        } elseif(method_exists(\yii\helpers\Html::class, $class)) {
            $args = func_get_args();
            array_shift($args);
            if(ArrayHelper::isIndexed($args)) {
                if(is_array($args[0])) {
                    $args = array_values($args[0]);
                }
            }
            return call_user_func_array([\yii\helpers\Html::class, $class], $args);
        } else {
            return $class::widget($options);
        }
    }

    /**
     * Get the UI Library being used
     *
     * @param [type] $library
     * @return void
     */
    protected static function getLibrary($library) {
        $libraries = [
            'semantic' => '\Zelenin\yii\SemanticUI\Elements'
        ];
        return ArrayHelper::getValue($libraries, $library);
    }
    
    public static function getButtonDropdownOptions($options=null) {
        $helper = static::getThemeHelper();
        if($helper) {
            return $helper::getButtonDropdownOptions($options);
        }
        return $options;
    }

    public static function getButtonsGroupOptions($options=null) {
        $helper = static::getThemeHelper();
        if($helper) {
            return $helper::getButtonGroupOptions($options);
        }
        return $options;
    }
    
    /**
     * Get the UI mapping for the specified widgets and theme
     *
     * @param array $widgets
     * @param [type] $theme
     * @return void
     */
    public static function getUIClassMap($widgets=[], $theme=null) {
        $helper = static::getThemeHelper();
        if($helper) {
            return $helper::getUIClassMap($widgets);
        }
        return $widgets;
    }

    public static function getWidgetClass($key=null) {
        $helper = static::getThemeHelper();
        if($helper) {
            return ArrayHelper::getValue($helper::getUIClassMap(), $key, null);
        }
        return null;
    }

    protected static function getModule() {
        return \Yii::$app->getModule('nitm');
    }

    /**
     * Get the widget theme helper class for mapping through differnet libraies
     *
     * @return void
     */
    protected static function getThemeHelper() {
        if(static::getModule()->UITheme == 'semantic'){
            return themes\SemanticHelper::class;
        } else if(static::getModule()->UITheme == 'foundation'){
            return themes\FoundationHelper::class;
        }
        return null;
    }
}
?>
