<?php

namespace nitm\helpers;

use Yii;
use nitm\helpers\Model as ModelHelper;
use nitm\helpers\Html;

/**
 * Form trait which supports the retrieval of form variables.
 */
class Grid
{
    public static function getDefaultActions($extraActions = [], $options = [])
    {
        $except = ArrayHelper::remove($extraActions, 'except', null);
        $iconSize = @$iconSize ?: '1x';
        extract($options);
        $controller = isset($controller) ? $controller : \Yii::$app->controller->id;
        $absolute = isset($absolute) ? true : false;
        $actions = array_replace_recursive([
               'class' => 'nitm\grid\ActionColumn',
               'buttons' => [
                   'form/update' => function ($url, $model) use ($iconSize) {
                       return Html::a(Icon::forAction('update').' Update', $url, [
                           'title' => Yii::t('yii', 'Edit '.ModelHelper::getTitle($model)),
                           'role' => 'disabledOnClose',
                           'data-pjax' => 1,
                           'class' => implode(' ', [
                               'icon-'.$iconSize, ($model->getAttribute('deleted_at') ? 'hidden' : ''),
                           ]),
                       ]);
                   },
                   'view' => function ($url, $model) use ($iconSize) {
                       return Html::a(Icon::forAction('view').' View', $url, [
                           'title' => Yii::t('yii', 'View '.$model->isWhat().' '.ModelHelper::getTitle($model)),
                           'role' => 'disabledOnClose',
                           'data-pjax' => 1,
                           'class' => implode(' ', [
                               'icon-'.$iconSize, ($model->getAttribute('deleted_at') ? 'hidden' : ''),
                           ]),
                       ]);
                   },
                   'permalink' => function ($url, $model) use ($iconSize) {
                       return Html::a(Icon::show('link').' Permalink',  \nitm\cms\helpers\App::getPublicLink($model), [
                           'target' => '_blank',
                           'title' => Yii::t('yii', 'View '.$model->isWhat().': '.ModelHelper::getTitle($model).' on frontend'),
                           'data-pjax' => 1,
                           'class' => 'icon-'.$iconSize,
                       ]);
                   },
                   'delete' => function ($url, $model) use ($iconSize) {
                       return Html::a(Icon::forAction('delete').' Delete', $url, [
                           'title' => Yii::t('yii', 'Delete '.$model->isWhat().' '.ModelHelper::getTitle($model)),
                           'role' => 'deleteAction metaAction disabledOnClose',
                           'data-pjax' => 0,
                           'data-method' => 'post',
                           'data-parent' => '#'.$model->isWhat().$model->getId(),
                           'class' => implode(' ', [
                               'icon-'.$iconSize, ($model->getAttribute('deleted_at') ? 'hidden' : ''),
                               'data-parent' => 'tr',
                           ]),
                       ]);
                   },
               ],
               'urlCreator' => function ($action, $model) use ($controller, $absolute) {
                   return ModelHelper::resolveUrl($model, [
                       'mask' => '/form',
                       'controller' => $controller,
                       'action' => $action,
                       'absolute' => $absolute,
                   ]);
               },
           ], $extraActions);
        if ($except) {
            $actions['buttons'] = array_intersect_key($actions['buttons'], array_flip($except));
        }
        $actions['template'] = implode(' ', array_map(function ($action) {
            return '{'.$action.'}';
        }, array_keys($actions['buttons'])));

        return $actions;
    }
}
