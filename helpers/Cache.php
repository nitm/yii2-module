<?php

namespace nitm\helpers;

use yii\base\Model;
use yii\data\BaseDataProvider;
use yii\data\ArrayDataProvider;
use yii\data\ActiveDataProvider;
use yii\db\ActiveRecordInterface;

/*
 * Setup model based caching, as PHP doesn't support serialization of Closures
 */
class Cache extends Model
{
    public static $cache;
    private static $_cache = [];

    public static function getKey($model, $idKey = null, $relation = null, $many = false)
    {
        $ret_val = [($many == true ? 'many' : 'one'), $relation];
        if ($model instanceof \nitm\db\ActiveRecord) {
            $ret_val[] = $model::isWhat();
        } elseif (is_string($model) || is_numeric($model)) {
            $ret_val[] = $model;
        }

        if (!is_null($idKey) && (is_array($model) && !empty($id = Helper::concatAttributes($model, $idKey)))) {
            $ret_val[] = $id;
        } else {
            $ret_val[] = md5(is_array($idKey) || is_object($idKey) ? json_encode(ArrayHelper::toArray($idKey)) : $idKey);
        }

        return implode('-', $ret_val);
    }

    /**
     * Cache function that returns caching object.
     */
    public static function cache()
    {
        if (!isset(static::$cache)) {
            static::$cache = \Yii::$app->hasProperty('cache') ? \Yii::$app->cache : new \yii\caching\FileCache();
        }

        return static::$cache;
    }

    public static function exists($key)
    {
        return static::cache()->exists($key);
    }

    public static function get($key)
    {
        return static::cache()->get($key);
    }

    public static function set($key, $value, $duration = 300)
    {
        return static::cache()->set($key, $value, $duration > 1 ? $duration : 300);
    }

    public static function delete($key)
    {
        return static::cache()->delete($key);
    }

    /**
     * Wrap setting the model to include th className.
     *
     * @param string $key
     * @param object $model
     *
     * @return bool
     */
    public static function setModel($key, $model, $asArray = false, $duration = 500, $modelClass = null)
    {
        if (is_object($model)) {
            $parsedModel = static::parseBeforeSet($model);

            if (is_object($model) && $model instanceof \yii\base\Model) {
                $className = $model->className();
            } elseif ($model instanceof ActiveRecordInterface) {
                $className = get_class($model);
            } elseif ($model instanceof ActiveDataProvider) {
                $className = $model->query->modelClass;
                $asArray = true;
            } elseif ($model instanceof ArrayDataProvider) {
                $className = 'stdClass';
            } elseif (is_array($model) && is_object(current($model))) {
                $className = get_class(current($model));
            }

            if ($asArray && is_object($model) && !ArrayHelper::isIndexed($parsedModel)) {
                $parsedModel = [$parsedModel];
            } elseif (!$asArray && ArrayHelper::isIndexed($parsedModel) && !$model instanceof BaseDataProvider) {
                $parsedModel = current($model);
            }

            $model = [
                '_class' => $className,
                '_data' => $parsedModel,
                '_asArray' => $asArray,
            ];
        } else {
            if ($asArray) {
                $className = is_object(current($model)) ? get_class(current($model)) : $modelClass;
            } elseif ($className = ArrayHelper::getValue($model, '_class') === null) {
                $className = $modelClass;
            }
            $model = [
                 '_class' => $className,
                 '_data' => static::parseBeforeSet($model, $asArray),
             ];
        }

        return static::set($key, json_encode($model), $duration);
    }

    /**
     * Get a cached array.
     *
     * @param string $key
     * @param string $property
     *
     * @return array
     */
    public static function getModel($sender, $key, $asArray = false, $modelClass = null, $property = null, $options = [])
    {
        //PHP Doesn't support serializing of Closure functions
        $ret_val = [];
        switch (static::exists($key)) {
            case true:
            $array = static::get($key);
            $array = is_string($array) ? json_decode(static::get($key), true) : $array;
            if (is_array($array)) {
                $class = ArrayHelper::getValue($array, '_class');
                $asArray = ArrayHelper::getValue($array, '_asArray', $asArray);
                if ($class && class_exists($class)) {
                    $model = \Yii::createObject($array['_class']);
                    if (is_array($array['_data']) && count(array_filter($array['_data'])) >= 1) {
                        $ret_val = static::parseAfterGet($array['_data'], $model, null, $asArray);
                    } else {
                        $ret_val = $model;
                    }
                } else {
                    return ArrayHelper::getValue($array, '_data', $array);
                }
            } else {
                $ret_val = $array;
            }
            break;

            default:
            if ($sender instanceof ActiveRecordInterface) {
                if (array_key_exists($property, $sender->getRelatedRecords())) {
                    $ret_val = \nitm\helpers\Relations::getRelatedRecord($property, $sender, $modelClass, @$options['construct']);
                } elseif ($sender->hasAttribute($property)) {
                    $ret_val = $sender->$property;
                }
            } elseif ($sender->hasProperty($property)) {
                $ret_val = $sender->$property;
            } else {
                switch (1) {
                    case isset($options['find']):
                    $find = $modelClass::find();

                    foreach ($options['find'] as $option => $params) {
                        $find->$option($params);
                    }

                    unset($options['find']);
                    $ret_val = $find->one();
                    $ret_val = !$ret_val ? new $modelClass(@$options['construct']) : $ret_val;
                    break;

                    case isset($options['construct']):
                    $ret_val = new $modelClass($options['construct']);
                    break;

                    default:
                    $ret_val = new $modelClass($options);
                    break;
                }
            }
            static::set($key, [
                '_class' => $modelClass,
                '_data' => ArrayHelper::toArray($ret_val),
            ], 300);
            break;
        }
        if (is_array($ret_val) && (count($ret_val)) == 1 && !$asArray) {
            $ret_val = current($ret_val);
        } elseif ($asArray) {
            $ret_val = (is_array($ret_val) && (is_array(current($ret_val)) || is_object(current($ret_val)))) ? $ret_val : [$ret_val];
        }

        return $ret_val;
    }

    protected static function parseBeforeSet($model, $asArray = null)
    {
        $ret_val = $model;
        if ($asArray && is_array($model) || ArrayHelper::isIndexed($model)) {
            foreach ($model as $key => $m) {
                $ret_val[$key] = static::parseBeforeSet($m, $asArray);
            }
        } else {
            if (is_object($model) && (get_class($model) === 'Closure')) {
                $model = $model->__invoke();
            } elseif (is_callable($model)) {
                $model = call_user_func($model);
            }

            if (!is_object($model) || (get_class($model) === 'Closure')) {
                return $model;
            }
            if ($asArray) {
                $ret_val = ArrayHelper::toArray($model);
            } elseif ($model instanceof BaseDataProvider) {
                $ret_val = static::parseBeforeSet($model->models, is_null($asArray) ? true : $asArray);
            } else {
                $attributes = $model->getAttributes();
                if (is_array($related = $model->getRelatedRecords())) {
                    $attributes = array_merge($attributes, $related);
                }
                $ret_val = $attributes;
                foreach ($attributes as $attribute => $value) {
                    if (($relation = $model->hasRelation($attribute, $model)) !== null) {
                        $ret_val[$attribute] = [
                            '_relation' => true,
                            '_many' => is_array($value),
                            '_class' => $relation->modelClass,
                            '_data' => static::parseBeforeSet($value),
                        ];
                    } else {
                        $ret_val[$attribute] = is_object($value) ? [
                        '_class' => get_class($value),
                        '_data' => ArrayHelper::toArray($value),
                    ] : $value;
                    }
                }
            }
        }

        return $ret_val;
    }

    protected static function parseAfterGet($array, &$model, $modelClass = null, $asArray = null)
    {
        if (ArrayHelper::isIndexed($array) || $asArray) {
            $className = $model instanceof ActiveRecordInterface ? $model->className() : is_object($model) ? get_class($model) : $model;
            foreach ($array as $index => $attributes) {
                $object = \Yii::createObject($className);

                $ret_val[$index] = static::parseAfterGet($attributes, $object);
            }

            return $ret_val;
        } elseif ($model instanceof BaseDataProvider) {
            $ret_val = static::parseAfterGet($array);
        } else {
            foreach ((array) $array as $attribute => $value) {
                if (is_array($value) && ArrayHelper::getValue($value, '_relation') === true) {
                    //We already determined that this was a relation. Now is it an array of relations?
                    if (ArrayHelper::getValue($value, '_many') === true) {
                        $records = [];
                        foreach ($value['_data'] as $idx => $relatedRecord) {
                            $object = \Yii::createObject($value['_class']);
                            $records[$idx] = static::parseAfterGet($relatedRecord, $object);
                        }
                        $model->populateRelation($attribute, $records);
                    } else {
                        //If not it's a single related object. Create the object and the poplate any related information
                        $object = \Yii::createObject($value['_class']);
                        $model->populateRelation($attribute, static::parseAfterGet($value['_data'], $object));
                    }
                //Some caches support whole objects for the model. In that case simply set the model to the value and return it.
                } elseif (is_object($value)) {
                    $model = $value;
                } else {
                    //We're populating properties for a regular object | model | attribute
                    $modelClass = ArrayHelper::getValue($value, '_class');
                    if (is_array($value) && class_exists($modelClass)) {
                        try {
                            $value = new $modelClass($value['_data']);
                            $model->$attribute = $value;
                        } catch (\Exception $e) {
                            throw $e;
                            $modelAttribute = new $modelClass;
                            foreach ($value['_data'] as $key=>$value) {
                                $modelAttribute->$key = $value;
                            }
                            $model->$attribute = $modelAttribute;
                            \Yii::warning($e);
                        }
                    } elseif (property_exists($model, $attribute)) {
                        try {
                            $model->$attribute = $value;
                        } catch (\Exception $e) {
                            \Yii::warning($e);
                        }
                    } elseif ($model instanceof ActiveRecordInterface) {
                        if ($model->hasAttribute($attribute)) {
                            $model->$attribute = $value;
                        }
                    } else {
                        $model->$attribute = $value;
                    }
                }
                try {
                    $model->setOldAttributes($model->getAttributes());
                } catch (\Exception $e) {
                }
            }

            return $model;
        }
    }

    /**
     * Same as get but uses a formatted key.
     *
     * @param [type] $key [description]
     *
     * @return [type] [description]
     */
    public static function fetch($key)
    {
        $key = call_user_func_array([self::class, 'getKey'], (array) $key);

        return static::get($key);
    }

    /**
     * Same as delete but uses a formatted key.
     *
     * @param [type] $key [description]
     *
     * @return [type] [description]
     */
    public static function forget($key)
    {
        $key = call_user_func_array([self::class, 'getKey'], (array) $key);

        return static::delete($key);
    }

    /**
     * Stores an item only if it doesn't exist in the cache.
     *
     * @param [type] $key      [description]
     * @param [type] $callable [description]
     * @param int    $duration [description]
     *
     * @return [type] [description]
     */
    public static function remember($key, $callable, $asArray = false, $duration = 300)
    {
        $key = call_user_func_array([self::class, 'getKey'], (array) $key);
        if (static::cache()->exists($key)) {
            return static::getModel(null, $key, $asArray);
        } else {
            $result = call_user_func($callable);
            static::setModel($key, $result, $asArray, $duration);

            return $result;
        }
    }
}
