<?php

namespace nitm\helpers;

use yii\db\Expression;
use yii\db\Query;
use yii\db\ActiveRecord;

class QueryFilter
{
    /**
     * Get the query that orders items by their activity
     */
    public static function getHasNewQuery($model, $filterCallback=null)
    {
        //Check parent_id columns in issues, vote...etc tables
        $types = \Yii::$app->getModule('nitm-widgets')->checkActivityFor;
        if (!count($types)) {
            return "";
        }

        $currentUser = \Yii::$app->getUser()->getIdentity();

        if (is_null($filterCallback)) {
            $filterCallback = function ($model) {
                return [
                    'parent_id='.$model->tableName().'.id',
                    ['parent_type' => $model->isWhat()],
                ];
            };
        } elseif (!is_callable($filterCallback)) {
            throw new \Exception("The second argument to ".__FUNCTION__." must be a callback function ");
        }

        foreach ((array)$types as $type=>$where) {
            if (is_int($type)) {
                $type = $where;
                $where = $filterCallback($model);
            }
            $query = $type::find();
            $query->from([
                $type::tableName(),
                $model->tableName()
            ]);
            $query->select(["SUM(IF((
					(
						'".$currentUser->lastActive()."'<=".$model->tableName().".created_at
						OR
						'".$currentUser->lastActive()."'<=".$model->tableName().".updated_at
					)
					AND
					(
						".$type::tableName().".updated_at>=".$model->tableName().".created_at
						OR
						".$type::tableName().".updated_at>=".$model->tableName().".updated_at
						OR
						".$type::tableName().".created_at>=".$model->tableName().".created_at
						OR
						".$type::tableName().".created_at>=".$model->tableName().".updated_at
					)
				),
				1, 0)
			) AS hasNew"]);
            foreach ($where as $filter) {
                $query->andWhere($filter);
            }
            $select[] = $query->createCommand()->getRawSql();
            unset($query);
        }
        return new Expression("(SELECT SUM(hasNew) FROM (".implode(' UNION ALL ', $select).") hasNewData) AS hasNewActivity");
    }

    /**
     * Get the query that orders items by their activity
     */
    public static function getOrderByQuery($model)
    {
        return [
            "COALESCE(".$model->tableName().".updated_at, ".$model->tableName().".created_at)" => SORT_DESC,
        ];
    }

    public static function getVisibilityFilter()
    {
        $currentUser = \Yii::$app->getUser()->getIdentity();

        $where = [
            'or',
            'author_id='.$currentUser->getId()
        ];
        $slugs = ['visibility-public'];

        if ((boolean)\Yii::$app->user->identity->isAdmin()) {
            array_push($slugs, 'visibility-admin');
        }

        foreach ($slugs as $slug) {
            array_push($where,
                'level_id=('.\nitm\models\Category::find()
                    ->select('id')
                    ->where(['slug' => $slug])
                    ->limit(1)
                    ->createCommand()->getRawSql().')'
            );
        }
        return $where;
    }

    public static function isExpression($string)
    {
        return is_string($string) && (strpos($string, '(') !== false || strpos($string, ')') !== false);
    }

    public static function joinFields($parts, $glue='.')
    {
        return (string)implode($glue, array_filter($parts, 'strlen'));
    }

    /**
     * Alias the select fields for a query
     * @param Query|array $query THe query or conditions being modified
     * @param Object|string $model Either a model or the table name
     */
    public static function aliasSelectFields(&$query, $model)
    {
        if (is_object($query)) {
            $query->select = !$query->select ? '*' : $query->select;
            if (!is_array($query->select)) {
                $class = $query->modelClass;
                $query->select = array_keys($class::getTableSchema()->columns);
            }
            $select =& $query->select;
        } else {
            $select =& $query;
        }

        $tableName = static::getAlias($query);

        foreach ((array)$select as $idx=>$field) {
            if (is_string($idx)) {
                $field = $idx;
            }

            if ($field instanceof Query || $field instanceof Expression) {
                continue;
            }
            if ((strpos($field, '(') || strpos($field, ')')) !== false) {
                continue;
            }

            if (is_string($field) && strpos($field, '.') !== false) {
                continue;
            }
            if (is_string($field) && $model instanceof ActiveRecord) {
                $select[$idx] = $tableName.'.'.$field;
            } elseif (is_string($field) && is_string($model)) {
                $select[$idx] = $model.'.'.$field;
            }
        }
        $query->select = $select;
    }

    public static function getAlias($query, $model=null, $alias=null)
    {
        if ($query instanceof \yii\db\Query && $query->from) {
            return ArrayHelper::isIndexed($query->from) ? ($alias ?: $query->from[0]) : key($query->from);
        } elseif (!is_null($alias)) {
            return $alias;
        } elseif (is_object($model)) {
            return is_string($model) ? $model : $model->tableName();
        }
    }

    /*
     * Set the aliased fields according to the class columns() function
     * @param ActiveQuery $query
     * @return ActiveQuery $query
     */
    public static function aliasColumns($query)
    {
        $class = $query->modelClass;
        $ret_val = [];
        if (method_exists($class, 'has')) {
            $has = is_array($class::has()) ? $class::has() : null;
        } else {
            $has = [];
        }
        switch (is_null($has)) {
            case false:
            foreach ($has as $property=>$value) {
                $special = explode(':', $property);
                switch (sizeof($special)) {
                    case 2:
                    $property = $special[1];
                    $column = $special[0];
                    break;

                    case 1:
                    $property = $special[0];
                    $column = $property;
                    break;

                    default:
                    $column = $property;
                    break;
                }
            }
            break;
        }
        if (count($ret_val)) {
            $query->select(array_merge($ret_val, array_keys($class::getTableSchema()->columns)));
        } else {
            $query->select('*');
        }

        static::aliasFields($query, $class::tableName());
        return $query;
    }

    /**
     * Alias the where fields for a query
     * @param Query|array $query THe query or conditions being modified
     * @param Object|string $model Either a model or the table name
     */
    public static function aliasWhereFields(&$query, $model, $alias = null)
    {
        if ($query instanceof Query) {
            $where =& $query->where;
        } elseif (is_array($query)) {
            $where =& $query;
        }

        if (!isset($where) || !is_array($where)) {
            return;
        }

        //Model will be the tablename if a string is passed for $model
        if (is_null($alias)) {
            if (is_string($model)) {
                $alias = $model;
            } else {
                $alias = static::getAlias($query, $model, $alias);
            }
        }
        foreach ($where as $field=>$value) {
            if (is_string($field) && strpos('.', $field) === false) {
                //If an object was passed get the table name
                if (is_object($model) && $model->hasAttribute($field)) {
                    $where[$alias.'.'.$field] = $value;
                }
                //Otherwise only a table name could have been passed
                elseif (is_string($model)) {
                    $where[$model.'.'.$field] = $value;
                }
                unset($where[$field]);
            } elseif (is_array($value)) {
                static::aliasWhereFields($value, $model);
            }
        }
    }

    /**
     * Alias the orderBy fields for a query
     * @param Query|array $query THe query or conditions being modified
     * @param Object|string $model Either a model or the table name
     */
    public static function aliasOrderByFields(&$query, $model)
    {
        $joined = $ret_val = [];
        if ($query instanceof Query) {
            $orderBy =& $query->orderBy;
        } elseif (is_array($query)) {
            $orderBy =& $query;
        }

        if (!isset($orderBy) || !is_array($orderBy)) {
            return;
        }

        if ($query instanceof Query) {
            $with =& $query->with;
        }

        $with = (array)$with;

        $db =  $query->createCommand()->db;
        $newOrderBy = $groupBy = [];
        foreach ($orderBy as $field=>$order) {
            if ($order instanceof Query || $order instanceof Expression) {
                $newOrderBy[$field] = $order;
                continue;
            }

            $originalField = $field;

            $table = '';

            /**
             * Try to see if this is a serialized string
             */
            if (is_string($field) && (($unserialized = @unserialize($field)) !== false)) {
                $field = $unserialized;
            }

            if (is_object($field) && !($field instanceof Expression)) {
                throw new \yii\base\InvalidArgumentException("The only object supported is a \yii\db\Expression object");
            }

            $fieldParts = explode('.', $field);
            $table = !static::isExpression($field) ? array_shift($fieldParts) : $table;

            if ($field instanceof Expression) {
                //The field/relation is most likely the first part before the first period. We should remove it if this is an expression
                $field = explode('.', $field);
                $table = array_shift($field);
                if (static::isExpression($table)) {
                    array_unshift($field, $table);
                    $table = '';
                }
                $field = new Expression(static::joinFields((array)$field));
            } elseif (static::isExpression($field)) {
                //This is an expression that hasn't been wrapped yet. Wrap it in yii db Expresion
                $field = new Expression(static::joinFields([$table, $field]));
            } elseif ((strpos($field, '.') !== false) && !static::isExpression($field)) {
                $parts = explode('.', $field);
                $field = array_pop($parts);
            } else {
                $table = is_string($model) ? $model : static::getAlias($query);
            }

            $class = $query->modelClass;

            if ($field instanceof Expression && (static::isExpression($table) || empty($table))) {
                $newOrderBy[(string)$field] = $order;
                continue;
            }

            /**
             * If the field belongs to the current table then alias it and add it to the list of sorted fields and continue
             */
            if ($class::tableName() == $table && !static::isExpression($table)) {
                $key = static::joinFields([$table, $field]);
                $newOrderBy[$key] = $order;
                $ret_val[$field] = $order;
                continue;
            }

            //If $table is actually the name of a relation then investigate further
            if (isset($table) && !in_array($table, $joined)) {
                //If a relation was specified as the joining value then we need to join the relation and order by the aliased table
                if (in_array($table, $with)) {
                    $toJoin = $table;
                    if ($relation = $model->hasRelation($toJoin)) {
                        $relationClass = $relation->modelClass;
                        $relationTable = $relationClass::tableName();

                        //Left join to return the values from the subject table only
                        //$on = '0=1';
                        //$query->innerJoin(static::joinFields([$relationTable, $alias], ' '), $on);
                        $relationLink = $relation->link;

                        if ($relationClass::tableName() == $class::tableName()) {
                            $alias = '';
                        } else {
                            $alias = $toJoin;
                        }

                        $query->innerJoinWith([
                            $toJoin => function ($relation) use ($db, $class, $alias, $relationTable) {
                                $relationClass = $relation->modelClass;
                                $relationTable = $relationClass::tableName();
                                $relation->from([$alias => $relationTable]);
                                $relation->select($alias.'.*');
                            }
                        ]);
                        $aliasField = static::joinFields($field instanceof Expression ? [$field] : [$table, $field]);
                        $newOrderBy[$aliasField] = $order;
                        //Ignore the universal 'id' attribute
                        $ret_val[static::joinFields($relationLink, ', ')] = $order;
                        $relationLink = array_merge($relationLink, array_keys((array)$relation->where));
                        self::aliasSelectFields($relationLink, $alias);
                        $groupBy = array_values(array_unique(array_merge($groupBy, $relationLink)));
                        $groupBy =array_merge($groupBy, array_map(function ($key) use ($model) {
                            return $model->tableName().'.'.$key;
                        }, $model->primaryKey()));
                    }
                } else {
                    $newOrderBy[static::joinFields([$table, $field])] = $order;
                    $ret_val[$field.''] = $order;
                }
                $joined[] = $table;
            }
        }
        if ($query instanceof Query) {
            $query->orderBy($newOrderBy);
            if (!empty($groupBy)) {
                $query->groupBy($groupBy);
            }
        } else {
            $query = $newOrderBy;
        }
        //Return the original fields that were sorted by
        return $ret_val;
    }

    public static function aliasFields(&$query, $model)
    {
        self::aliasSelectFields($query, $model);
        self::aliasOrderByFields($query, $model);
        self::aliasWhereFields($query, $model);
    }


    public static function setDataProviderOrders($dataProvider, $orders=[])
    {
        $dataProvider->sort->params = $sort = [];
        if (is_array($orders)) {
            foreach ($orders as $key=>$direction) {
                try {
                    $sortParams = $dataProvider->sort->attributes[$key];
                    foreach (['asc', 'desc'] as $order) {
                        foreach ($sortParams[$order] as $field=>$fieldDirection) {
                            unset($dataProvider->sort->attributes[$key][$order][$field]);
                            $field = ($unserialized = @unserialize($field)) !== false ? $unserialized : $field;
                            /*$newField = explode('.', $field);
                            $newField[0] .= 'OrderBy';
                            $field = static::joinFields($newField);*/
                            $dataProvider->sort->attributes[$key][$order][$field] = $fieldDirection;
                        }
                    }
                } catch (\Exception $e) {
                }

                $parts = explode('.', $key);
                if (count($parts) >= 2) {
                    $relation = $parts[0];
                    //If we're sorting by relation then we should join the relation here'
                    $primaryModelClass = $dataProvider->query->modelClass;
                    $primaryModel = $dataProvider->query->primaryModel ?? new $primaryModelClass;
                    if ($primaryModel->hasRelation($relation)) {
                        $relationQuery = $primaryModel->getRelation($relation);
                        $relationModelClass = $relationQuery->modelClass;
                        //Need to alais the relaton here
                        $dataProvider->query->joinWith([$relation.' '.$relation]);
                    }
                }
                $sort[] = ($direction == SORT_ASC) ? $key : '-'.$key;
            }
            $dataProvider->sort->params[$dataProvider->sort->sortParam] = implode(',', $sort);
            $dataProvider->sort->getOrders(true);
        }
    }



    /*
     * Apply the filters specified by the end user
     * @param ActiveQuery $this
     * @param mixed $filters
     */
    public static function applyFilters($query, $filters=null)
    {
        //search for special filters
        switch (true) {
            case is_a($query->className(), '\yii\mongo\ActiveQuery'):
            case is_a($query->className(), '\yii\elasticsearch\ActiveQuery'):
            $target = null;
            break;

            default:
            $target = $query->from[0];
            break;
        }

        switch (is_array($filters)) {
            case true:
            foreach ($filters as $name=>$value) {
                switch ($query->hasFilter($name)) {
                    case true:
                    switch (strtolower($name)) {
                        case 'select':
                        static::aliasSelectFields($value, $target);
                        break;

                        case 'orderby':
                        static::aliasOrderByFields($value, $target);
                        if (isset($filters['order'])) {
                            unset($filters['order']);
                        }
                        break;

                        case 'groupby':
                        static::aliasOrderByFields($value, $target);
                        break;

                        case 'where':
                        static::aliasWhereFields($value, $target);
                        break;
                    }
                    $filters[$name] = $value;
                    break;
                }
            }

            $filters = static::applyQueryOptions($query, $filters, $target);
            break;
        }
        return $query;
    }

    /*
     * Does this object support this filter?
     * @param string|int #name
     * @return boolean
     */
    public static function hasFilter($name, $filters=[])
    {
        return array_key_exists($name, $filters);
    }

    /**
     * Some common filters
     * @param $model THe model
     * @param $name The name of the filter
     * @param $default Should a default value be appended?
     * @return mixed $filter
     */
    public static function getFilter($model, $name=null, $default=true)
    {
        $ret_val = null;
        switch ($model->hasFilter($name)) {
            case true:
            $filters = $model->filters();
            switch (is_null($filters[$name])) {
                case false:
                switch (is_array($filters[$name])) {
                    case true:
                    switch (sizeof($filters[$name])) {
                        case 3:
                        $class = @$filters[$name][0];
                        $method = @$filters[$name][1];
                        $args = @$filters[$name][2];
                        break;

                        case 2:
                        $class = get_class($this);
                        $method = @$filters[$name][0];
                        $args = @$filters[$name][1];
                        break;
                    }
                }
                $r = new ReflectionClass($class);
                if (!$r->hasMethod($method)) {
                    throw new \base\ErrorException("The method: $method does not exist in class: $class");
                }
                break;

                default:
                $class = null;
                break;
            }
            switch ($name) {
                case 'author':
                case 'editor':
                switch ($class == null) {
                    case true:
                    $class = \Yii::$app->getModule('nitm')->getSearchClass('user');
                    $o = new $class;
                    $o->addWith('profile');
                    $filters = $o->getList(['profile.name', 'username'], ['(', ')', ' ']);
                    break;

                    default:
                    $filters = call_user_func_array(array($class, $method), $args);
                    break;
                }
                $ret_val = $filters;
                break;

                case 'status':
                $ret_val = ['0' => 'Disabled', '1' => 'Enabled'];
                $ret_val = ($default === true) ? array_merge(['' => 'Any'], $ret_val) : $ret_val;
                break;

                case 'boolean':
                $ret_val = ['0' => 'No', '1' => 'Yes'];
                break;

                case 'rating':
                break;

                case 'order':
                $ret_val = ['desc' => 'Descending', 'asc' => 'Ascending'];
                break;

                case 'order_by':
                foreach ($model->getTableSchema()->columns as $colName=>$info) {
                    switch ($info->type) {
                        case 'text':
                        case 'binary':
                        break;

                        default:
                        if ($info->isPrimaryKey) {
                            $primaryKey = [$colName => ClassHelper::properName($colName)];
                            continue;
                        }
                        $ret_val[$colName] = Classhelper::properName($colName);
                        break;
                    }
                }

                foreach ($model->getSort() as $attr=>$options) {
                    @list($relation, $label, $orderAttr) = (array)$options;
                    if ($model->hasMethod('get'.$relation)) {
                        $ret_val[$attr] = $options['label'];
                    }
                }

                ksort($ret_val);

                if (isset($primaryKey)) {
                    $ret_val = array_reverse($ret_val, true);
                    $ret_val[key($primaryKey)] = current($primaryKey);
                    $ret_val = array_reverse($ret_val, true);
                }
                break;

                default:
                switch ($class == null) {
                    case true:
                    $modelClas = $model->className();
                    $filters = isset($modelClass::$settings[$modelClass::isWhat()]['filter'][$name]) ? $modelClass::$settings[$modelClass::isWhat()]['filter'][$name] : [];
                    break;

                    default:
                    $filters = call_user_func_array(array($class, $method), $args);
                    break;
                }
                $ret_val = ($default === true) ? array_merge(['' => 'Select '.ClassHelper::properName($name)], $filters) : $filters;
                break;
            }
            break;
        }
        return $ret_val;
    }

    public static function applyQueryOptions($query, $options, $target=null)
    {
        foreach ($options as $type=>$args) {
            if ($query->hasMethod($type)) {
                $query->$type($args);
                unset($options[$type]);
            }
        }
        if (is_array($options) && (sizeof($options) >= 1)) {
            static::aliasWhereFields($model, $options, $target);
            $query->andWhere($options);
        }
        return $options;
    }
}
