<?php

namespace nitm\helpers;

use yii\helpers\Inflector;

/**
 * Provides helpers for rendering widgets and elements. Can support various CSS Libraries
 */

class WidgetHelper
{
    
    public static function getButtonsGroupOptions($options) {
        return static::getButtonGroupOptions($options);
    }

    public static function getButtonGroupOptions($options) {
        $content = '';
        foreach($options['buttons'] as $name=>$button) {
            $button['options']['class'] = Html::convertCssClass(ArrayHelper::getValue($button, 'options.class', ''), 'ui');
            $button['options']['tag'] = ArrayHelper::getValue($button, 'options.tag', 'button');
            $content .= Html::button(\Yii::t('app', $button['label']), $button['options']);
        }
        return ['buttonsGroup', $content, ArrayHelper::remove($options, 'options', [])];
    }

    public static function getButtonDropdownOptions($options) {
        $items = array_map(function ($item) {
            $item['options'] = ArrayHelper::remove($item, 'linkOptions', []);
            return $item;
        }, $options['dropdown']['items']);
        $resolvedOptions = [
            'pointing' => ArrayHelper::remove($options, 'pointing', true),
            'encodeLabels' => ArrayHelper::getValue($options, 'encodeLabels', false),
            'items' => [
                [
                    'label' => ArrayHelper::getValue($options, 'label', 'Menu'),
                    'items' => $items,
                    'options' => ArrayHelper::getValue($options, 'options', [
                        'class' => 'pointing'
                    ])
                ]
            ],
            'options' => ArrayHelper::getValue($options, 'dropdown.options', [])
        ];
        return ['buttonDropdown', $options['label'], $resolvedOptions];
    }

    public static function getUIClassMap($widgets=[]) {
        return \nitm\helpers\Cache::remember('foundation-ui-container-map', function () use($widgets) {
            $map = [];
            if(class_exists('foundationize\foundation\FoundationAsset')) {
                $widgets = array_merge(static::getWidgets(), $widgets);
                foreach($widgets as $specifiedClass => $widget) {
                    $uiClass = strpos($widget, '\\') !== false ? $widget : '\foundationize\foundation\\'.$widget;
                    if(!class_exists($uiClass)) {
                        foreach(['grid'] as $domain) {
                            $uiClass = '\foundationize\foundation\\'.$domain.'\\'.$widget;
                            if(class_exists($uiClass)) {
                                break;
                            } else {
                                $uiClass = '\foundationize\foundation\\'.$domain.'\\'.Inflector::singularize($widget);
                                if(class_exists($uiClass)) {
                                    break;
                                }
                            }
                        }
                    }
                    if(class_exists($uiClass)) {
                        if(class_exists($specifiedClass)) {
                           $map[$specifiedClass] = $uiClass;
                        }
                        $yiiGridWidget = 'yii\grid\\'.$widget;
                        if(class_exists($yiiGridWidget)) {
                           $map[$yiiGridWidget] = $uiClass;
                        }
                        $yiiWidgetClass = 'yii\widgets\\'.$widget;
                        if(class_exists($yiiWidgetClass)) {
                            $map[$yiiWidgetClass] = $uiClass;
                        }
                        $bootstrapClass = 'yii\bootstrap\\'.$widget;
                        if(class_exists($bootstrapClass)) {
                            $map[$bootstrapClass] = $uiClass;
                        }
                        // $kartikClass = 'kartik\widgets\\'.$widget;
                        // if(class_exists($kartikClass)) {
                        //     $map[$kartikClass] = $uiClass;
                        // }
                    }
                }
            } else {
                \Yii::warning("You requested the foundation UI but the foundation UI package isn't installed: `composer require foundationize/yii2-foundation:dev-master`");
            }
            return $map;
        }, true, 600);
    }

    public static function getWidgets($key=null) {
        return [
            'Accordion',
            'ActionColumn',
            'ctiveForm' => 'foundationize\foundation\FnActiveForm',
            'ActiveField' => 'foundationize\foundation\FnActiveField',
            'Breadcrumbs',
            'Checkbox',
            'CheckboxList',
            'CheckboxColumn',
            'DataColumn',
            'DetailView',
            'Dropdown',
            'Embed',
            'FnAlert',
            'FnActiveForm',
            'FnActiveField',
            'GridView',
            'IconBar',
            'LinkPager',
            'yii\bootstrap\Dropdown' => 'Dropdown',
            'yii\bootstrap\ButtonDropdown' => 'Menu',
            'yii\bootstrap\Nav' => 'Menu',
            'Message',
            'Modal',
            'NavigationWidget',
            'Progress',
            'Pjax',
            'Radio',
            'RadioList',
            'Rating',
            'Search',
            'Select',
            'Shape',
            'foundationize\foundation\SideNav' => 'Sidebar',
            'Sticky',
            'TopBar',
            'TopBarSection',
            // 'yii\bootstrap\Tabs' => 'Tab'
        ];
    }

    protected static function getModule() {
        return \Yii::$app->getModule('nitm');
    }
}
?>
