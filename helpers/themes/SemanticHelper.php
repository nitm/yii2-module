<?php

namespace nitm\helpers\themes;

use yii\helpers\Inflector;

/**
 * Provides helpers for rendering widgets and elements. Can support various CSS Libraries
 */

class SemanticHelper
{
    
    public static function getButtonsGroupOptions($options) {
        return static::getButtonGroupOptions($options);
    }

    public static function getButtonGroupOptions($options) {
        $content = '';
        foreach($options['buttons'] as $name=>$button) {
            $button['options']['class'] = Html::convertCssClass(ArrayHelper::getValue($button, 'options.class', ''), 'semantic');
            $button['options']['tag'] = ArrayHelper::getValue($button, 'options.tag', 'button');
            $content .= Html::button(\Yii::t('app', $button['label']), $button['options']);
        }
        return ['buttonsGroup', $content, ArrayHelper::remove($options, 'options', [])];
    }

    public static function getButtonDropdownOptions($options) {
        $items = array_map(function ($item) {
            $item['options'] = ArrayHelper::remove($item, 'linkOptions', []);
            return $item;
        }, $options['dropdown']['items']);
        $resolvedOptions = [
            'pointing' => ArrayHelper::remove($options, 'pointing', true),
            'encodeLabels' => ArrayHelper::getValue($options, 'encodeLabels', false),
            'items' => [
                [
                    'label' => ArrayHelper::getValue($options, 'label', 'Menu'),
                    'items' => $items,
                    'options' => ArrayHelper::getValue($options, 'options', [
                        'class' => 'pointing'
                    ])
                ]
            ],
            'options' => ArrayHelper::getValue($options, 'dropdown.options', [])
        ];
        return ['buttonDropdown', $options['label'], $resolvedOptions];
    }

    public static function getUIClassMap($widgets=[]) {
        return \nitm\helpers\Cache::remember('semantic-ui-container-map', function () use($widgets) {
            $map = [];
            if(class_exists('Zelenin\yii\SemanticUI\InputWidget')) {
                $widgets = array_merge(static::getWidgets(), $widgets);
                foreach($widgets as $specifiedClass => $widget) {
                    foreach(['collections', 'modules', 'widgets'] as $domain) {
                        $semanticClass = '\Zelenin\yii\SemanticUI\\'.$domain.'\\'.$widget;
                        if(class_exists($semanticClass)) {
                            break;
                        } else {
                            $semanticClass = '\Zelenin\yii\SemanticUI\\'.$domain.'\\'.Inflector::singularize($widget);
                            if(class_exists($semanticClass)) {
                                break;
                            }
                        }
                    }
                    if(class_exists($semanticClass)) {
                        if(class_exists($specifiedClass)) {
                           $map[$specifiedClass] = $semanticClass;
                        }
                        $yiiGridWidget = 'yii\grid\\'.$widget;
                        if(class_exists($yiiGridWidget)) {
                           $map[$yiiGridWidget] = $semanticClass;
                        }
                        $yiiWidgetClass = 'yii\widgets\\'.$widget;
                        if(class_exists($yiiWidgetClass)) {
                            $map[$yiiWidgetClass] = $semanticClass;
                        }
                        $bootstrapClass = 'yii\bootstrap\\'.$widget;
                        if(class_exists($bootstrapClass)) {
                            $map[$bootstrapClass] = $semanticClass;
                        }
                        // $kartikClass = 'kartik\widgets\\'.$widget;
                        // if(class_exists($kartikClass)) {
                        //     $map[$kartikClass] = $semanticClass;
                        // }
                    }
                }
            } else {
                \Yii::warning("You requested the semantic UI but the semantic UI package isn't installed: `php composer.phar require zelenin/yii2-semantic-ui \"~2\"`");
            }
            return $map;
        }, true, 600);
    }

    public static function getWidgetClass($key=null) {
        if(static::getModule()->UITheme == 'semantic'){
            return ArrayHelper::getValue(static::getUIClassMap(), $key, null);
        } else if(static::getModule()->UITheme == 'foundation'){
            return ArrayHelper::getValue(static::getFoundationUIClassMap(), $key, null);
        }
    }
    
    public static function getWidgets($key=null) {
        return [
            'Accordion',
            'ActionColumn',
            'ActiveForm',
            'ActiveField',
            'Breadcrumbs',
            'Checkbox',
            'CheckboxList',
            'CheckboxColumn',
            'DataColumn',
            'DetailView',
            'Dropdown',
            'Embed',
            'GridView',
            'LinkPager',
            'yii\bootstrap\Dropdown' => 'Dropdown',
            'yii\bootstrap\ButtonDropdown' => 'Menu',
            // 'yii\bootstrap\Nav' => 'Menu',
            'Message',
            'Modal',
            'Progress',
            'Pjax',
            'Radio',
            'RadioList',
            'Rating',
            'Search',
            'Select',
            'Shape',
            'Sidebar',
            'Sticky',
            // 'yii\bootstrap\Tabs' => 'Tab'
        ];
    }

    protected static function getModule() {
        return \Yii::$app->getModule('nitm');
    }
}
?>
