<?php

namespace nitm\helpers;

class Html extends \yii\helpers\Html
{
    /**
     * Get certain types of icons
     * @param string $action
     * @param string $attribute
     * @param Object $model
     * @param mixed $options
     */
    public static function linkify($text)
    {
        return preg_replace("
			#((http|https|ftp)://(\S*?\.\S*?))(\s|\;|\)|\]|\[|\{|\}|,|\"|'|:|\<|$|\.\s)#ie",
            "'<a href=\"$1\" target=\"_blank\">$3</a>$4'",
            $text
        );
    }

    public static function parseLinks($str)
    {
        return preg_replace("[[:alpha:]]+://[^<>[:space:]]+[[:alnum:]/]", "<a href=\"\\0\">\\0</a>", $str);
    }

    public static function getHtmlDataOption($value, $label=null, $key=null, $options=null, $defaultOptions=[])
    {
        if (is_callable($options)) {
            return call_user_func_array($options, [$value, $label, $key, $defaultOptions]);
        } elseif (is_array($options)) {
            return array_replace_recursive([
              'label' => $label,
              'options' => [
                  'class' => $key,
                  'value' => $value
              ]
          ], $defaultOptions, $options);
        } else {
            return array_merge($defaultOptions, [
                'label' => $label,
                'options' => [
                    'class' => $key,
                    'value' => $value,
                ]
            ]);
        }
    }

    public static function label($content, $for = null, $options = [])
    {
        static::convertCssClasses($options);
        return parent::label($content, $for, $options);
    }

    public static function mailto($text, $email = null, $options = [])
    {
        static::convertCssClasses($options);
        return parent::mailto($text, $email, $options);
    }

    public static function img($src, $options = [])
    {
        static::convertCssClasses($options);
        return parent::img($src, $options);
    }

    /**
     * @inheritdoc
     */
    public static function a($text, $url = null, $options = [])
    {
        static::convertCssClasses($options);
        return parent::a($text, $url, $options);
    }

    /**
     * @inheritdoc
     */
    public static function button($content='Button', $options = [])
    {
        $labeled = ArrayHelper::remove($options, 'labeled', true);
        $direction = ArrayHelper::remove($options, 'labeledDirection', 'left');
        if($labeled) {
            static::addCssClass($options, 'labeled icon '.$direction);
            $content = Html::tag('i', '', [
                'class' => 'fa-'.ArrayHelper::remove($options, 'icon', strtolower($content)).' fa'
            ]).\Yii::t('app', $content);
        }
        static::addCssClass($options, 'btn');
        return WidgetHelper::widget('button', $content, array_merge([
            'tag' => 'button',
        ], $options));
    }

    /**
     * @inheritdoc
     */
    public static function tag($name, $content = '', $options = [])
    {
        $module = \Yii::$app->getModule('nitm');
        static::convertCssClasses($options, $module->uiTheme);
        return parent::tag($name, $content, $options);
    }

    /**
     * @inheritdoc
     */
    public static function submitButton($text='Save', $options=[]) {
        return static::button($text, array_merge($options, [
            'icon' => 'save',
            'type' => 'submit',
            'class' => 'btn-primary'
        ]));
    }

    /**
     * @inheritdoc
     */
    public static function resetButton($text='Reset', $options=[]) {
        return static::button($text, array_merge($options, [
            'icon' => 'reset',
            'type' => 'reset',
            'class' => 'btn-default'
        ]));
    }

    /**
     * @inheritdoc
     */
    public static function labeledButton($text, $options=[]) {

        return static::button($text, array_merge($options, [
            'labeled' => true
        ]));
    }


    public static function convertCssClasses(&$options, $using=null) {
        if(!is_array($options)) {
            return static::convertCssClass($options);
        }
        foreach($options as $k=>$v) {
            //Make sure we don't conver actual php classes'
            if($k === 'class' && !class_exists($v)) {
                $options[$k] = static::convertCssClass($v);
            } else if(is_array($v)) {
                static::convertCssClasses($o);
            }
        }
    }

    /**
     * Convert a CSS class. If the converter optiosn doesn't exist then return the original value
     * @param string $class
     * @param string $using What library to map to?
     * @return string
     */
    public static function convertCssClass($class, $using=null) {
        $optionsMethod = 'get'.$using.'CssMap';
        if(method_exists(static::class, $optionsMethod)) {
            $map = static::$optionsMethod();
            return str_replace(array_keys($map), $map, $class);
        } else {
            return $class;
        }
    }

    protected static function getSemanticCssMap() {
        return [
            'fa-' => '',
            'fa ' => 'icon ',
            'hidden-sm' => 'tablet large only',
            'hidden-md' => 'large only',
            'hidden-lg' => 'tablet mobile only',
            'visible-sm' => 'mobile only',
            'visible-md' => 'tablet only',
            'visible-lg' => 'large only',
            'alert-danger' => 'error',
            'alert-info' => 'info',
            'alert-warning' => 'warning',
            'alert' => 'message',
            '-info' => '-teal',
            'success' => 'positive',
            'danger' => 'negative', 
            'col' => ' column ',
            '-sm' => ' small',
            '-md' => ' medium',
            '-lg' => ' large',
            '-1' => ' three',
            '-2' => ' four',
            '-3' => ' five',
            '-4' => ' six',
            '-5' => ' seven',
            '-6' => ' eight',
            '-7' => ' nine',
            '-8' => ' ten',
            '-9' => ' eleven',
            '-10' => ' twelve',
            '-11' => ' thirteen',
            '-12' => ' sixteen',
            'btn-' => '',
            'btn ' => 'button ',
        ];
    }
}
