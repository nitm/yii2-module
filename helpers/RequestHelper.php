<?php

namespace nitm\helpers;

use yii\helpers\Json as JsonHelper;
use yii\web\UploadedFile;

class RequestHelper
{

    /**
     * Resolve the request body parameters
     *
     * @param [type] $key
     * @param [type] $default
     * @return mixed
     */
    public static function resolveData($key = null, $default = null)
    {
        $inputData = $files = [];
        if (empty($_FILES)) {
            $inputData = file_get_contents('php://input');
        } else {
            foreach ($_FILES as $instance=>$file) {
                $files[$instance] = UploadedFile::getInstancesByName($instance);
                // $files[$instance] = [];
            }
        }
        $inputData = Json::isJson($inputData) ? Json::decode($inputData) : [];
        $data = array_merge(\Yii::$app->request->bodyParams, (array)\Yii::$app->request->get(), (array)$inputData, $files);
        if (empty($data)) {
            $string = \Yii::$app->request->rawBody;
            $data = Json::isJson($string) ? Json::decode($string) : null;
        }
        return $key ? ArrayHelper::getValue($data, $key, $default) : (empty($data) ? $default : $data);
    }
}
