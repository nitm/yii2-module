<?php

namespace nitm\helpers;

use Yii;
use nitm\helpers\Html;

/**
 * Form trait which supports the retrieval of form variables.
 */
class Form
{
    public static function getVariables($model, $options = [], $modalOptions = [], $setViewOptions = true, $force = false)
    {
        $ret_val = [
            'success' => false,
            'data' => \nitm\helpers\Html::tag('h3', \Yii::t('app', 'No form found'), ['class' => 'alert alert-danger text-center']),
        ];
        $nitm = \Yii::$app->getModule('nitm');
        if (!empty($options['scenario']) && ($model instanceof \yii\db\ActiveRecord)) {
            $attributes = [];
            if ($model->getScenario() == 'default' || $model->validate()) {
                $model->setScenario($options['scenario']);
                $options['modelOptions'] = (isset($options['modelOptions']) && is_array($options['modelOptions'])) ? $options['modelOptions'] : null;
                //this means we found our object
                switch (ltrim($options['modelClass'], '\\')) {
                    case $model->className():
                     /**
                      * Otherwise we need to make sure this model exists.
                      */
                     $queryOptions = ArrayHelper::getValue($options, 'queryOptions',  ArrayHelper::getValue($options, 'modelOptions.queryOptions', []), []);
                     $found = \Yii::$app->controller->getModel($options['scenario'], $options['id'], $options['modelClass'], null, $queryOptions, false);
                     $model = ($found instanceof $options['modelClass'] && !$found->isNewRecord) ? $found : null;
                    if (!$model) {
                        $model = new $options['modelClass'](ArrayHelper::getValue($options, 'construct', []));
                    }
                    break;

                    default:
                    //Get the data according to get$options['param'] functions
                    $class = $model->className();
                    $model = \Yii::$app->controller->getModel($options['scenario'], $options['id'], $class, null, false);
                    if (!$model) {
                        $model = new $options['modelClass'](ArrayHelper::getValue($options, 'construct', []));
                    } else {
                        switch ($model->hasMethod($options['provider'])) {
                            case true:
                            call_user_func_array([$model, $options['provider']], $args);
                            $model->queryOptions['limit'] = 1;
                            $found = $model->getArrays();
                            $model = empty($found) ? $model : $found[0];
                            break;
                        }
                    }
                    break;
                }
                if (!is_null($model) || $force) {

                    /**
                     * Get scenario and form options.
                     */
                    $scenario = ArrayHelper::getValue($options, 'scenario', $model->isNewRecord ? 'create' : 'update');
                    $model->setScenario($scenario);
                    $action = ArrayHelper::getValue($options, 'action', null);
                    if (is_null($action)) {
                        $action = parse_url(\Yii::$app->request->url,  PHP_URL_PATH);
                        $formStartsAt = strpos($action, '/form');
                        if ($formStartsAt) {
                            $action = '/'.ltrim(substr($action, 0, $formStartsAt ?: strlen($action)), '/');
                        }

                        $action = $action.'/'.$scenario;
                        $action = \yii\helpers\Url::toRoute([$action,
                           'id' => ArrayHelper::getValue($options, 'id', $model->getId()),
                        ]);
                    }
                    $formOptions = array_replace_recursive([
                        'container' => [
                            'id' => $model->isWhat().'-form'.$model->getId().'-container',
                            'class' => implode(' ', [
                                $model->isWhat().'-'.$model->getScenario(),
                                \Yii::$app->request->isAjax ? '' : 'wrapper',
                            ]),
                        ],
                        'action' => $action,
                        'options' => [
                            'id' => $model->isWhat().'-form'.$model->getId(),
                            'role' => $scenario.$model->formName().($nitm ? ($nitm->enableAjaxForms ? ' ajaxForm' : '') : ' ajaxForm'),
                        ],
                    ], \yii\helpers\ArrayHelper::getValue($options, 'formOptions', []));

                    /**
                     * Setup view options.
                     */
                    $options['viewArgs'] = ArrayHelper::getValue($options, 'viewArgs', []);
                    $footer = ArrayHelper::getValue($options, 'footer', Html::submitButton($model->isNewRecord ? \Yii::t('app', 'Create') : \Yii::t('app', 'Update'), [
                        'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary',
                        'form' => $formOptions['options']['id'],
                    ]));

                    $formArgs = [
                        'view' => $options['view'],
                        'modalOptions' => static::getModalOptions($modalOptions, $model),
                        'title' => static::getTitle($model, $options['title']),
                        'footer' => $footer,
                    ];

                    if ($setViewOptions) {
                        Response::viewOptions(null, $formArgs);
                    }

                    /**
                     * Get data provider information.
                     */
                    $dataProviderOptions = array_intersect_key($options, [
                        'provider' => null,
                        'args' => null,
                        'force' => null,
                    ]);
                    $ret_val['data'] = static::getDataProvider($model, $dataProviderOptions);

                    if (is_callable($options['viewArgs'])) {
                        $options['viewArgs'] = call_user_func_array($options['viewArgs'], [$model, $formOptions, $ret_val['data'], $scenario]);
                    }

                    $formArgs['args'] = array_merge([
                        'scenario' => $scenario,
                        'formOptions' => $formOptions,
                        'model' => $model,
                        'dataProvider' => $ret_val['data'],
                        'action' => $scenario,
                        'type' => $model->isWhat(),
                    ], $options['viewArgs']);

                    if ($setViewOptions) {
                        Response::viewOptions('args', $formArgs['args']);
                        switch (\Yii::$app->request->isAjax) {
                            case false:
                            Response::viewOptions('options', [
                                'style' => 'padding: 0 15px; position: absolute; height: 100%, width: 100%',
                            ]);
                            break;
                        }
                    } else {
                        $ret_val['form'] = $formArgs;
                    }
                    $ret_val['success'] = true;
                    $ret_val['action'] = $options['param'];
                }
            }
        }

        if (is_callable($callback = ArrayHelper::remove($options, 'callback', null))) {
            Response::viewOptions('content', call_user_func_array($callback, [$formArgs]));
        }

        return $ret_val;
    }
    public static function getDefaultGridActions($extraActions = [], $options = [])
    {
        $except = ArrayHelper::remove($extraActions, 'except', null);
        extract($options);
        $controller = isset($controller) ? $controller : '';
        $absolute = isset($absolute) ? true : false;
        $actions = array_replace_recursive([
               'class' => 'nitm\grid\ActionColumn',
               'buttons' => [
                   'form/update' => function ($url, $model) {
                       return Html::a(Icon::forAction('update'), $url, [
                           'title' => Yii::t('yii', 'Edit '.$model->title()),
                           'role' => 'disabledOnClose',
                           'data-pjax' => 1,
                           'class' => implode(' ', [
                               'fa-2x', ($model->deleted_at ? 'hidden' : ''),
                           ]),
                       ]);
                   },
                   'view' => function ($url, $model) {
                       return Html::a(Icon::forAction('view'), $url, [
                           'title' => Yii::t('yii', 'View '.$model->isWhat().' '.$model->title()),
                           'role' => 'disabledOnClose',
                           'data-pjax' => 1,
                           'class' => implode(' ', [
                               'fa-2x', ($model->deleted_at ? 'hidden' : ''),
                           ]),
                       ]);
                   },
                   'permalink' => function ($url, $model) {
                       return Html::a(Icon::show('link'),  \nitm\cms\helpers\App::getPublicLink($model), [
                           'target' => '_blank',
                           'title' => Yii::t('yii', 'View '.$model->isWhat().': '.$model->title.' on frontend'),
                           'data-pjax' => 1,
                           'class' => 'fa-2x',
                       ]);
                   },
                   'delete' => function ($url, $model) {
                       return Html::a(Icon::forAction('delete'), $url, [
                           'title' => Yii::t('yii', 'Delete '.$model->isWhat().' '.$model->title()),
                           'role' => 'deleteAction metaAction disabledOnClose',
                           'data-pjax' => 1,
                           'class' => implode(' ', [
                               'fa-2x', ($model->deleted_at ? 'hidden' : ''),
                           ]),
                       ]);
                   },
               ],
               'urlCreator' => function ($action, $model) use ($controller, $absolute) {
                   if (($formLocation = strpos(\Yii::$app->requestedRoute, '/form')) !== false) {
                       $baseUrl = substr(\Yii::$app->requestedRoute, 0, $fromLocation);
                   } else {
                       $baseUrl = \Yii::$app->requestedRoute;
                   }
                   $params = [
                       ($absolute ? '/' : '').implode('/', array_unique([$baseUrl, $controller, $action])),
                       $model->primaryKey()[0] => $model->getId(),
                   ];

                   return \yii\helpers\Url::toRoute($params);
               },
           ], $extraActions);
        if ($except) {
            $actions['buttons'] = array_intersect_key($actions['buttons'], array_flip($except));
        }
        $actions['template'] = implode(' ', array_map(function ($action) {
            return '{'.$action.'}';
        }, array_keys($actions['buttons'])));

        return $actions;
    }

    protected static function findQuery($id, $modelClass, $options = [])
    {
        $find = $modelClass::find()->select('*')->where([$modelClass::primaryKey()[0] => $id]);
        if ((sizeof($options) >= 1) && is_array($options)) {
            foreach ($options as $type => $value) {
                $find->$type($value);
            }
        }

        return $find;
    }

    public static function getDataProvider($model, $options)
    {
        $ret_val = new \yii\data\ArrayDataProvider(ArrayHelper::getValue($options, 'provider.construct', []));
        if (!isset($options['provider'])) {
            return $ret_val;
        }

        switch (is_array($options['provider'])) {
            case true:
            $object = $model;
            foreach ($options['provider'] as $func => $property) {
                if (is_callable($property)) {
                    $object = call_user_func($property, $object);
                } elseif (is_object($property) && $property->hasMethod($func)) {
                    $object = call_user_func([$property, $func], $object);
                } elseif (is_object($object) && is_string($property) && $object->hasMethod($property)) {
                    $object = call_user_func_array([$object, $property], isset($options['args']) ? $options['args'] : []);
                } elseif (is_object($object) && $object->hasAttribute($property)) {
                    $object = $object->$property;
                } elseif (is_object($property) && $property->hasAttribute($func)) {
                    $object = $property->$func;
                }
            }
            $ret_val->setModels((array) $object);
            break;

            default:
            if ($model->hasMethod($options['provider']) || ArrayHelper::getValue($options, 'force', false) == true) {
                $ret_val->setModels(call_user_func_array([$model, $options['provider']], (array) @$options['args']));
            } elseif (is_callable($options['provider'])) {
                $ret_val->setModels(call_user_func_array($options['provider'], $model));
            } elseif ($model->hasAttribute($options['provider']) || $model->hasProperty($options['provider'])) {
                $ret_val->setModels((array) $model->getAttribute($options['provider']));
            }
            break;
        }

        return $ret_val;
    }

    public static function getTitle($model, $options)
    {
        switch (1) {
            case is_callable($options):
            $title = $options($model);
            break;

            case $model->hasProperty(@$options[0]) || $model->hasAttribute(@$options[0]):
            $title = $model->getAttribute($options[0]);
            break;

            case is_string($options):
            $title = $options;
            break;

            case is_array($options):
            $title = @$options[1];
            break;

            default:
            $title = ($model->getIsNewRecord() ? 'Create' : 'Update').' '.ucfirst($model->properName($model->isWhat()));
            break;
        }

        return $title;
    }

    protected static function getModalOptions($options, $model)
    {
        foreach ($options as $option => $settings) {
            switch (is_callable($settings)) {
                case true:
                $options[$option] = $settings($model);
                break;

                default:
                $options[$option] = (array) $settings;
                break;
            }
        }

        return $options;
    }

    public static function getHtmlOptions($items = [], $idKey = 'id', $valueKey = 'name')
    {
        $ret_val = [];
        foreach ($items as $idx => $item) {
            switch (is_array($item->$valueKey)) {
                case true:
                $ret_val[$idx] = static::getHtmlOptions($item->$valueKey);
                break;

                default:
                $ret_val[$item->$idKey] = $item->$valueKey;
                break;
            }
        }

        return $ret_val;
    }
}
