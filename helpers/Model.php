<?php

namespace nitm\helpers;

class Model
{
    public static function getTitle($model)
    {
        return $model->hasMethod('title') ? $model->title() : ArrayHelper::getValue($model, 'title', ArrayHelper::getValue($model, 'name', ''));
    }

    public static function concatAttributes($models, $attributes, $glue = '-', $discardEmpty = false)
    {
        $ret_val = [];
        if (count($models)) {
            $models = is_array($models) ? $models : [$models];
            foreach ($models as $model) {
                $ret_val[] = implode($glue, array_map(function ($attribute) use ($model, $discardEmpty) {
                    if (is_callable($attribute)) {
                        return call_user_func($model, $attribute);
                    } elseif (is_object($model) && $model->hasMethod($attribute)) {
                        $arguments = explode(':', $attribute);
                        $attribute = $arguments[0];
                        switch (strtolower($attribute)) {
                            case 'getid':
                            case 'iswhat':
                            return call_user_func_array([$model, $attribute], (array) $arguments);
                            break;

                            default:
                            return call_user_func_array([$model, $attribute], array_merge([$model], (array) $arguments));
                            break;
                        }
                    } elseif (is_object($model) && $attribute && $model->hasAttribute($attribute)) {
                        return \yii\helpers\ArrayHelper::getValue($model, $attribute, ($discardEmpty ? null : $attribute));
                    }
                }, (array) $attributes));
            }
        }

        return implode($glue, array_filter($ret_val));
    }

    public static function resolveUrl($model, $options = [])
    {
        extract($options);
        $url = $url ?? '';
        $absolute = isset($absolute) ? $absolute : $baseUrl[0] == '/';
        $controller = @$controller ?: \Yii::$app->controller->id;
        $module = @$module ?: \Yii::$app->controller->module->id;
        $action = @$action ?: \Yii::$app->controller->action->id;

        if (!isset($baseUrl)) {
            if (isset($mask) && ($location = strpos(\Yii::$app->requestedRoute, $mask)) !== false) {
                $url = substr(\Yii::$app->request->getBaseUrl(), 0, $location);
            } else {
                // The url is in the proper format
                $url = \Yii::$app->request->getBaseUrl();
            }
        }
        if (isset($baseUrl) && $baseUrl) {
            $baseUrl = ($absolute ? '/' : '').trim($baseUrl, '/');
            $url = $baseUrl.'/'.ltrim($url, $baseUrl);
        }

        $params = array_merge((isset($params) ? $params : []), [
            implode('/', array_unique([
               rtrim(rtrim($url, $controller), '/'),
               $module == 'app' ? null : $module,
               $controller,
               $action,
            ])),
            $model->primaryKey()[0] => $model->getId(),
        ]);

        return \Yii::$app->urlManager->createUrl($params);
    }

    /**
     * Get the label for use based on.
     *
     * @param $model The model being resolved
     * @param mixed $label     Either a string or array indicating where the label lies
     * @param mixed $separator the separator being used to clue everything together
     *
     * @return string
     */
    public static function getLabel($model, $label, $separator = ' ')
    {
        if (is_array($label)) {
            $resolvedLabel = '';
            /**
             * We're supporting multi propertiy/relation properties for labels.
             */
            foreach ($label as $idx => $l) {
                $workingItem = $model;
                $properties = explode('.', $l);
                foreach ($properties as $prop) {
                    if (is_object($workingItem) && ($workingItem->hasAttribute($prop) || $workingItem->hasProperty($prop))) {
                        $workingItem = $workingItem->$prop;
                    }
                }
               /**
                * Support enacpsulating sub values when $separator is sent as a length==2 array.
                */
               switch (is_array($separator) && ($idx == sizeof($label) - 1)) {
                    case true:
                    $resolvedLabel .= $separator[0].$workingItem.$separator[1];
                    break;

                    default:
                    $resolvedLabel .= $workingItem.(is_array($separator) ? $separator[2] : $separator);
                    break;
               }
            }
            $ret_val = $resolvedLabel;
        } elseif (is_callable($label)) {
            $ret_val = call_user_func_array($label, [
               $model, null, $separator,
           ]);
        } else {
            $ret_val = ArrayHelper::getValue($model, $label, $label);
        }

        return $ret_val;
    }

    public function getTitleAttribute($model = null)
    {
        if ($model && method_exists($model, 'getTitleAttribute')) {
            return $model->titleAttribute;
        }

        return 'title';
    }

    /**
     * Function to get items for the List methods.
     *
     * @return array
     */
    public static function locateItems($model, $options, $count = false)
    {
        $class = $model->className();
        $items = [];
        $options = array_merge([
             'limit' => 100,
             'select' => '*',
         ], $options);

        if (ArrayHelper::getValue($options, 'children', false) === true) {
            $query = $class::findFromRoot();
        } else {
            $query = $class::find();
        }
        foreach ($options as $name => $value) {
            if ($query->hasMethod($name)) {
                $query->$name($value);
            }
        }

        if ($count === true) {
            $items = $query->count();
        } else {
            $items = $query->all();
        }

        return $items;
    }

    /**
     * Format the errors as a single string.
     *
     * @param ActiveRecord $model [description]
     *
     * @return string [description]
     */
    public static function formatErrors($model)
    {
        $result = '';
        foreach ($model->getErrors() as $attribute => $errors) {
            $result .= implode("\n", $errors).' ';
        }

        return $result;
    }
}
